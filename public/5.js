(window["webpackJsonp"] = window["webpackJsonp"] || []).push([[5],{

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/Paymentmethods.vue?vue&type=script&lang=js&":
/*!********************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/views/Paymentmethods.vue?vue&type=script&lang=js& ***!
  \********************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _components_orders_list_component__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../components/orders.list.component */ "./resources/js/components/orders.list.component.vue");
function _toConsumableArray(arr) { return _arrayWithoutHoles(arr) || _iterableToArray(arr) || _unsupportedIterableToArray(arr) || _nonIterableSpread(); }

function _nonIterableSpread() { throw new TypeError("Invalid attempt to spread non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); }

function _unsupportedIterableToArray(o, minLen) { if (!o) return; if (typeof o === "string") return _arrayLikeToArray(o, minLen); var n = Object.prototype.toString.call(o).slice(8, -1); if (n === "Object" && o.constructor) n = o.constructor.name; if (n === "Map" || n === "Set") return Array.from(o); if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen); }

function _iterableToArray(iter) { if (typeof Symbol !== "undefined" && Symbol.iterator in Object(iter)) return Array.from(iter); }

function _arrayWithoutHoles(arr) { if (Array.isArray(arr)) return _arrayLikeToArray(arr); }

function _arrayLikeToArray(arr, len) { if (len == null || len > arr.length) len = arr.length; for (var i = 0, arr2 = new Array(len); i < len; i++) { arr2[i] = arr[i]; } return arr2; }

//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

/* harmony default export */ __webpack_exports__["default"] = ({
  data: function data() {
    return {
      orders: []
    };
  },
  components: {
    OrdersList: _components_orders_list_component__WEBPACK_IMPORTED_MODULE_0__["default"]
  },
  methods: {
    removeOrderItem: function removeOrderItem(orderId) {
      var otherOrders = this.orders.filter(function (order) {
        return order.orderId != orderId;
      });
      this.orders = _toConsumableArray(otherOrders);
      var storedOrdersData = localStorage.getItem('orders'); // nice one last issue when u dont add any quantity it adds it to the cart eg

      if (storedOrdersData) {
        localStorage.setItem('orders', JSON.stringify(this.orders));
      }
    }
  },
  created: function created() {
    var storedOrdersData = localStorage.getItem('orders');

    if (storedOrdersData) {
      var storeOrders = JSON.parse(storedOrdersData);
      this.orders = storeOrders;
    }
  }
});

/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/Paymentmethods.vue?vue&type=template&id=d3b3d86a&":
/*!************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/views/Paymentmethods.vue?vue&type=template&id=d3b3d86a& ***!
  \************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("div", [
    _vm._m(0),
    _vm._v(" "),
    _vm._m(1),
    _vm._v(" "),
    _c("div", { staticClass: "container margin_60_35" }, [
      _c(
        "div",
        { staticClass: "row" },
        [
          _vm._m(2),
          _vm._v(" "),
          _vm._m(3),
          _vm._v(" "),
          _c(
            "OrdersList",
            {
              attrs: { orders: _vm.orders },
              on: {
                removeOrderItem: function($event) {
                  return _vm.removeOrderItem($event)
                }
              }
            },
            [
              _c(
                "a",
                { staticClass: "btn_full", attrs: { href: "/confirm" } },
                [_vm._v("Confirm your order")]
              )
            ]
          )
        ],
        1
      )
    ])
  ])
}
var staticRenderFns = [
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c(
      "section",
      {
        staticClass: "parallax-window",
        attrs: {
          id: "short",
          "data-parallax": "scroll",
          "data-image-src": "img/sub_header_cart.jpg",
          "data-natural-width": "1400",
          "data-natural-height": "350"
        }
      },
      [
        _c(
          "div",
          {
            staticStyle: { background: "url(img/sub_header_cart.jpg)" },
            attrs: { id: "subheader" }
          },
          [
            _c("div", { staticClass: "overlay" }),
            _vm._v(" "),
            _c("div", { attrs: { id: "sub_content" } }, [
              _c("h1", [_vm._v("Place your order")]),
              _vm._v(" "),
              _c("div", { staticClass: "bs-wizard" }, [
                _c("div", { staticClass: "col-xs-4 bs-wizard-step complete" }, [
                  _c("div", { staticClass: "text-center bs-wizard-stepnum" }, [
                    _c("strong", [_vm._v("1.")]),
                    _vm._v(" Your details")
                  ]),
                  _vm._v(" "),
                  _c("div", { staticClass: "progress" }, [
                    _c("div", { staticClass: "progress-bar" })
                  ]),
                  _vm._v(" "),
                  _c("a", {
                    staticClass: "bs-wizard-dot",
                    attrs: { href: "/carts" }
                  })
                ]),
                _vm._v(" "),
                _c("div", { staticClass: "col-xs-4 bs-wizard-step active" }, [
                  _c("div", { staticClass: "text-center bs-wizard-stepnum" }, [
                    _c("strong", [_vm._v("2.")]),
                    _vm._v(" Payment")
                  ]),
                  _vm._v(" "),
                  _c("div", { staticClass: "progress" }, [
                    _c("div", { staticClass: "progress-bar" })
                  ]),
                  _vm._v(" "),
                  _c("a", {
                    staticClass: "bs-wizard-dot",
                    attrs: { href: "#" }
                  })
                ]),
                _vm._v(" "),
                _c("div", { staticClass: "col-xs-4 bs-wizard-step disabled" }, [
                  _c("div", { staticClass: "text-center bs-wizard-stepnum" }, [
                    _c("strong", [_vm._v("3.")]),
                    _vm._v(" Finish!")
                  ]),
                  _vm._v(" "),
                  _c("div", { staticClass: "progress" }, [
                    _c("div", { staticClass: "progress-bar" })
                  ]),
                  _vm._v(" "),
                  _c("a", {
                    staticClass: "bs-wizard-dot",
                    attrs: { href: "/confirm" }
                  })
                ])
              ])
            ])
          ]
        )
      ]
    )
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { attrs: { id: "position" } }, [
      _c("div", { staticClass: "container" }, [
        _c("ul", [
          _c("li", [_c("a", { attrs: { href: "#0" } }, [_vm._v("Home")])]),
          _vm._v(" "),
          _c("li", [_c("a", { attrs: { href: "#0" } }, [_vm._v("Category")])]),
          _vm._v(" "),
          _c("li", [_vm._v("Page active")])
        ]),
        _vm._v(" "),
        _c(
          "a",
          { staticClass: "search-overlay-menu-btn", attrs: { href: "#0" } },
          [_c("i", { staticClass: "icon-search-6" }), _vm._v(" Search")]
        )
      ])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "col-md-3" }, [
      _c("div", { staticClass: "box_style_2 hidden-xs info" }, [
        _c("h4", { staticClass: "nomargin_top" }, [
          _vm._v("Delivery time "),
          _c("i", { staticClass: "icon_clock_alt pull-right" })
        ]),
        _vm._v(" "),
        _c("p", [
          _vm._v(
            "\n\t\t\t\t\t\tOur efficient delivery bike riders will get to you before the your food is cold.\n\t\t\t\t\t"
          )
        ]),
        _vm._v(" "),
        _c("hr"),
        _vm._v(" "),
        _c("h4", [
          _vm._v("Secure payment "),
          _c("i", { staticClass: "icon_creditcard pull-right" })
        ]),
        _vm._v(" "),
        _c("p", [
          _vm._v(
            "\n\t\t\t\t\t\tWe provide secure payments on all transactions you do on our platform\n\t\t\t\t\t"
          )
        ])
      ]),
      _vm._v(" "),
      _c(
        "div",
        { staticClass: "box_style_2 hidden-xs", attrs: { id: "help" } },
        [
          _c("i", { staticClass: "icon_lifesaver" }),
          _vm._v(" "),
          _c("h4", [_vm._v("Need "), _c("span", [_vm._v("Help?")])]),
          _vm._v(" "),
          _c(
            "a",
            { staticClass: "phone", attrs: { href: "tel://004542344599" } },
            [_vm._v("+45 423 445 99")]
          ),
          _vm._v(" "),
          _c("small", [_vm._v("Monday to Friday 9.00am - 7.30pm")])
        ]
      )
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "col-md-6" }, [
      _c("div", { staticClass: "box_style_2" }, [
        _c("h2", { staticClass: "inner" }, [_vm._v("Payment methods")]),
        _vm._v(" "),
        _c("div", { staticClass: "payment_select" }, [
          _c("label", [
            _c("input", {
              staticClass: "icheck",
              attrs: {
                type: "radio",
                value: "",
                checked: "",
                name: "payment_method"
              }
            }),
            _vm._v("Credit card")
          ]),
          _vm._v(" "),
          _c("i", { staticClass: "icon_creditcard" })
        ]),
        _vm._v(" "),
        _c("div", { staticClass: "form-group" }, [
          _c("label", [_vm._v("Name on card")]),
          _vm._v(" "),
          _c("input", {
            staticClass: "form-control",
            attrs: {
              type: "text",
              id: "name_card_order",
              name: "name_card_order",
              placeholder: "First and last name"
            }
          })
        ]),
        _vm._v(" "),
        _c("div", { staticClass: "form-group" }, [
          _c("label", [_vm._v("Card number")]),
          _vm._v(" "),
          _c("input", {
            staticClass: "form-control",
            attrs: {
              type: "text",
              id: "card_number",
              name: "card_number",
              placeholder: "Card number"
            }
          })
        ]),
        _vm._v(" "),
        _c("div", { staticClass: "row" }, [
          _c("div", { staticClass: "col-md-6" }, [
            _c("label", [_vm._v("Expiration date")]),
            _vm._v(" "),
            _c("div", { staticClass: "row" }, [
              _c("div", { staticClass: "col-md-6 col-sm-6" }, [
                _c("div", { staticClass: "form-group" }, [
                  _c("input", {
                    staticClass: "form-control",
                    attrs: {
                      type: "text",
                      id: "expire_month",
                      name: "expire_month",
                      placeholder: "mm"
                    }
                  })
                ])
              ]),
              _vm._v(" "),
              _c("div", { staticClass: "col-md-6 col-sm-6" }, [
                _c("div", { staticClass: "form-group" }, [
                  _c("input", {
                    staticClass: "form-control",
                    attrs: {
                      type: "text",
                      id: "expire_year",
                      name: "expire_year",
                      placeholder: "yyyy"
                    }
                  })
                ])
              ])
            ])
          ]),
          _vm._v(" "),
          _c("div", { staticClass: "col-md-6 col-sm-12" }, [
            _c("div", { staticClass: "form-group" }, [
              _c("label", [_vm._v("Security code")]),
              _vm._v(" "),
              _c("div", { staticClass: "row" }, [
                _c("div", { staticClass: "col-md-4 col-sm-6" }, [
                  _c("div", { staticClass: "form-group" }, [
                    _c("input", {
                      staticClass: "form-control",
                      attrs: {
                        type: "text",
                        id: "ccv",
                        name: "ccv",
                        placeholder: "CCV"
                      }
                    })
                  ])
                ]),
                _vm._v(" "),
                _c("div", { staticClass: "col-md-8 col-sm-6" }, [
                  _c("img", {
                    attrs: {
                      src: "img/icon_ccv.gif",
                      width: "50",
                      height: "29",
                      alt: "ccv"
                    }
                  }),
                  _c("small", [_vm._v("Last 3 digits")])
                ])
              ])
            ])
          ])
        ]),
        _vm._v(" "),
        _c("div", { staticClass: "payment_select", attrs: { id: "paypal" } }, [
          _c("label", [
            _c("input", {
              staticClass: "icheck",
              attrs: { type: "radio", value: "", name: "payment_method" }
            }),
            _vm._v("Pay with paypal")
          ])
        ]),
        _vm._v(" "),
        _c("div", { staticClass: "payment_select nomargin" }, [
          _c("label", [
            _c("input", {
              staticClass: "icheck",
              attrs: { type: "radio", value: "", name: "payment_method" }
            }),
            _vm._v("Pay with cash")
          ]),
          _vm._v(" "),
          _c("i", { staticClass: "icon_wallet" })
        ])
      ])
    ])
  }
]
render._withStripped = true



/***/ }),

/***/ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js":
/*!********************************************************************!*\
  !*** ./node_modules/vue-loader/lib/runtime/componentNormalizer.js ***!
  \********************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return normalizeComponent; });
/* globals __VUE_SSR_CONTEXT__ */

// IMPORTANT: Do NOT use ES2015 features in this file (except for modules).
// This module is a runtime utility for cleaner component module output and will
// be included in the final webpack user bundle.

function normalizeComponent (
  scriptExports,
  render,
  staticRenderFns,
  functionalTemplate,
  injectStyles,
  scopeId,
  moduleIdentifier, /* server only */
  shadowMode /* vue-cli only */
) {
  // Vue.extend constructor export interop
  var options = typeof scriptExports === 'function'
    ? scriptExports.options
    : scriptExports

  // render functions
  if (render) {
    options.render = render
    options.staticRenderFns = staticRenderFns
    options._compiled = true
  }

  // functional template
  if (functionalTemplate) {
    options.functional = true
  }

  // scopedId
  if (scopeId) {
    options._scopeId = 'data-v-' + scopeId
  }

  var hook
  if (moduleIdentifier) { // server build
    hook = function (context) {
      // 2.3 injection
      context =
        context || // cached call
        (this.$vnode && this.$vnode.ssrContext) || // stateful
        (this.parent && this.parent.$vnode && this.parent.$vnode.ssrContext) // functional
      // 2.2 with runInNewContext: true
      if (!context && typeof __VUE_SSR_CONTEXT__ !== 'undefined') {
        context = __VUE_SSR_CONTEXT__
      }
      // inject component styles
      if (injectStyles) {
        injectStyles.call(this, context)
      }
      // register component module identifier for async chunk inferrence
      if (context && context._registeredComponents) {
        context._registeredComponents.add(moduleIdentifier)
      }
    }
    // used by ssr in case component is cached and beforeCreate
    // never gets called
    options._ssrRegister = hook
  } else if (injectStyles) {
    hook = shadowMode
      ? function () {
        injectStyles.call(
          this,
          (options.functional ? this.parent : this).$root.$options.shadowRoot
        )
      }
      : injectStyles
  }

  if (hook) {
    if (options.functional) {
      // for template-only hot-reload because in that case the render fn doesn't
      // go through the normalizer
      options._injectStyles = hook
      // register for functional component in vue file
      var originalRender = options.render
      options.render = function renderWithStyleInjection (h, context) {
        hook.call(context)
        return originalRender(h, context)
      }
    } else {
      // inject component registration as beforeCreate hook
      var existing = options.beforeCreate
      options.beforeCreate = existing
        ? [].concat(existing, hook)
        : [hook]
    }
  }

  return {
    exports: scriptExports,
    options: options
  }
}


/***/ }),

/***/ "./resources/js/views/Paymentmethods.vue":
/*!***********************************************!*\
  !*** ./resources/js/views/Paymentmethods.vue ***!
  \***********************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _Paymentmethods_vue_vue_type_template_id_d3b3d86a___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./Paymentmethods.vue?vue&type=template&id=d3b3d86a& */ "./resources/js/views/Paymentmethods.vue?vue&type=template&id=d3b3d86a&");
/* harmony import */ var _Paymentmethods_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./Paymentmethods.vue?vue&type=script&lang=js& */ "./resources/js/views/Paymentmethods.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _Paymentmethods_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _Paymentmethods_vue_vue_type_template_id_d3b3d86a___WEBPACK_IMPORTED_MODULE_0__["render"],
  _Paymentmethods_vue_vue_type_template_id_d3b3d86a___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/views/Paymentmethods.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/views/Paymentmethods.vue?vue&type=script&lang=js&":
/*!************************************************************************!*\
  !*** ./resources/js/views/Paymentmethods.vue?vue&type=script&lang=js& ***!
  \************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Paymentmethods_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../node_modules/babel-loader/lib??ref--4-0!../../../node_modules/vue-loader/lib??vue-loader-options!./Paymentmethods.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/Paymentmethods.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Paymentmethods_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/views/Paymentmethods.vue?vue&type=template&id=d3b3d86a&":
/*!******************************************************************************!*\
  !*** ./resources/js/views/Paymentmethods.vue?vue&type=template&id=d3b3d86a& ***!
  \******************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Paymentmethods_vue_vue_type_template_id_d3b3d86a___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../node_modules/vue-loader/lib??vue-loader-options!./Paymentmethods.vue?vue&type=template&id=d3b3d86a& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/Paymentmethods.vue?vue&type=template&id=d3b3d86a&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Paymentmethods_vue_vue_type_template_id_d3b3d86a___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Paymentmethods_vue_vue_type_template_id_d3b3d86a___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ })

}]);