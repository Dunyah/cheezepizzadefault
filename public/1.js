(window["webpackJsonp"] = window["webpackJsonp"] || []).push([[1],{

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/Main/main.vue?vue&type=script&lang=js&":
/*!*********************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/Main/main.vue?vue&type=script&lang=js& ***!
  \*********************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
/* harmony default export */ __webpack_exports__["default"] = ({
  methods: {
    closeLoginModal: function closeLoginModal() {
      $('#login_2').modal('toggle');
    }
  }
});

/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/Main/main.vue?vue&type=template&id=5ab31421&":
/*!*************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/Main/main.vue?vue&type=template&id=5ab31421& ***!
  \*************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "div",
    [
      _vm._m(0),
      _vm._v(" "),
      _vm._m(1),
      _vm._v(" "),
      _c("router-view"),
      _vm._v(" "),
      _vm._m(2),
      _vm._v(" "),
      _c("div", { staticClass: "layer" }),
      _vm._v(" "),
      _c(
        "div",
        {
          staticClass: "modal ",
          attrs: {
            id: "login_2",
            tabindex: "-1",
            role: "dialog",
            "aria-labelledby": "myLogin",
            "aria-hidden": "true"
          }
        },
        [
          _c("div", { staticClass: "modal-dialog" }, [
            _c("div", { staticClass: "modal-content modal-popup" }, [
              _c("a", { staticClass: "close-link", attrs: { href: "#" } }, [
                _c("i", {
                  staticClass: "icon_close_alt2",
                  on: {
                    click: function($event) {
                      return _vm.closeLoginModal()
                    }
                  }
                })
              ]),
              _vm._v(" "),
              _vm._m(3)
            ])
          ])
        ]
      )
    ],
    1
  )
}
var staticRenderFns = [
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { attrs: { id: "preloader" } }, [
      _c(
        "div",
        { staticClass: "sk-spinner sk-spinner-wave", attrs: { id: "status" } },
        [
          _c("div", { staticClass: "sk-rect1" }),
          _vm._v(" "),
          _c("div", { staticClass: "sk-rect2" }),
          _vm._v(" "),
          _c("div", { staticClass: "sk-rect3" }),
          _vm._v(" "),
          _c("div", { staticClass: "sk-rect4" }),
          _vm._v(" "),
          _c("div", { staticClass: "sk-rect5" })
        ]
      )
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("header", [
      _c("div", { staticClass: "container-fluid" }, [
        _c("div", { staticClass: "row" }, [
          _c("div", { staticClass: "col--md-4 col-sm-4 col-xs-4" }, [
            _c("a", { attrs: { href: "index.html", id: "logo" } }, [
              _c("img", {
                staticClass: "hidden-xs",
                attrs: {
                  src: "img/logo.png",
                  width: "190",
                  height: "23",
                  alt: "",
                  "data-retina": "true"
                }
              }),
              _vm._v(" "),
              _c("img", {
                staticClass: "hidden-lg hidden-md hidden-sm",
                attrs: {
                  src: "img/logo.png",
                  width: "59",
                  height: "23",
                  alt: "",
                  "data-retina": "true"
                }
              })
            ])
          ]),
          _vm._v(" "),
          _c("nav", { staticClass: "col--md-8 col-sm-8 col-xs-8" }, [
            _c(
              "a",
              {
                staticClass:
                  "cmn-toggle-switch cmn-toggle-switch__htx open_close",
                attrs: { href: "javascript:void(0);" }
              },
              [_c("span", [_vm._v("Menu mobile")])]
            ),
            _vm._v(" "),
            _c("div", { staticClass: "main-menu" }, [
              _c("div", { attrs: { id: "header_menu" } }, [
                _c("img", {
                  attrs: {
                    src: "img/logo.png",
                    width: "190",
                    height: "23",
                    alt: "",
                    "data-retina": "true"
                  }
                })
              ]),
              _vm._v(" "),
              _c(
                "a",
                {
                  staticClass: "open_close",
                  attrs: { href: "#", id: "close_in" }
                },
                [_c("i", { staticClass: "icon_close" })]
              ),
              _vm._v(" "),
              _c("ul", [
                _c("li", { staticClass: "submenu" }, [
                  _c(
                    "a",
                    { staticClass: "show-submenu", attrs: { href: "/" } },
                    [_vm._v("Home")]
                  )
                ]),
                _vm._v(" "),
                _c("li", [
                  _c("a", { attrs: { href: "/about" } }, [_vm._v("About us")])
                ]),
                _vm._v(" "),
                _c("li", [
                  _c(
                    "a",
                    {
                      attrs: {
                        href: "#",
                        "data-toggle": "modal",
                        "data-target": "#login_2"
                      }
                    },
                    [_vm._v("Login")]
                  )
                ]),
                _vm._v(" "),
                _c("li", [
                  _c("a", { attrs: { href: "/contact" } }, [
                    _vm._v("Contact Us")
                  ])
                ])
              ])
            ])
          ])
        ])
      ])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("footer", [
      _c("div", { staticClass: "container" }, [
        _c("div", { staticClass: "row" }, [
          _c("div", { staticClass: "col-md-2 col-sm-3" })
        ]),
        _vm._v(" "),
        _c("div", { staticClass: "row" }, [
          _c("div", { staticClass: "col-md-12" }, [
            _c("div", { attrs: { id: "social_footer" } }, [
              _c("ul", [
                _c("li", [
                  _c(
                    "a",
                    { attrs: { href: "https://m.facebook.com/cheezzypizza/" } },
                    [_c("i", { staticClass: "icon-facebook" })]
                  )
                ]),
                _vm._v(" "),
                _c("li", [
                  _c(
                    "a",
                    { attrs: { href: "https://twitter.com/CheezzyPizza" } },
                    [_c("i", { staticClass: "icon-twitter" })]
                  )
                ]),
                _vm._v(" "),
                _c("li", [
                  _c(
                    "a",
                    {
                      attrs: {
                        href:
                          "https://www.youtube.com/watch?v=wt5v0g4sNxI&feature=emb_logo"
                      }
                    },
                    [_c("i", { staticClass: "icon-youtube-play" })]
                  )
                ])
              ]),
              _vm._v(" "),
              _c("p", [
                _vm._v(
                  "\n                            © Cheezy Pizza 2020\n                        "
                )
              ])
            ])
          ])
        ])
      ])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c(
      "form",
      { staticClass: "popup-form", attrs: { action: "#", id: "myLogin" } },
      [
        _c("div", { staticClass: "login_icon" }, [
          _c("i", { staticClass: "icon_lock_alt" })
        ]),
        _vm._v(" "),
        _c("input", {
          staticClass: "form-control form-white",
          attrs: { type: "text", placeholder: "Username" }
        }),
        _vm._v(" "),
        _c("input", {
          staticClass: "form-control form-white",
          attrs: { type: "text", placeholder: "Password" }
        }),
        _vm._v(" "),
        _c("div", { staticClass: "text-left" }, [
          _c("a", { attrs: { href: "#" } }, [_vm._v("Forgot Password?")])
        ]),
        _vm._v(" "),
        _c(
          "button",
          { staticClass: "btn btn-submit", attrs: { type: "submit" } },
          [_vm._v("Submit")]
        )
      ]
    )
  }
]
render._withStripped = true



/***/ }),

/***/ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js":
/*!********************************************************************!*\
  !*** ./node_modules/vue-loader/lib/runtime/componentNormalizer.js ***!
  \********************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return normalizeComponent; });
/* globals __VUE_SSR_CONTEXT__ */

// IMPORTANT: Do NOT use ES2015 features in this file (except for modules).
// This module is a runtime utility for cleaner component module output and will
// be included in the final webpack user bundle.

function normalizeComponent (
  scriptExports,
  render,
  staticRenderFns,
  functionalTemplate,
  injectStyles,
  scopeId,
  moduleIdentifier, /* server only */
  shadowMode /* vue-cli only */
) {
  // Vue.extend constructor export interop
  var options = typeof scriptExports === 'function'
    ? scriptExports.options
    : scriptExports

  // render functions
  if (render) {
    options.render = render
    options.staticRenderFns = staticRenderFns
    options._compiled = true
  }

  // functional template
  if (functionalTemplate) {
    options.functional = true
  }

  // scopedId
  if (scopeId) {
    options._scopeId = 'data-v-' + scopeId
  }

  var hook
  if (moduleIdentifier) { // server build
    hook = function (context) {
      // 2.3 injection
      context =
        context || // cached call
        (this.$vnode && this.$vnode.ssrContext) || // stateful
        (this.parent && this.parent.$vnode && this.parent.$vnode.ssrContext) // functional
      // 2.2 with runInNewContext: true
      if (!context && typeof __VUE_SSR_CONTEXT__ !== 'undefined') {
        context = __VUE_SSR_CONTEXT__
      }
      // inject component styles
      if (injectStyles) {
        injectStyles.call(this, context)
      }
      // register component module identifier for async chunk inferrence
      if (context && context._registeredComponents) {
        context._registeredComponents.add(moduleIdentifier)
      }
    }
    // used by ssr in case component is cached and beforeCreate
    // never gets called
    options._ssrRegister = hook
  } else if (injectStyles) {
    hook = shadowMode
      ? function () {
        injectStyles.call(
          this,
          (options.functional ? this.parent : this).$root.$options.shadowRoot
        )
      }
      : injectStyles
  }

  if (hook) {
    if (options.functional) {
      // for template-only hot-reload because in that case the render fn doesn't
      // go through the normalizer
      options._injectStyles = hook
      // register for functional component in vue file
      var originalRender = options.render
      options.render = function renderWithStyleInjection (h, context) {
        hook.call(context)
        return originalRender(h, context)
      }
    } else {
      // inject component registration as beforeCreate hook
      var existing = options.beforeCreate
      options.beforeCreate = existing
        ? [].concat(existing, hook)
        : [hook]
    }
  }

  return {
    exports: scriptExports,
    options: options
  }
}


/***/ }),

/***/ "./resources/js/Main/main.vue":
/*!************************************!*\
  !*** ./resources/js/Main/main.vue ***!
  \************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _main_vue_vue_type_template_id_5ab31421___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./main.vue?vue&type=template&id=5ab31421& */ "./resources/js/Main/main.vue?vue&type=template&id=5ab31421&");
/* harmony import */ var _main_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./main.vue?vue&type=script&lang=js& */ "./resources/js/Main/main.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _main_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _main_vue_vue_type_template_id_5ab31421___WEBPACK_IMPORTED_MODULE_0__["render"],
  _main_vue_vue_type_template_id_5ab31421___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/Main/main.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/Main/main.vue?vue&type=script&lang=js&":
/*!*************************************************************!*\
  !*** ./resources/js/Main/main.vue?vue&type=script&lang=js& ***!
  \*************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_main_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../node_modules/babel-loader/lib??ref--4-0!../../../node_modules/vue-loader/lib??vue-loader-options!./main.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/Main/main.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_main_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/Main/main.vue?vue&type=template&id=5ab31421&":
/*!*******************************************************************!*\
  !*** ./resources/js/Main/main.vue?vue&type=template&id=5ab31421& ***!
  \*******************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_main_vue_vue_type_template_id_5ab31421___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../node_modules/vue-loader/lib??vue-loader-options!./main.vue?vue&type=template&id=5ab31421& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/Main/main.vue?vue&type=template&id=5ab31421&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_main_vue_vue_type_template_id_5ab31421___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_main_vue_vue_type_template_id_5ab31421___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ })

}]);