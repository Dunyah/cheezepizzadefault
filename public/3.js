(window["webpackJsonp"] = window["webpackJsonp"] || []).push([[3],{

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/Home.vue?vue&type=script&lang=js&":
/*!**********************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/views/Home.vue?vue&type=script&lang=js& ***!
  \**********************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
/* harmony default export */ __webpack_exports__["default"] = ({
  data: function data() {
    return {
      branches: {}
    };
  },
  methods: {
    GetBranches: function GetBranches() {
      var _this = this;

      axios.get('/api/v1/branches').then(function (response) {
        // handle success
        if (response.data.status) {
          _this.branches = response.data.data;
          console.log("My Branch :", response.data);
          console.log(response.data.data);
          console.log(" Branches " + _this.branches);
        } else {
          console.log("empty " + response.data);
        }
      })["catch"](function (error) {
        // handle error
        console.log(error);
      });
    },
    GetBranchProductsAndCat: function GetBranchProductsAndCat(branch) {
      window.location.href = '/products';
      console.log("Branch Id :", branch.id);
      localStorage.setItem('branchId', branch.id);
      localStorage.setItem('branchName', branch.name);
      localStorage.setItem('location', branch.location);
      localStorage.setItem('openingTime', branch.opening_time);
    }
  },
  created: function created() {
    this.GetBranches();
  }
});

/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/Home.vue?vue&type=template&id=63cd6604&":
/*!**************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/views/Home.vue?vue&type=template&id=63cd6604& ***!
  \**************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("div", [
    _c(
      "section",
      {
        staticClass: "header-video",
        staticStyle: { width: "100vw !important", height: "100vh !important" }
      },
      [
        _vm._m(0),
        _vm._v(" "),
        _c("img", {
          staticClass: "header-video--media",
          staticStyle: { display: "none" },
          attrs: {
            src: "img/video_fix.png",
            alt: "",
            "data-video-src": "video/intro",
            "data-teaser-source": "video/intro",
            "data-provider": "Vimeo",
            "data-video-width": "1920",
            "data-video-height": "960"
          }
        }),
        _vm._v(" "),
        _vm._m(1),
        _vm._v(" "),
        _c(
          "video",
          {
            staticClass: "teaser-video",
            attrs: {
              autoplay: "true",
              loop: "loop",
              muted: "",
              id: "teaser-video"
            },
            domProps: { muted: true }
          },
          [
            _c("source", {
              attrs: { src: "video/intro.mp4", type: "video/mp4" }
            }),
            _vm._v(" "),
            _c("source", {
              attrs: { src: "video/intro.ogv", type: "video/ogg" }
            })
          ]
        ),
        _c("iframe", {
          attrs: {
            id: "video",
            src:
              "http://player.vimeo.com/video/video/intro?title=0&byline=0&portrait=0&color=3d96d2&autoplay=1",
            frameborder: "0",
            webkitallowfullscreen: "",
            mozallowfullscreen: "",
            allowfullscreen: ""
          }
        })
      ]
    ),
    _vm._v(" "),
    _c("div", { staticClass: "white_bg" }, [
      _c("div", { staticClass: "container margin_60" }, [
        _vm._m(2),
        _vm._v(" "),
        _c(
          "div",
          { staticClass: "row" },
          _vm._l(_vm.branches, function(branch) {
            return _c("div", { key: branch.id, staticClass: "col-md-6" }, [
              _c(
                "a",
                {
                  staticClass: "strip_list",
                  on: {
                    click: function($event) {
                      return _vm.GetBranchProductsAndCat(branch)
                    }
                  }
                },
                [
                  _c("div", { staticClass: "ribbon_1" }, [_vm._v("Popular")]),
                  _vm._v(" "),
                  _c("div", { staticClass: "desc" }, [
                    _vm._m(3, true),
                    _vm._v(" "),
                    _vm._m(4, true),
                    _vm._v(" "),
                    _c("h3", [_vm._v(_vm._s(branch.name))]),
                    _vm._v(" "),
                    _c("div", { staticClass: "type" }, [
                      _vm._v(
                        "\n                                 " +
                          _vm._s(branch.id) +
                          "\n                            "
                      )
                    ]),
                    _vm._v(" "),
                    _c("div", { staticClass: "location" }, [
                      _vm._v(
                        "\n                                " +
                          _vm._s(branch.location) +
                          " "
                      ),
                      _c("span", { staticClass: "opening" }, [
                        _vm._v(" Opens at " + _vm._s(branch.opening_time))
                      ])
                    ]),
                    _vm._v(" "),
                    _vm._m(5, true)
                  ])
                ]
              )
            ])
          }),
          0
        )
      ])
    ]),
    _vm._v(" "),
    _vm._m(6),
    _vm._v(" "),
    _vm._m(7)
  ])
}
var staticRenderFns = [
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { attrs: { id: "hero_video" } }, [
      _c("div", {
        staticClass: "animated zoomIn",
        attrs: { id: "sub_content" }
      })
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "hidden-xs", attrs: { id: "count" } }, [
      _c("ul", [
        _c("li", [
          _c("span", { staticClass: "number" }, [_vm._v("2650")]),
          _vm._v(" Restaurant")
        ]),
        _vm._v(" "),
        _c("li", [
          _c("span", { staticClass: "number" }, [_vm._v("5350")]),
          _vm._v(" People Served")
        ]),
        _vm._v(" "),
        _c("li", [
          _c("span", { staticClass: "number" }, [_vm._v("12350")]),
          _vm._v(" Registered Users")
        ])
      ])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "main_title" }, [
      _c("h2", { staticClass: "nomargin_top" }, [_vm._v("Select a branch")])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "thumb_strip" }, [
      _c("img", { attrs: { src: "img/thumb_restaurant.jpg", alt: "" } })
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "rating" }, [
      _c("i", { staticClass: "icon_star voted" }),
      _c("i", { staticClass: "icon_star voted" }),
      _c("i", { staticClass: "icon_star voted" }),
      _c("i", { staticClass: "icon_star voted" }),
      _c("i", { staticClass: "icon_star" })
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("ul", [
      _c("li", [
        _vm._v("Take away"),
        _c("i", { staticClass: "icon_check_alt2 ok" })
      ]),
      _vm._v(" "),
      _c("li", [
        _vm._v("Delivery"),
        _c("i", { staticClass: "icon_check_alt2 ok" })
      ])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "high_light" }, [
      _c("div", { staticClass: "container" }, [
        _c("h3", [_vm._v("Choose from over 2,000 Restaurants")]),
        _vm._v(" "),
        _c("p", [
          _vm._v(
            "Ridiculus sociosqu cursus neque cursus curae ante scelerisque vehicula."
          )
        ]),
        _vm._v(" "),
        _c("a", { attrs: { href: "list_page.html" } }, [
          _vm._v("View all Restaurants")
        ])
      ])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c(
      "section",
      {
        staticClass: "parallax-window",
        attrs: {
          "data-parallax": "scroll",
          "data-image-src": "img/bg_office.jpg",
          "data-natural-width": "1200",
          "data-natural-height": "600"
        }
      },
      [
        _c(
          "div",
          {
            staticClass: "parallax-content",
            staticStyle: { background: "url(img/bg_office.jpg)" }
          },
          [
            _c("div", { staticClass: "sub_content" }, [
              _c("i", { staticClass: "icon_mug" }),
              _vm._v(" "),
              _c("h3", [_vm._v("We also deliver to your office")]),
              _vm._v(" "),
              _c("p")
            ])
          ]
        )
      ]
    )
  }
]
render._withStripped = true



/***/ }),

/***/ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js":
/*!********************************************************************!*\
  !*** ./node_modules/vue-loader/lib/runtime/componentNormalizer.js ***!
  \********************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return normalizeComponent; });
/* globals __VUE_SSR_CONTEXT__ */

// IMPORTANT: Do NOT use ES2015 features in this file (except for modules).
// This module is a runtime utility for cleaner component module output and will
// be included in the final webpack user bundle.

function normalizeComponent (
  scriptExports,
  render,
  staticRenderFns,
  functionalTemplate,
  injectStyles,
  scopeId,
  moduleIdentifier, /* server only */
  shadowMode /* vue-cli only */
) {
  // Vue.extend constructor export interop
  var options = typeof scriptExports === 'function'
    ? scriptExports.options
    : scriptExports

  // render functions
  if (render) {
    options.render = render
    options.staticRenderFns = staticRenderFns
    options._compiled = true
  }

  // functional template
  if (functionalTemplate) {
    options.functional = true
  }

  // scopedId
  if (scopeId) {
    options._scopeId = 'data-v-' + scopeId
  }

  var hook
  if (moduleIdentifier) { // server build
    hook = function (context) {
      // 2.3 injection
      context =
        context || // cached call
        (this.$vnode && this.$vnode.ssrContext) || // stateful
        (this.parent && this.parent.$vnode && this.parent.$vnode.ssrContext) // functional
      // 2.2 with runInNewContext: true
      if (!context && typeof __VUE_SSR_CONTEXT__ !== 'undefined') {
        context = __VUE_SSR_CONTEXT__
      }
      // inject component styles
      if (injectStyles) {
        injectStyles.call(this, context)
      }
      // register component module identifier for async chunk inferrence
      if (context && context._registeredComponents) {
        context._registeredComponents.add(moduleIdentifier)
      }
    }
    // used by ssr in case component is cached and beforeCreate
    // never gets called
    options._ssrRegister = hook
  } else if (injectStyles) {
    hook = shadowMode
      ? function () {
        injectStyles.call(
          this,
          (options.functional ? this.parent : this).$root.$options.shadowRoot
        )
      }
      : injectStyles
  }

  if (hook) {
    if (options.functional) {
      // for template-only hot-reload because in that case the render fn doesn't
      // go through the normalizer
      options._injectStyles = hook
      // register for functional component in vue file
      var originalRender = options.render
      options.render = function renderWithStyleInjection (h, context) {
        hook.call(context)
        return originalRender(h, context)
      }
    } else {
      // inject component registration as beforeCreate hook
      var existing = options.beforeCreate
      options.beforeCreate = existing
        ? [].concat(existing, hook)
        : [hook]
    }
  }

  return {
    exports: scriptExports,
    options: options
  }
}


/***/ }),

/***/ "./resources/js/views/Home.vue":
/*!*************************************!*\
  !*** ./resources/js/views/Home.vue ***!
  \*************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _Home_vue_vue_type_template_id_63cd6604___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./Home.vue?vue&type=template&id=63cd6604& */ "./resources/js/views/Home.vue?vue&type=template&id=63cd6604&");
/* harmony import */ var _Home_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./Home.vue?vue&type=script&lang=js& */ "./resources/js/views/Home.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _Home_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _Home_vue_vue_type_template_id_63cd6604___WEBPACK_IMPORTED_MODULE_0__["render"],
  _Home_vue_vue_type_template_id_63cd6604___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/views/Home.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/views/Home.vue?vue&type=script&lang=js&":
/*!**************************************************************!*\
  !*** ./resources/js/views/Home.vue?vue&type=script&lang=js& ***!
  \**************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Home_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../node_modules/babel-loader/lib??ref--4-0!../../../node_modules/vue-loader/lib??vue-loader-options!./Home.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/Home.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Home_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/views/Home.vue?vue&type=template&id=63cd6604&":
/*!********************************************************************!*\
  !*** ./resources/js/views/Home.vue?vue&type=template&id=63cd6604& ***!
  \********************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Home_vue_vue_type_template_id_63cd6604___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../node_modules/vue-loader/lib??vue-loader-options!./Home.vue?vue&type=template&id=63cd6604& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/Home.vue?vue&type=template&id=63cd6604&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Home_vue_vue_type_template_id_63cd6604___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Home_vue_vue_type_template_id_63cd6604___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ })

}]);