(window["webpackJsonp"] = window["webpackJsonp"] || []).push([[8],{

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/ContactUs.vue?vue&type=template&id=420b99cf&":
/*!*******************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/views/ContactUs.vue?vue&type=template&id=420b99cf& ***!
  \*******************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _vm._m(0)
}
var staticRenderFns = [
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", [
      _c(
        "section",
        {
          staticClass: "parallax-window",
          attrs: {
            id: "short",
            "data-parallax": "scroll",
            "data-image-src": "img/sub_header_short.jpg",
            "data-natural-width": "1400",
            "data-natural-height": "350"
          }
        },
        [
          _c(
            "div",
            {
              staticStyle: { background: "url(img/sub_header_short.jpg)" },
              attrs: { id: "subheader" }
            },
            [
              _c("div", { staticClass: "overlay" }),
              _vm._v(" "),
              _c(
                "div",
                {
                  staticClass: "animated zoomIn",
                  attrs: { id: "sub_content" }
                },
                [
                  _c("h1", [_vm._v("Contacts")]),
                  _vm._v(" "),
                  _c("p", [
                    _vm._v(
                      "Qui debitis meliore ex, tollit debitis conclusionemque te eos."
                    )
                  ])
                ]
              )
            ]
          )
        ]
      ),
      _vm._v(" "),
      _c("div", { attrs: { id: "position" } }, [
        _c("div", { staticClass: "container" }, [
          _c("ul", [
            _c("li", [_c("a", { attrs: { href: "#0" } }, [_vm._v("Home")])]),
            _vm._v(" "),
            _c("li", [
              _c("a", { attrs: { href: "#0" } }, [_vm._v("Category")])
            ]),
            _vm._v(" "),
            _c("li", [_vm._v("Page active")])
          ]),
          _vm._v(" "),
          _c(
            "a",
            { staticClass: "search-overlay-menu-btn", attrs: { href: "#0" } },
            [_c("i", { staticClass: "icon-search-6" }), _vm._v(" Search")]
          )
        ])
      ]),
      _vm._v(" "),
      _c("div", { staticClass: "container margin_60_35" }, [
        _c("div", { staticClass: "row", attrs: { id: "contacts" } }, [
          _c("div", { staticClass: "col-md-6 col-sm-6" }, [
            _c("div", { staticClass: "box_style_2" }, [
              _c("h2", { staticClass: "inner" }, [_vm._v("Customer service")]),
              _vm._v(" "),
              _c("p", { staticClass: "add_bottom_30" }, [
                _vm._v(
                  "Adipisci conclusionemque ea duo, quo id fuisset prodesset, vis ea agam quas. "
                ),
                _c("strong", [_vm._v("Lorem iisque periculis")]),
                _vm._v(
                  " id vis, no eum utinam interesset. Quis voluptaria id per, an nibh atqui vix. Mei falli simul nusquam te."
                )
              ]),
              _vm._v(" "),
              _c("p", [
                _c(
                  "a",
                  {
                    staticClass: "phone",
                    attrs: { href: "tel://004542344599" }
                  },
                  [
                    _c("i", { staticClass: "icon-phone-circled" }),
                    _vm._v(" +45 423 445 70")
                  ]
                )
              ]),
              _vm._v(" "),
              _c("p", { staticClass: "nopadding" }, [
                _c(
                  "a",
                  { attrs: { href: "mailto:customercare@quickfood.com" } },
                  [
                    _c("i", { staticClass: "icon-mail-3" }),
                    _vm._v(" customercare@quickfood.com")
                  ]
                )
              ])
            ])
          ]),
          _vm._v(" "),
          _c("div", { staticClass: "col-md-6 col-sm-6" }, [
            _c("div", { staticClass: "box_style_2" }, [
              _c("h2", { staticClass: "inner" }, [
                _vm._v("Restaurant Support")
              ]),
              _vm._v(" "),
              _c("p", { staticClass: "add_bottom_30" }, [
                _vm._v(
                  "Quo ex rebum petentium, cum alia illud molestiae in, pro ea paulo gubergren. Ne case constituto pro, ex vis delenit complectitur, per ad "
                ),
                _c("strong", [_vm._v("everti timeam")]),
                _vm._v(
                  " conclusionemque. Quis voluptaria id per, an nibh atqui vix."
                )
              ]),
              _vm._v(" "),
              _c("p", [
                _c(
                  "a",
                  {
                    staticClass: "phone",
                    attrs: { href: "tel://004542344599" }
                  },
                  [
                    _c("i", { staticClass: "icon-phone-circled" }),
                    _vm._v(" 0544133397")
                  ]
                )
              ]),
              _vm._v(" "),
              _c("p", { staticClass: "nopadding" }, [
                _c("a", { attrs: { href: "mailto:support@cheezepizza.com" } }, [
                  _c("i", { staticClass: "icon-mail-3" }),
                  _vm._v(" support@cheezepizza.com")
                ])
              ])
            ])
          ])
        ])
      ])
    ])
  }
]
render._withStripped = true



/***/ }),

/***/ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js":
/*!********************************************************************!*\
  !*** ./node_modules/vue-loader/lib/runtime/componentNormalizer.js ***!
  \********************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return normalizeComponent; });
/* globals __VUE_SSR_CONTEXT__ */

// IMPORTANT: Do NOT use ES2015 features in this file (except for modules).
// This module is a runtime utility for cleaner component module output and will
// be included in the final webpack user bundle.

function normalizeComponent (
  scriptExports,
  render,
  staticRenderFns,
  functionalTemplate,
  injectStyles,
  scopeId,
  moduleIdentifier, /* server only */
  shadowMode /* vue-cli only */
) {
  // Vue.extend constructor export interop
  var options = typeof scriptExports === 'function'
    ? scriptExports.options
    : scriptExports

  // render functions
  if (render) {
    options.render = render
    options.staticRenderFns = staticRenderFns
    options._compiled = true
  }

  // functional template
  if (functionalTemplate) {
    options.functional = true
  }

  // scopedId
  if (scopeId) {
    options._scopeId = 'data-v-' + scopeId
  }

  var hook
  if (moduleIdentifier) { // server build
    hook = function (context) {
      // 2.3 injection
      context =
        context || // cached call
        (this.$vnode && this.$vnode.ssrContext) || // stateful
        (this.parent && this.parent.$vnode && this.parent.$vnode.ssrContext) // functional
      // 2.2 with runInNewContext: true
      if (!context && typeof __VUE_SSR_CONTEXT__ !== 'undefined') {
        context = __VUE_SSR_CONTEXT__
      }
      // inject component styles
      if (injectStyles) {
        injectStyles.call(this, context)
      }
      // register component module identifier for async chunk inferrence
      if (context && context._registeredComponents) {
        context._registeredComponents.add(moduleIdentifier)
      }
    }
    // used by ssr in case component is cached and beforeCreate
    // never gets called
    options._ssrRegister = hook
  } else if (injectStyles) {
    hook = shadowMode
      ? function () {
        injectStyles.call(
          this,
          (options.functional ? this.parent : this).$root.$options.shadowRoot
        )
      }
      : injectStyles
  }

  if (hook) {
    if (options.functional) {
      // for template-only hot-reload because in that case the render fn doesn't
      // go through the normalizer
      options._injectStyles = hook
      // register for functional component in vue file
      var originalRender = options.render
      options.render = function renderWithStyleInjection (h, context) {
        hook.call(context)
        return originalRender(h, context)
      }
    } else {
      // inject component registration as beforeCreate hook
      var existing = options.beforeCreate
      options.beforeCreate = existing
        ? [].concat(existing, hook)
        : [hook]
    }
  }

  return {
    exports: scriptExports,
    options: options
  }
}


/***/ }),

/***/ "./resources/js/views/ContactUs.vue":
/*!******************************************!*\
  !*** ./resources/js/views/ContactUs.vue ***!
  \******************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _ContactUs_vue_vue_type_template_id_420b99cf___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./ContactUs.vue?vue&type=template&id=420b99cf& */ "./resources/js/views/ContactUs.vue?vue&type=template&id=420b99cf&");
/* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");

var script = {}


/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_1__["default"])(
  script,
  _ContactUs_vue_vue_type_template_id_420b99cf___WEBPACK_IMPORTED_MODULE_0__["render"],
  _ContactUs_vue_vue_type_template_id_420b99cf___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/views/ContactUs.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/views/ContactUs.vue?vue&type=template&id=420b99cf&":
/*!*************************************************************************!*\
  !*** ./resources/js/views/ContactUs.vue?vue&type=template&id=420b99cf& ***!
  \*************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_ContactUs_vue_vue_type_template_id_420b99cf___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../node_modules/vue-loader/lib??vue-loader-options!./ContactUs.vue?vue&type=template&id=420b99cf& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/ContactUs.vue?vue&type=template&id=420b99cf&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_ContactUs_vue_vue_type_template_id_420b99cf___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_ContactUs_vue_vue_type_template_id_420b99cf___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ })

}]);