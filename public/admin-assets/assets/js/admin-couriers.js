
   var available_couriers = $('.available-couriers');
   var my_pending_couriers = $('.my-pending-couriers');
   var my_pending_incoming_couriers = $('.pending-incoming-couriers');
   var my_blocked_couriers = $('.my-blocked-couriers');
   var my_connected_couriers = $('.my-connected-couriers');
   var block_ele = $('.overlay-unblock');
   var branches_list = $('.branches-list');



   var branches_table = branches_list.DataTable({
    "ajax":{
        "url" : branches_list.attr('url'),
        "data" : function (data) {
            if(block_ele !== undefined){
                dismissLoader();
            }
            if(data !== 'undefined'){
                console.log(data);
            }
            return JSON.stringify(data);
        }
    },
    "columns": [
        { "data": "checkbox" },
        { "data": "name" },
        { "data": "region" },
        { "data": "place" }

    ],
    columnDefs: [ {
        orderable: false,
        className: 'select-checkbox',
        targets:   0
    } ],
    select: {
        style:    'multi',
        selector: 'td:first-child'
    }
});

   var unlinked_couriers = available_couriers.DataTable({
        "ajax":{
            "url" : available_couriers.attr('url'),
            "data" : function (data) {
                if(block_ele !== undefined){
                    dismissLoader();
                }
                if(data !== 'undefined'){
                    console.log(data);
                }
                return JSON.stringify(data);
            }
        },
        "columns": [
            { "data": "checkbox" },
            { "data": "image" },
            { "data": "name" },
            { "data": "email" },
            { "data" : "phone" },
            { "data" : "action" }

        ],columnDefs: [ {
            orderable: false,
            className: 'select-checkbox',
            targets:   0
        } ],
        select: {
            style:    'single',
            selector: 'td:first-child'
        }
    });

    // pending outgoing requests couriers init ##############
    var pending_table = my_pending_couriers.DataTable({
        "ajax":{
            "url" : my_pending_couriers.attr('url'),
            "data" : function (data) {
                if(block_ele !== undefined){
                    dismissLoader();
                }
                if(data !== 'undefined'){
                    console.log(data);
                    
                }
            }
        },
        "columns": [
            { "data": "checkbox" },
            { "data": "image" },
            { "data": "name" },
            { "data" : "associated_branch_name" },
            { "data" : "status"},
            { "data" : "action" }

        ],columnDefs: [ {
            orderable: false,
            className: 'select-checkbox',
            targets:   0
        } ],
        select: {
            style:    'multi',
            selector: 'td:first-child'
        },
        order: [[ 1, 'asc' ]],
        dom: 'Bfrtip',
        buttons: [
            'pageLength',
            {
                text: 'Select all',
                action: function () {
                    pending_table.rows().select();
                }
            },
            {
                text: 'Select none',
                action: function () {
                    pending_table.rows().deselect();
                }
            }
        ],
        className: 'my-1',
        lengthMenu: [
            [ 10, 25, 50, -1 ],
            [ '10 rows', '25 rows', '50 rows', 'Show all' ]
        ]
    });

// pending incoming requests lists
var pending_incoming_table = my_pending_incoming_couriers.DataTable({
    "ajax":{
        "url" : my_pending_incoming_couriers.attr('url'),
        "data" : function (data) {
            if(block_ele !== undefined){
                dismissLoader();
            }
            if(data !== 'undefined'){
                console.log(data);
                
            }
        }
    },
    "columns": [
        { "data": "checkbox" },
        { "data": "image" },
        { "data": "name" },
        { "data" : "associated_branch_name" },
        { "data" : "status"},
        { "data" : "action" }

    ],columnDefs: [ {
        orderable: false,
        className: 'select-checkbox',
        targets:   0
    } ],
    select: {
        style:    'multi',
        selector: 'td:first-child'
    },
    order: [[ 1, 'asc' ]],
    dom: 'Bfrtip',
    buttons: [
        'pageLength',
        {
            text: 'Select all',
            action: function () {
                pending_incoming_table.rows().select();
            }
        },
        {
            text: 'Select none',
            action: function () {
                pending_incoming_table.rows().deselect();
            }
        }
    ],
    className: 'my-1',
    lengthMenu: [
        [ 10, 25, 50, -1 ],
        [ '10 rows', '25 rows', '50 rows', 'Show all' ]
    ]
});
    
    // blocked courier services ###################

    var blocked_table = my_blocked_couriers.DataTable({
        "ajax":{
            "url" : my_blocked_couriers.attr('url'),
            "data" : function (data) {
                if(block_ele !== undefined){
                    dismissLoader();
                }
                if(data !== 'undefined'){
                    console.log(data);
                    
                }
            }
        },
        "columns": [
            { "data": "checkbox" },
            { "data": "image" },
            { "data": "name" },
            { "data" : "associated_branch_name" },
            { "data" : "status"},
            { "data" : "action" }

        ],columnDefs: [ {
            orderable: false,
            className: 'select-checkbox',
            targets:   0
        } ],
        select: {
            style:    'multi',
            selector: 'td:first-child'
        },
        order: [[ 1, 'asc' ]],
        dom: 'Bfrtip',
        buttons: [
            'pageLength',
            {
                text: 'Select all',
                action: function () {
                    blocked_table.rows().select();
                }
            },
            {
                text: 'Select none',
                action: function () {
                    blocked_table.rows().deselect();
                }
            }
        ],
        className: 'my-1',
        lengthMenu: [
            [ 10, 25, 50, -1 ],
            [ '10 rows', '25 rows', '50 rows', 'Show all' ]
        ]
    });

    // connected courier services ############
    var connected_table = my_connected_couriers.DataTable({
        "ajax":{
            "url" : my_connected_couriers.attr('url'),
            "data" : function (data) {
                if(block_ele !== undefined){
                    dismissLoader();
                }
                if(data !== 'undefined'){
                    console.log(data);
                    
                }
            }
        },
        "columns": [
            { "data": "checkbox" },
            { "data": "image" },
            { "data": "name" },
            { "data" : "associated_branch_name" },
            { "data" : "status"},
            { "data" : "action" }

        ],columnDefs: [ {
            orderable: false,
            className: 'select-checkbox',
            targets:   0
        } ],
        select: {
            style:    'multi',
            selector: 'td:first-child'
        },
        order: [[ 1, 'asc' ]],
        dom: 'Bfrtip',
        buttons: [
            'pageLength',
            {
                text: 'Select all',
                action: function () {
                    connected_table.rows().select();
                }
            },
            {
                text: 'Select none',
                action: function () {
                    connected_table.rows().deselect();
                }
            }
        ],
        className: 'my-1',
        lengthMenu: [
            [ 10, 25, 50, -1 ],
            [ '10 rows', '25 rows', '50 rows', 'Show all' ]
        ]
    });

  

    function loadTable() {
        $(block_ele).block({
            message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
            overlayCSS: {
                backgroundColor: '#fff',
                opacity: 0.8,
                cursor: 'wait'
            },
            css: {
                border: 0,
                padding: 0,
                backgroundColor: 'transparent'
            }
        });
    }
    
    function dismissLoader(){
        $(block_ele).unblock();
    }

    $('#connect-to-couriers').on('click', function(e){
        e.preventDefault();
        if(unlinked_couriers.rows('.selected').data().length < 1){
            swal("Whoops!", "You forgot to select the couriers");
            return;
        }

        var pData = unlinked_couriers.rows('.selected').data();
        console.log(pData);

        //loadTable();
        var jsonData = {};

        var html = '';

        var arr = Array("btn-success","btn-info","btn-warning","btn-danger","btn-dark ");
        var item = arr[Math.floor(Math.random()*arr.length)];


        for(var i = 0; i< pData.length; i++){
            jsonData[i] = pData[i];
            html += '<button type="button" class="btn '+item+' round btn-min-width mr-1 mb-1">'+jsonData[i].name+'</button>';
        }

        // we pick only on courier service and assign to many branches
        $('#selected_courier_id').val(jsonData[0].id);
        $('.selected-couriers').html(html);
        
        $('#chooseBranchesModal').modal('show');

        $('.connect-couriers-to-branches').click(function(e){
            e.preventDefault();
            var connect_url = $('#connect-to-couriers').attr('href');
            var courier_id = $('#selected_courier_id').val();

            if(branches_table.rows('.selected').data().length < 1){
                swal("Whoops!", "You forgot to select the branches");
                return;
            }

            var pData = branches_table.rows('.selected').data();
            console.log(pData);

            $('#chooseBranchesModal').modal('hide');

            loadTable();
            var jsonData = {};

            for(var i = 0; i< pData.length; i++){
                jsonData[i] = pData[i];
            }

            var postData = {
                '_token': _token,
                'data' : jsonData,
                'courier_id' : courier_id
            };


            $.post(connect_url, postData, function(result){
                    console.log(result);
                    reloadAllTables();
                    console.log("result is here ...");
                    successToast("Request sent to couriers");
            });

        });
    });

    function reloadAllTables(){
        unlinked_couriers.ajax.reload();
        pending_table.ajax.reload();
        blocked_table.ajax.reload();
        connected_table.ajax.reload();
        pending_incoming_table.ajax.reload();
    }


    // actions ---------------
    $('.incoming-connection-action').click(function(e){
        e.preventDefault();
        if(pending_incoming_table.rows('.selected').data().length < 1){
            swal("Whoops!", "You forgot to select the couriers");
            return;
        }

        var url = $(this).attr('href')
        var pData = pending_incoming_table.rows('.selected').data();


            loadTable();
            var jsonData = {};

            for(var i = 0; i< pData.length; i++){
                jsonData[i] = pData[i]['tId'];
            }

            var postData = {
                '_token': _token,
                'ids' : jsonData
            };

            console.log(postData);
            $.post(url, postData, function(result){
                reloadAllTables();
                successToast('Successful')
            });

    });

    $('.outgoing-connection-action').click(function(e){
        e.preventDefault();
        if(pending_table.rows('.selected').data().length < 1){
            swal("Whoops!", "You forgot to select the couriers");
            return;
        }

        var url = $(this).attr('href')
        var pData = pending_table.rows('.selected').data();

            loadTable();
            var jsonData = {};

            for(var i = 0; i< pData.length; i++){
                jsonData[i] = pData[i]['tId'];
            }

            var postData = {
                '_token': _token,
                'ids' : jsonData
            };

            console.log(postData);
            $.post(url, postData, function(result){
                reloadAllTables();
                successToast('Successful')
            });

    });

    $('.blocked-connection-action').click(function(e){
        e.preventDefault();
        if(blocked_table.rows('.selected').data().length < 1){
            swal("Whoops!", "You forgot to select the couriers");
            return;
        }

        var url = $(this).attr('href')
        var pData = blocked_table.rows('.selected').data();

            loadTable();
            var jsonData = {};

            for(var i = 0; i< pData.length; i++){
                jsonData[i] = pData[i]['tId'];
            }

            var postData = {
                '_token': _token,
                'ids' : jsonData
            };

            console.log(postData);
            $.post(url, postData, function(result){
                reloadAllTables();
                successToast('Successful')
            });

    });

    $('.accepted-connection-action').click(function(e){
        e.preventDefault();
        if(connected_table.rows('.selected').data().length < 1){
            swal("Whoops!", "You forgot to select the couriers");
            return;
        }

        var url = $(this).attr('href')
        var pData = connected_table.rows('.selected').data();

            loadTable();
            var jsonData = {};

            for(var i = 0; i< pData.length; i++){
                jsonData[i] = pData[i]['tId'];
            }

            var postData = {
                '_token': _token,
                'ids' : jsonData
            };

            console.log(postData);
            
            $.post(url, postData, function(result){
                reloadAllTables();
                successToast('Successful')
            });

    });


    function showCourierDetails(data,days){
       // console.log(data)
        document.getElementById('courier_name').innerText = data.name;
        document.getElementById('courier_email').innerHTML = '<a href="mailto:'+data.email+'">'+data.email+'</a>';
        document.getElementById('courier_phone').innerHTML = '<a href="tel:'+data.phone+'">'+data.phone+'</a>';

        $('#working_days').empty();

        for (var i = 0; i < days.length; i++) {
            $('#working_days').append('\n' +
    '                                    ' +
    '                                        <div class="custom-control custom-checkbox">\n' +
    '                                            <input type="checkbox" class="custom-control-input" checked="">\n' +
    '                                            <label class="custom-control-label" for="sms-channel"> '+days[i].name+'</label>\n' +
    '                                        </div>\n' +
    '                                    ')
        }
        console.log('days', days);


        $('#courierDetailModal').modal('show')
    }


   
