function errorToast(message) {
    toastr.error(message,"Notice!",{progressBar:!0})
}

function successToast(message) {
    toastr.success(message,"Notice!",{progressBar:!0})
}

function warningToast(message) {
    toastr.warning(message,"Notice!",{progressBar:!0})
}

function infoToast(message) {
    toastr.info(message,"Notice!",{progressBar:!0})
}
