// /////// orders manageent ----------------------
var block_ele = $('.overlay-unblock');
var eleOrderList = $('.order-list');
var table = eleOrderList.DataTable({
    
    "ajax":{
        "url" : eleOrderList.attr('url'),
        "data" : function (data) {
            if(block_ele !== undefined){
                dismissLoader();
            }
            if(data !== 'undefined'){
                console.log(data);
                return JSON.stringify(data);
            }
        }
    },
    "columns": [
        { "data": "checkbox" },
        { "data": "image" },
        { "data": "name" },
        { "data": "quantity" },
        { "data": "size" },
        { "data" : "price" },
        { "data" : "status" }

    ],columnDefs: [ {
        orderable: false,
        className: 'select-checkbox',
        targets:   0
    } ],
    select: {
        style:    'multi',
        selector: 'td:first-child'
    },
    order: [[ 1, 'asc' ]],
    dom: 'Bfrtip',
    buttons: [
        'pageLength',
        {
            text: 'Select all',
            action: function () {
                table.rows().select();
            }
        },
        {
            text: 'Select none',
            action: function () {
                table.rows().deselect();
            }
        }
    ],
    className: 'my-1',
    lengthMenu: [
        [ 10, 25, 50, -1 ],
        [ '10 rows', '25 rows', '50 rows', 'Show all' ]
    ]
});

function loadTable() {
    $(block_ele).block({
        message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
        overlayCSS: {
            backgroundColor: '#fff',
            opacity: 0.8,
            cursor: 'wait'
        },
        css: {
            border: 0,
            padding: 0,
            backgroundColor: 'transparent'
        }
    });
}

function dismissLoader(){
    $(block_ele).unblock();
}

$('.mark-items-as-paid').click(function(e){
    e.preventDefault();
    if(table.rows('.selected').data().length < 1){
        swal("Whoops!", "You forgot to select the orders");
        return;
    }

    var url = $(this).attr('href');
    swal({
        title: "Are you sure?",
        text: "You're manually marking selected items as paid",
        icon: "warning",
        buttons: {
            cancel: {
                text: "No, cancel!",
                value: null,
                visible: true,
                className: "",
                closeModal: true
            },
            confirm: {
                text: "Yes, Continue!",
                value: true,
                visible: true,
                className: "",
                closeModal: true
            }
        }
    }).then(function(isConfirm){
        if (isConfirm) {
                // excute marking of items as delivered
                var pData = table.rows('.selected').data();
                //console.log(pData);
                var jsonData = {};

                for(var i = 0; i< pData.length; i++){
                    jsonData[i] = pData[i];
                }

                var postData = {
                    '_token': _token,
                    'data' : jsonData,
                    'status' : 'paid'
                };
                
                loadTable()
                // console.log(jsonData);
                // return
                $.post(url, postData ,function (result) {
                    table.ajax.reload();
                    console.log("result is here ...");
                    successToast("Success");
                } );

        }else{
            // 
            swal("Cancelled", "action aborted :)", "error");
        }

    });

});




// assign selected items to couriers -----------------
$('.ask-customer-to-make-payment').click(function(e){
    e.preventDefault();

    if(table.rows('.selected').data().length < 1){
        swal("Whoops!", "You forgot to select the products");
        return;
    }

    var content = $('#payment_required').attr('data');
    $('#templateTextArea').html(content);
    $('#extra_data').val("make-payment")
    $('#chooseTemplate').modal('show');

});

$('.ask-customer-to-make-payment-to-download').click(function(e){
    e.preventDefault();

    if(table.rows('.selected').data().length < 1){
        swal("Whoops!", "You forgot to select the products");
        return;
    }

    var content = $('#download_material').attr('data');
    $('#templateTextArea').html(content);
    $('#extra_data').val("make-payment-download")
    $('#chooseTemplate').modal('show');

});

$('#proceed-with-message').click(function(e){

    e.preventDefault();

    if($('#templateTextArea').html() === ''){
        errorToast('please write a message')
        return
    }

    //// get selected items - done
    //// get message - done
    //// get message channels (sms /email) - done
    //// get customers - done

    $('#chooseTemplate').modal('hide');

    // excute marking of items as delivered
    var pData = table.rows('.selected').data();
    //console.log(pData);
    var jsonData = {};
    var recipientIds = {};

    for(var i = 0; i< pData.length; i++){
        jsonData[i] = pData[i];
    }

    var message = $('#templateTextArea').val();
    var useEmail = $('#email-channel').is(":checked");
    var useSms = $('#sms-channel').is(":checked")

    recipientIds[0] = $('#message-receipients').val();
    var postData = {
        '_token': _token,
        'productlist' : jsonData,
        'useEmail' : useEmail,
        'useSms' : useSms,
        'message' : message,
        'recipientIds' : recipientIds,
        'extraData' : $('#extra_data').val()
    };
    
    loadTable();

    // console.log(jsonData);
    var url =  $(this).attr('href');
    $.post(url, postData, function(result){
        dismissLoader();
       if(result.status){
           table.ajax.reload()
           successToast('Message sent!')
           return
       }

       errorToast('unable to send message. please try again!')
    });

});


$('#mark-as-delivered').on('click', function(e){
    e.preventDefault();
    // console.log(table.rows('.selected').data());
    if(table.rows('.selected').data().length < 1){
        swal("Whoops!", "You forgot to select the orders");
        return;
    }

    var url = $(this).attr('href');

    swal({
        title: "Are you sure?",
        text: "Mark selected items as delivered?",
        icon: "warning",
        buttons: {
            cancel: {
                text: "No, cancel!",
                value: null,
                visible: true,
                className: "",
                closeModal: true
            },
            confirm: {
                text: "Yes, Continue!",
                value: true,
                visible: true,
                className: "",
                closeModal: true
            }
        }
    }).then(function(isConfirm){
        if (isConfirm) {
            // excute marking of items as delivered
            var pData = table.rows('.selected').data();
            //console.log(pData);
            var jsonData = {};

            for(var i = 0; i< pData.length; i++){
                jsonData[i] = pData[i];
            }

            var postData = {
                '_token': _token,
                'data' : jsonData,
                'status' : 'delivered'
            };

            loadTable()
            // console.log(jsonData);
            // return
            $.post(url, postData ,function (result) {
                table.ajax.reload();
                console.log("result is here ...");
                successToast("Items delivered");
            } );

        }else{
            //
            swal("Cancelled", "action aborted :)", "error");
        }
    });

});

$('#cancel-selected').on('click', function(e){
    e.preventDefault();
    // console.log(table.rows('.selected').data());
    if(table.rows('.selected').data().length < 1){
        swal("Whoops!", "You forgot to select the orders");
        return;
    }

    var url = $(this).attr('href');

    swal({
        title: "Are you sure?",
        text: "Cancel selected orders?",
        icon: "warning",
        buttons: {
            cancel: {
                text: "No, cancel!",
                value: null,
                visible: true,
                className: "",
                closeModal: true
            },
            confirm: {
                text: "Yes, Continue!",
                value: true,
                visible: true,
                className: "",
                closeModal: true
            }
        }
    }).then(function(isConfirm){
        if (isConfirm) {
            // excute marking of items as delivered
            var pData = table.rows('.selected').data();
            //console.log(pData);
            var jsonData = {};

            for(var i = 0; i< pData.length; i++){
                jsonData[i] = pData[i];
            }

            var postData = {
                '_token': _token,
                'data' : jsonData
            };

            loadTable()
            // console.log(jsonData);
            // return
            $.post(url, postData ,function (result) {
                table.ajax.reload();
                console.log("result is here ...");

                successToast(result.message);
            } );

        }else{
            //
            swal("Cancelled", "action aborted :)", "error");
        }
    });

});

// mark orders as confirmed
$('#mark-as-confirmed').on('click', function(e){
    e.preventDefault();
    // console.log(table.rows('.selected').data());
    if(table.rows('.selected').data().length < 1){
        swal("Whoops!", "You forgot to select the orders");
        return;
    }

    var url = $(this).attr('href');

    swal({
        title: "Are you sure?",
        text: "Confirm the selected orders?",
        icon: "warning",
        buttons: {
            cancel: {
                text: "No, cancel!",
                value: null,
                visible: true,
                className: "",
                closeModal: true
            },
            confirm: {
                text: "Yes, Continue!",
                value: true,
                visible: true,
                className: "",
                closeModal: true
            }
        }
    }).then(function(isConfirm){
        if (isConfirm) {
            // excute marking of items as delivered
            var pData = table.rows('.selected').data();
            //console.log(pData);
            var jsonData = {};

            for(var i = 0; i< pData.length; i++){
                jsonData[i] = pData[i];
            }

            var postData = {
                '_token': _token,
                'data' : jsonData,
                'status' : 'delivered'
            };

            loadTable()
            // console.log(jsonData);
            // return
            $.post(url, postData ,function (result) {
                table.ajax.reload();
                console.log("result is here ...");
                successToast("Orders Confirmed");
            } );

        }else{
            //
            swal("Cancelled", "action aborted :)", "error");
        }
    });

});

// mark orders as shipped
$('#mark-as-shipped').on('click', function(e){
    e.preventDefault();
    // console.log(table.rows('.selected').data());
    if(table.rows('.selected').data().length < 1){
        swal("Whoops!", "You forgot to select the orders");
        return;
    }

    var url = $(this).attr('href');

    swal({
        title: "Are you sure?",
        text: "Mark selected items as shipped?",
        icon: "warning",
        buttons: {
            cancel: {
                text: "No, cancel!",
                value: null,
                visible: true,
                className: "",
                closeModal: true
            },
            confirm: {
                text: "Yes, Continue!",
                value: true,
                visible: true,
                className: "",
                closeModal: true
            }
        }
    }).then(function(isConfirm){
        if (isConfirm) {
            // excute marking of items as delivered
            var pData = table.rows('.selected').data();
            //console.log(pData);
            var jsonData = {};

            for(var i = 0; i< pData.length; i++){
                jsonData[i] = pData[i];
            }

            var postData = {
                '_token': _token,
                'data' : jsonData,
                'status' : 'delivered'
            };

            loadTable()
            // console.log(jsonData);
            // return
            $.post(url, postData ,function (result) {
                table.ajax.reload();
                console.log("result is here ...");
                successToast("Items delivered");
            } );

        }else{
            //
            swal("Cancelled", "action aborted :)", "error");
        }
    });

});


