(window["webpackJsonp"] = window["webpackJsonp"] || []).push([[6],{

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/order.component.vue?vue&type=script&lang=js&":
/*!**************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/order.component.vue?vue&type=script&lang=js& ***!
  \**************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
//
//
//
//
//
//
//
//
//
//
//
/* harmony default export */ __webpack_exports__["default"] = ({
  name: 'order-item',
  props: {
    order: {
      type: Object
    }
  },
  methods: {
    removeOrderItem: function removeOrderItem() {
      var orderId = this.order.orderId;
      this.$emit('removeOrderItem', orderId);
    }
  },
  computed: {
    getOrderName: function getOrderName() {
      if (this.order.additional) {
        return this.order.name = "".concat(this.order.name, "(").concat(this.order.additional.name, ")");
      }

      return this.order.name;
    },
    getOrderPrice: function getOrderPrice() {
      if (this.order.additional) {
        return this.order.price + this.order.additional.price;
      }

      return this.order.price;
    }
  }
});

/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/orders.list.component.vue?vue&type=script&lang=js&":
/*!********************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/orders.list.component.vue?vue&type=script&lang=js& ***!
  \********************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _order_component__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./order.component */ "./resources/js/components/order.component.vue");
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

/* harmony default export */ __webpack_exports__["default"] = ({
  name: 'orders-list',
  data: function data() {
    return {
      enableOdDisablebtn: true
    };
  },
  components: {
    Order: _order_component__WEBPACK_IMPORTED_MODULE_0__["default"]
  },
  props: {
    orders: Array
  },
  methods: {
    removeOrderItem: function removeOrderItem(orderId) {
      this.$emit('removeOrderItem', orderId);
    }
  },
  computed: {
    getSubTotal: function getSubTotal() {
      return this.orders.reduce(function (a, b) {
        return a + b.price * b.quantity;
      }, 0);
    },
    getTotal: function getTotal() {
      return this.getSubTotal + 10;
    }
  }
});

/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/order.component.vue?vue&type=template&id=ca77073c&":
/*!******************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/order.component.vue?vue&type=template&id=ca77073c& ***!
  \******************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("tr", [
    _c("td", [
      _c(
        "a",
        {
          staticClass: "remove_item",
          attrs: { href: "#0" },
          on: { click: _vm.removeOrderItem }
        },
        [_c("i", { staticClass: "icon_minus_alt" })]
      ),
      _vm._v(" "),
      _c("strong", [_vm._v(_vm._s(_vm.order.quantity) + "x")]),
      _vm._v(" " + _vm._s(_vm.getOrderName) + "\r\n    ")
    ]),
    _vm._v(" "),
    _c("td", [
      _c("strong", { staticClass: "pull-right" }, [
        _vm._v("₵" + _vm._s(_vm.getOrderPrice))
      ])
    ])
  ])
}
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/orders.list.component.vue?vue&type=template&id=65a2e59b&":
/*!************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/orders.list.component.vue?vue&type=template&id=65a2e59b& ***!
  \************************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "div",
    {
      staticClass: "col-md-3",
      staticStyle: {
        position: "relative",
        overflow: "visible",
        "box-sizing": "border-box",
        "min-height": "1px"
      },
      attrs: { id: "sidebar" }
    },
    [
      _c(
        "div",
        {
          staticClass: "theiaStickySidebar",
          staticStyle: {
            "padding-top": "0px",
            "padding-bottom": "1px",
            position: "static",
            top: "80px",
            left: "1048.3px"
          }
        },
        [
          _c(
            "div",
            { attrs: { id: "cart_box" } },
            [
              _vm._m(0),
              _vm._v(" "),
              _c("table", { staticClass: "table table_summary" }, [
                _c(
                  "tbody",
                  [
                    _vm._l(_vm.orders, function(order, index) {
                      return _c("Order", {
                        key: index,
                        attrs: { order: order },
                        on: {
                          removeOrderItem: function($event) {
                            return _vm.removeOrderItem($event)
                          }
                        }
                      })
                    }),
                    _vm._v(" "),
                    !_vm.orders.length
                      ? _c(
                          "span",
                          {
                            staticClass: "text-center",
                            staticStyle: { width: "100%" }
                          },
                          [_vm._v("You have no orders")]
                        )
                      : _vm._e()
                  ],
                  2
                )
              ]),
              _vm._v(" "),
              _c("hr"),
              _vm._v(" "),
              _vm._m(1),
              _vm._v(" "),
              _c("hr"),
              _vm._v(" "),
              _c("table", { staticClass: "table table_summary" }, [
                _c("tbody", [
                  _c("tr", [
                    _c("td", [
                      _vm._v("\r\n                            Subtotal "),
                      this.orders.length
                        ? _c("span", { staticClass: "pull-right" }, [
                            _vm._v("₵" + _vm._s(_vm.getSubTotal))
                          ])
                        : _c("span", { staticClass: "pull-right" }, [
                            _vm._v("----")
                          ])
                    ])
                  ]),
                  _vm._v(" "),
                  _vm._m(2),
                  _vm._v(" "),
                  _c("tr", [
                    _c("td", { staticClass: "total" }, [
                      _vm._v("\r\n                            TOTAL "),
                      this.orders.length
                        ? _c("span", { staticClass: "pull-right" }, [
                            _vm._v("₵" + _vm._s(_vm.getTotal))
                          ])
                        : _c("span", { staticClass: "pull-right" }, [
                            _vm._v("----")
                          ])
                    ])
                  ])
                ])
              ]),
              _vm._v(" "),
              _c("hr"),
              _vm._v(" "),
              _vm._t("default")
            ],
            2
          )
        ]
      )
    ]
  )
}
var staticRenderFns = [
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("h3", [
      _vm._v("Your order "),
      _c("i", { staticClass: "icon_cart_alt pull-right" })
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "row", attrs: { id: "options_2" } }, [
      _c("div", { staticClass: "col-lg-6 col-md-12 col-sm-12 col-xs-6" }, [
        _c("label", [
          _c("input", {
            staticClass: "icheck",
            attrs: { type: "radio", value: "", checked: "", name: "option_2" }
          }),
          _vm._v("Delivery")
        ])
      ]),
      _vm._v(" "),
      _c("div", { staticClass: "col-lg-6 col-md-12 col-sm-12 col-xs-6" }, [
        _c("label", [
          _c("input", {
            staticClass: "icheck",
            attrs: { type: "radio", value: "", name: "option_2" }
          }),
          _vm._v("Take Away")
        ])
      ])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("tr", [
      _c("td", [
        _vm._v("\r\n                            Delivery fee "),
        _c("span", { staticClass: "pull-right" }, [_vm._v("₵10")])
      ])
    ])
  }
]
render._withStripped = true



/***/ }),

/***/ "./resources/js/components/order.component.vue":
/*!*****************************************************!*\
  !*** ./resources/js/components/order.component.vue ***!
  \*****************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _order_component_vue_vue_type_template_id_ca77073c___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./order.component.vue?vue&type=template&id=ca77073c& */ "./resources/js/components/order.component.vue?vue&type=template&id=ca77073c&");
/* harmony import */ var _order_component_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./order.component.vue?vue&type=script&lang=js& */ "./resources/js/components/order.component.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _order_component_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _order_component_vue_vue_type_template_id_ca77073c___WEBPACK_IMPORTED_MODULE_0__["render"],
  _order_component_vue_vue_type_template_id_ca77073c___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/components/order.component.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/components/order.component.vue?vue&type=script&lang=js&":
/*!******************************************************************************!*\
  !*** ./resources/js/components/order.component.vue?vue&type=script&lang=js& ***!
  \******************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_order_component_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../node_modules/babel-loader/lib??ref--4-0!../../../node_modules/vue-loader/lib??vue-loader-options!./order.component.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/order.component.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_order_component_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/components/order.component.vue?vue&type=template&id=ca77073c&":
/*!************************************************************************************!*\
  !*** ./resources/js/components/order.component.vue?vue&type=template&id=ca77073c& ***!
  \************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_order_component_vue_vue_type_template_id_ca77073c___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../node_modules/vue-loader/lib??vue-loader-options!./order.component.vue?vue&type=template&id=ca77073c& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/order.component.vue?vue&type=template&id=ca77073c&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_order_component_vue_vue_type_template_id_ca77073c___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_order_component_vue_vue_type_template_id_ca77073c___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ }),

/***/ "./resources/js/components/orders.list.component.vue":
/*!***********************************************************!*\
  !*** ./resources/js/components/orders.list.component.vue ***!
  \***********************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _orders_list_component_vue_vue_type_template_id_65a2e59b___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./orders.list.component.vue?vue&type=template&id=65a2e59b& */ "./resources/js/components/orders.list.component.vue?vue&type=template&id=65a2e59b&");
/* harmony import */ var _orders_list_component_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./orders.list.component.vue?vue&type=script&lang=js& */ "./resources/js/components/orders.list.component.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _orders_list_component_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _orders_list_component_vue_vue_type_template_id_65a2e59b___WEBPACK_IMPORTED_MODULE_0__["render"],
  _orders_list_component_vue_vue_type_template_id_65a2e59b___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/components/orders.list.component.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/components/orders.list.component.vue?vue&type=script&lang=js&":
/*!************************************************************************************!*\
  !*** ./resources/js/components/orders.list.component.vue?vue&type=script&lang=js& ***!
  \************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_orders_list_component_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../node_modules/babel-loader/lib??ref--4-0!../../../node_modules/vue-loader/lib??vue-loader-options!./orders.list.component.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/orders.list.component.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_orders_list_component_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/components/orders.list.component.vue?vue&type=template&id=65a2e59b&":
/*!******************************************************************************************!*\
  !*** ./resources/js/components/orders.list.component.vue?vue&type=template&id=65a2e59b& ***!
  \******************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_orders_list_component_vue_vue_type_template_id_65a2e59b___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../node_modules/vue-loader/lib??vue-loader-options!./orders.list.component.vue?vue&type=template&id=65a2e59b& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/orders.list.component.vue?vue&type=template&id=65a2e59b&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_orders_list_component_vue_vue_type_template_id_65a2e59b___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_orders_list_component_vue_vue_type_template_id_65a2e59b___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ })

}]);