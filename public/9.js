(window["webpackJsonp"] = window["webpackJsonp"] || []).push([[9],{

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/Products.vue?vue&type=script&lang=js&":
/*!**************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/views/Products.vue?vue&type=script&lang=js& ***!
  \**************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _components_orders_list_component__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../components/orders.list.component */ "./resources/js/components/orders.list.component.vue");
function _toConsumableArray(arr) { return _arrayWithoutHoles(arr) || _iterableToArray(arr) || _unsupportedIterableToArray(arr) || _nonIterableSpread(); }

function _nonIterableSpread() { throw new TypeError("Invalid attempt to spread non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); }

function _iterableToArray(iter) { if (typeof Symbol !== "undefined" && Symbol.iterator in Object(iter)) return Array.from(iter); }

function _arrayWithoutHoles(arr) { if (Array.isArray(arr)) return _arrayLikeToArray(arr); }

function _createForOfIteratorHelper(o, allowArrayLike) { var it; if (typeof Symbol === "undefined" || o[Symbol.iterator] == null) { if (Array.isArray(o) || (it = _unsupportedIterableToArray(o)) || allowArrayLike && o && typeof o.length === "number") { if (it) o = it; var i = 0; var F = function F() {}; return { s: F, n: function n() { if (i >= o.length) return { done: true }; return { done: false, value: o[i++] }; }, e: function e(_e) { throw _e; }, f: F }; } throw new TypeError("Invalid attempt to iterate non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); } var normalCompletion = true, didErr = false, err; return { s: function s() { it = o[Symbol.iterator](); }, n: function n() { var step = it.next(); normalCompletion = step.done; return step; }, e: function e(_e2) { didErr = true; err = _e2; }, f: function f() { try { if (!normalCompletion && it["return"] != null) it["return"](); } finally { if (didErr) throw err; } } }; }

function _unsupportedIterableToArray(o, minLen) { if (!o) return; if (typeof o === "string") return _arrayLikeToArray(o, minLen); var n = Object.prototype.toString.call(o).slice(8, -1); if (n === "Object" && o.constructor) n = o.constructor.name; if (n === "Map" || n === "Set") return Array.from(o); if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen); }

function _arrayLikeToArray(arr, len) { if (len == null || len > arr.length) len = arr.length; for (var i = 0, arr2 = new Array(len); i < len; i++) { arr2[i] = arr[i]; } return arr2; }

//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

/* harmony default export */ __webpack_exports__["default"] = ({
  data: function data() {
    return {
      categories: [],
      branchId: '',
      branchName: '',
      location: '',
      open_time: '',
      orders: [],
      enableOdDisablebtn: false
    };
  },
  components: {
    OrdersList: _components_orders_list_component__WEBPACK_IMPORTED_MODULE_0__["default"]
  },
  methods: {
    GetAllCategoriesAndProducts: function GetAllCategoriesAndProducts() {
      var _this = this;

      axios.get('/api/v1/branch/' + this.branchId + '/products').then(function (response) {
        // handle success
        // console.log(response.data, 'untamed')
        // console.log("My data :" + response.data.data, ' full auto')
        var datalist = response.data['data']; // console.log(datalist, ' All data, its type: ', typeof datalist)

        if (response.data.status) {
          // console.log(" Categories :" + response.data.data);
          var _iterator = _createForOfIteratorHelper(datalist),
              _step;

          try {
            for (_iterator.s(); !(_step = _iterator.n()).done;) {
              var lit = _step.value;

              // console.log(lit, 'came here continue')
              _this.categories.push(lit);
            }
          } catch (err) {
            _iterator.e(err);
          } finally {
            _iterator.f();
          }
        } else {
          console.log("empty " + response.data);
        }
      })["catch"](function (error) {
        // handle error
        console.log(error);
      });
    },
    GetQuantityOrSize: function GetQuantityOrSize(size, product) {
      if (size <= 0) {
        this.GetProductQuantity(product);
      }
    },
    GetProductQuantity: function GetProductQuantity(product) {
      var _this2 = this;

      swal.fire({
        title: 'Choose Quantity',
        input: 'number',
        inputAttributes: {
          autocapitalize: 'off'
        },
        showCancelButton: true,
        confirmButtonText: 'confirm',
        showLoaderOnConfirm: true,
        allowOutsideClick: function allowOutsideClick() {
          return !swal.isLoading();
        }
      }).then(function (result) {
        if (result.isConfirmed) {
          if (result.value < 1) {
            result.value = 1;
          }

          var quantity = result.value;

          if (product) {
            var storedOrdersData = localStorage.getItem('orders');
            var orderData = {
              orderId: product.id,
              name: product.name,
              quantity: quantity,
              price: product.price,
              additional: product.additional
            };

            _this2.orders.push(orderData);

            if (storedOrdersData) {
              var storeOrders = JSON.parse(storedOrdersData);
              storeOrders = [].concat(_toConsumableArray(storeOrders), [orderData]);
              localStorage.setItem('orders', JSON.stringify(storeOrders));
            } else {
              localStorage.setItem('orders', JSON.stringify([orderData]));
            }
          }
        }
      });
    },
    removeOrderItem: function removeOrderItem(orderId) {
      var otherOrders = this.orders.filter(function (order) {
        return order.orderId != orderId;
      });
      this.orders = _toConsumableArray(otherOrders);
      var storedOrdersData = localStorage.getItem('orders'); // nice one last issue when u dont add any quantity it adds it to the cart eg

      if (storedOrdersData) {
        localStorage.setItem('orders', JSON.stringify(this.orders));
      }
    },
    getAdditionalData: function getAdditionalData(size) {
      console.log(size);
    },
    prepAdditionalData: function prepAdditionalData(e, size, product) {
      console.log(size);
      var name = size.name;
      var price = size.pivot.additional_price;
      product.additional = {
        name: name,
        price: price
      };
    }
  },
  created: function created() {
    this.branchId = localStorage.getItem('branchId');
    console.log("My branch Id :", this.branchId);
    this.branchName = localStorage.getItem('branchName');
    this.location = localStorage.getItem('location');
    this.open_time = localStorage.getItem('openingTime');
    this.GetAllCategoriesAndProducts();
    var storedOrdersData = localStorage.getItem('orders');

    if (storedOrdersData) {
      var storeOrders = JSON.parse(storedOrdersData);
      this.orders = storeOrders;
    }
  }
});

/***/ }),

/***/ "./node_modules/css-loader/index.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/Products.vue?vue&type=style&index=0&id=eec6f8fa&scoped=true&lang=css&":
/*!*********************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/css-loader??ref--6-1!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src??ref--6-2!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/views/Products.vue?vue&type=style&index=0&id=eec6f8fa&scoped=true&lang=css& ***!
  \*********************************************************************************************************************************************************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(/*! ../../../node_modules/css-loader/lib/css-base.js */ "./node_modules/css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "\n.show[data-v-eec6f8fa]{\nheight: 180px;\nposition: absolute;\n}\n", ""]);

// exports


/***/ }),

/***/ "./node_modules/style-loader/index.js!./node_modules/css-loader/index.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/Products.vue?vue&type=style&index=0&id=eec6f8fa&scoped=true&lang=css&":
/*!*************************************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/style-loader!./node_modules/css-loader??ref--6-1!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src??ref--6-2!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/views/Products.vue?vue&type=style&index=0&id=eec6f8fa&scoped=true&lang=css& ***!
  \*************************************************************************************************************************************************************************************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {


var content = __webpack_require__(/*! !../../../node_modules/css-loader??ref--6-1!../../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../../node_modules/postcss-loader/src??ref--6-2!../../../node_modules/vue-loader/lib??vue-loader-options!./Products.vue?vue&type=style&index=0&id=eec6f8fa&scoped=true&lang=css& */ "./node_modules/css-loader/index.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/Products.vue?vue&type=style&index=0&id=eec6f8fa&scoped=true&lang=css&");

if(typeof content === 'string') content = [[module.i, content, '']];

var transform;
var insertInto;



var options = {"hmr":true}

options.transform = transform
options.insertInto = undefined;

var update = __webpack_require__(/*! ../../../node_modules/style-loader/lib/addStyles.js */ "./node_modules/style-loader/lib/addStyles.js")(content, options);

if(content.locals) module.exports = content.locals;

if(false) {}

/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/Products.vue?vue&type=template&id=eec6f8fa&scoped=true&":
/*!******************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/views/Products.vue?vue&type=template&id=eec6f8fa&scoped=true& ***!
  \******************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("div", [
    _c(
      "section",
      {
        staticClass: "parallax-window",
        attrs: {
          "data-parallax": "scroll",
          "data-image-src": "img/sub_header_2.jpg",
          "data-natural-width": "1400",
          "data-natural-height": "470"
        }
      },
      [
        _c(
          "div",
          {
            staticStyle: { background: "url(img/sub_header_cart.jpg)" },
            attrs: { id: "subheader" }
          },
          [
            _c("div", { staticClass: "overlay" }),
            _vm._v(" "),
            _c(
              "div",
              { staticClass: "animated zoomIn", attrs: { id: "sub_content" } },
              [
                _vm._m(0),
                _vm._v(" "),
                _vm._m(1),
                _vm._v(" "),
                _c("h1", [_vm._v(_vm._s(_vm.branchName))]),
                _vm._v(" "),
                _c("div", [_c("em", [_vm._v(_vm._s(_vm.branchId))])]),
                _vm._v(" "),
                _c("div", [
                  _c("i", { staticClass: "icon_pin" }),
                  _vm._v(_vm._s(_vm.location))
                ])
              ]
            )
          ]
        )
      ]
    ),
    _vm._v(" "),
    _vm._m(2),
    _vm._v(" "),
    _c(
      "div",
      {
        staticClass: "container margin_60_35",
        staticStyle: { transform: "none" }
      },
      [
        _c(
          "div",
          { staticClass: "row", staticStyle: { transform: "none" } },
          [
            _c("div", { staticClass: "col-md-3" }, [
              _vm._m(3),
              _vm._v(" "),
              _vm.categories
                ? _c(
                    "div",
                    { staticClass: "box_style_1" },
                    _vm._l(_vm.categories, function(cat) {
                      return _c(
                        "ul",
                        { key: cat.id, attrs: { id: "cat_nav" } },
                        [
                          _c("li", [
                            _c(
                              "a",
                              {
                                staticClass: "active",
                                attrs: { href: "#" + cat.id }
                              },
                              [
                                _vm._v(_vm._s(cat.name) + " "),
                                _c("span", [
                                  _vm._v(
                                    "( " + _vm._s(cat.products.length) + ")"
                                  )
                                ])
                              ]
                            )
                          ])
                        ]
                      )
                    }),
                    0
                  )
                : _vm._e(),
              _vm._v(" "),
              _c(
                "div",
                { staticClass: "box_style_2 hidden-xs", attrs: { id: "help" } },
                [
                  _c("i", { staticClass: "icon_lifesaver" }),
                  _vm._v(" "),
                  _vm._m(4),
                  _vm._v(" "),
                  _c("a", { staticClass: "phone", attrs: { href: "#" } }, [
                    _vm._v("0544133397")
                  ]),
                  _vm._v(" "),
                  _c("small", [_vm._v("opens at " + _vm._s(_vm.open_time))])
                ]
              )
            ]),
            _vm._v(" "),
            _c("div", { staticClass: "col-md-6" }, [
              _c(
                "div",
                { staticClass: "box_style_2", attrs: { id: "main_menu" } },
                [
                  _c("h2", { staticClass: "inner" }, [_vm._v("Menu")]),
                  _vm._v(" "),
                  _vm._l(_vm.categories, function(cat) {
                    return _c("div", { key: cat.id }, [
                      _c(
                        "h3",
                        { staticClass: "nomargin_top", attrs: { id: cat.id } },
                        [_vm._v(_vm._s(cat.name))]
                      ),
                      _vm._v(" "),
                      _c("p", [
                        _vm._v(
                          "\n                            Te ferri iisque aliquando pro, posse nonumes efficiantur in cum. Sensibus reprimique eu pro. Fuisset mentitum deleniti sit ea.\n                        "
                        )
                      ]),
                      _vm._v(" "),
                      _c(
                        "table",
                        { staticClass: "table table-striped cart-list" },
                        [
                          _vm._m(5, true),
                          _vm._v(" "),
                          _c(
                            "tbody",
                            _vm._l(cat.products, function(product) {
                              return _c("tr", { key: product.id }, [
                                _c("td", [
                                  _vm._m(6, true),
                                  _vm._v(" "),
                                  _c("h5", [
                                    _vm._v(" " + _vm._s(product.name))
                                  ]),
                                  _vm._v(" "),
                                  _c("p", [
                                    _vm._v(
                                      "\n                                            " +
                                        _vm._s(product.description) +
                                        "\n                                        "
                                    )
                                  ])
                                ]),
                                _vm._v(" "),
                                _c("td", [
                                  _c("strong", [
                                    _vm._v("₵" + _vm._s(product.price))
                                  ])
                                ]),
                                _vm._v(" "),
                                _c("td", { staticClass: "options" }, [
                                  _c(
                                    "div",
                                    {
                                      staticClass:
                                        "dropdown dropdown-options dropup"
                                    },
                                    [
                                      product.sizes
                                        ? _c(
                                            "a",
                                            {
                                              staticClass: "dropdown-toggle",
                                              attrs: {
                                                href: "#",
                                                "data-toggle": "dropdown",
                                                "aria-expanded": "true"
                                              },
                                              on: {
                                                click: function($event) {
                                                  return _vm.GetQuantityOrSize(
                                                    product.sizes.length,
                                                    product
                                                  )
                                                }
                                              }
                                            },
                                            [
                                              _c("i", {
                                                staticClass: "icon_plus_alt2"
                                              })
                                            ]
                                          )
                                        : _vm._e(),
                                      _vm._v(" "),
                                      product.sizes.length > 0
                                        ? _c(
                                            "div",
                                            { staticClass: "dropdown-menu" },
                                            [
                                              _c("h5", [
                                                _vm._v("Select an option")
                                              ]),
                                              _vm._v(" "),
                                              _vm._l(product.sizes, function(
                                                size
                                              ) {
                                                return _c(
                                                  "div",
                                                  { key: size.id },
                                                  [
                                                    _c("label", [
                                                      _c("input", {
                                                        attrs: {
                                                          type: "radio",
                                                          value: "option1",
                                                          name: "options_1",
                                                          checked: ""
                                                        },
                                                        on: {
                                                          change: function(
                                                            $event
                                                          ) {
                                                            return _vm.prepAdditionalData(
                                                              $event,
                                                              size,
                                                              product
                                                            )
                                                          }
                                                        }
                                                      }),
                                                      _vm._v(
                                                        _vm._s(size.name) + " "
                                                      ),
                                                      _c("span", [
                                                        _vm._v(
                                                          " ₵ " +
                                                            _vm._s(
                                                              product.price +
                                                                size.pivot
                                                                  .additional_price
                                                            ) +
                                                            " "
                                                        )
                                                      ])
                                                    ])
                                                  ]
                                                )
                                              }),
                                              _vm._v(" "),
                                              _c(
                                                "a",
                                                {
                                                  staticClass: "add_to_basket",
                                                  attrs: { href: "#0" },
                                                  on: {
                                                    click: function($event) {
                                                      return _vm.GetProductQuantity(
                                                        product
                                                      )
                                                    }
                                                  }
                                                },
                                                [_vm._v("Add to cart")]
                                              )
                                            ],
                                            2
                                          )
                                        : _vm._e()
                                    ]
                                  )
                                ])
                              ])
                            }),
                            0
                          )
                        ]
                      ),
                      _vm._v(" "),
                      _c("hr")
                    ])
                  })
                ],
                2
              )
            ]),
            _vm._v(" "),
            _c(
              "OrdersList",
              {
                attrs: { orders: _vm.orders },
                on: {
                  removeOrderItem: function($event) {
                    return _vm.removeOrderItem($event)
                  }
                }
              },
              [
                _c(
                  "a",
                  {
                    staticClass: "btn_full",
                    attrs: { href: "/carts", disabled: _vm.enableOdDisablebtn }
                  },
                  [_vm._v("Order now")]
                )
              ]
            )
          ],
          1
        )
      ]
    )
  ])
}
var staticRenderFns = [
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { attrs: { id: "thumb" } }, [
      _c("img", { attrs: { src: "img/thumb_restaurant.jpg", alt: "" } })
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "rating" }, [
      _c("i", { staticClass: "icon_star voted" }),
      _c("i", { staticClass: "icon_star voted" }),
      _c("i", { staticClass: "icon_star voted" }),
      _c("i", { staticClass: "icon_star voted" }),
      _c("i", { staticClass: "icon_star" }),
      _vm._v(" ("),
      _c("small", [
        _c("a", { attrs: { href: "#" } }, [_vm._v("Read 98 reviews")])
      ]),
      _vm._v(")")
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { attrs: { id: "position" } }, [
      _c("div", { staticClass: "container" }, [
        _c("ul", [
          _c("li", [_c("a", { attrs: { href: "#0" } }, [_vm._v("Home")])]),
          _vm._v(" "),
          _c("li", [_c("a", { attrs: { href: "#0" } }, [_vm._v("Category")])]),
          _vm._v(" "),
          _c("li", [_vm._v("Page active")])
        ]),
        _vm._v(" "),
        _c(
          "a",
          { staticClass: "search-overlay-menu-btn", attrs: { href: "#0" } },
          [_c("i", { staticClass: "icon-search-6" }), _vm._v(" Search")]
        )
      ])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("p", [
      _c("a", { staticClass: "btn_side", attrs: { href: "#0" } }, [
        _vm._v("Back to search")
      ])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("h4", [_vm._v("Need "), _c("span", [_vm._v("Help?")])])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("thead", [
      _c("tr", [
        _c("th", [
          _vm._v(
            "\n                                        Item\n                                    "
          )
        ]),
        _vm._v(" "),
        _c("th", [
          _vm._v(
            "\n                                        Price\n                                    "
          )
        ]),
        _vm._v(" "),
        _c("th", [
          _vm._v(
            "\n                                        Order\n                                    "
          )
        ])
      ])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("figure", { staticClass: "thumb_menu_list" }, [
      _c("img", { attrs: { src: "img/menu-thumb-1.jpg", alt: "thumb" } })
    ])
  }
]
render._withStripped = true



/***/ }),

/***/ "./resources/js/views/Products.vue":
/*!*****************************************!*\
  !*** ./resources/js/views/Products.vue ***!
  \*****************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _Products_vue_vue_type_template_id_eec6f8fa_scoped_true___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./Products.vue?vue&type=template&id=eec6f8fa&scoped=true& */ "./resources/js/views/Products.vue?vue&type=template&id=eec6f8fa&scoped=true&");
/* harmony import */ var _Products_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./Products.vue?vue&type=script&lang=js& */ "./resources/js/views/Products.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _Products_vue_vue_type_style_index_0_id_eec6f8fa_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./Products.vue?vue&type=style&index=0&id=eec6f8fa&scoped=true&lang=css& */ "./resources/js/views/Products.vue?vue&type=style&index=0&id=eec6f8fa&scoped=true&lang=css&");
/* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");






/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_3__["default"])(
  _Products_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _Products_vue_vue_type_template_id_eec6f8fa_scoped_true___WEBPACK_IMPORTED_MODULE_0__["render"],
  _Products_vue_vue_type_template_id_eec6f8fa_scoped_true___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  "eec6f8fa",
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/views/Products.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/views/Products.vue?vue&type=script&lang=js&":
/*!******************************************************************!*\
  !*** ./resources/js/views/Products.vue?vue&type=script&lang=js& ***!
  \******************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Products_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../node_modules/babel-loader/lib??ref--4-0!../../../node_modules/vue-loader/lib??vue-loader-options!./Products.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/Products.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Products_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/views/Products.vue?vue&type=style&index=0&id=eec6f8fa&scoped=true&lang=css&":
/*!**************************************************************************************************!*\
  !*** ./resources/js/views/Products.vue?vue&type=style&index=0&id=eec6f8fa&scoped=true&lang=css& ***!
  \**************************************************************************************************/
/*! no static exports found */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_Products_vue_vue_type_style_index_0_id_eec6f8fa_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../node_modules/style-loader!../../../node_modules/css-loader??ref--6-1!../../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../../node_modules/postcss-loader/src??ref--6-2!../../../node_modules/vue-loader/lib??vue-loader-options!./Products.vue?vue&type=style&index=0&id=eec6f8fa&scoped=true&lang=css& */ "./node_modules/style-loader/index.js!./node_modules/css-loader/index.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/Products.vue?vue&type=style&index=0&id=eec6f8fa&scoped=true&lang=css&");
/* harmony import */ var _node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_Products_vue_vue_type_style_index_0_id_eec6f8fa_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_Products_vue_vue_type_style_index_0_id_eec6f8fa_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0__);
/* harmony reexport (unknown) */ for(var __WEBPACK_IMPORT_KEY__ in _node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_Products_vue_vue_type_style_index_0_id_eec6f8fa_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0__) if(["default"].indexOf(__WEBPACK_IMPORT_KEY__) < 0) (function(key) { __webpack_require__.d(__webpack_exports__, key, function() { return _node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_Products_vue_vue_type_style_index_0_id_eec6f8fa_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0__[key]; }) }(__WEBPACK_IMPORT_KEY__));
 /* harmony default export */ __webpack_exports__["default"] = (_node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_Products_vue_vue_type_style_index_0_id_eec6f8fa_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0___default.a); 

/***/ }),

/***/ "./resources/js/views/Products.vue?vue&type=template&id=eec6f8fa&scoped=true&":
/*!************************************************************************************!*\
  !*** ./resources/js/views/Products.vue?vue&type=template&id=eec6f8fa&scoped=true& ***!
  \************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Products_vue_vue_type_template_id_eec6f8fa_scoped_true___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../node_modules/vue-loader/lib??vue-loader-options!./Products.vue?vue&type=template&id=eec6f8fa&scoped=true& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/Products.vue?vue&type=template&id=eec6f8fa&scoped=true&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Products_vue_vue_type_template_id_eec6f8fa_scoped_true___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Products_vue_vue_type_template_id_eec6f8fa_scoped_true___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ })

}]);