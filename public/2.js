(window["webpackJsonp"] = window["webpackJsonp"] || []).push([[2],{

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/ConfirmPayment.vue?vue&type=script&lang=js&":
/*!********************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/views/ConfirmPayment.vue?vue&type=script&lang=js& ***!
  \********************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
/* harmony default export */ __webpack_exports__["default"] = ({
  data: function data() {
    return {
      orders: []
    };
  },
  created: function created() {
    var storedOrdersData = localStorage.getItem('orders');

    if (storedOrdersData) {
      var storeOrders = JSON.parse(storedOrdersData);
      this.orders = storeOrders;
    }
  },
  computed: {
    getSubTotal: function getSubTotal() {
      return this.orders.reduce(function (a, b) {
        return a + b.price * b.quantity;
      }, 0);
    },
    getTotal: function getTotal() {
      return this.getSubTotal + 10;
    }
  }
});

/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/ConfirmPayment.vue?vue&type=template&id=27394b05&":
/*!************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/views/ConfirmPayment.vue?vue&type=template&id=27394b05& ***!
  \************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("div", [
    _vm._m(0),
    _vm._v(" "),
    _vm._m(1),
    _vm._v(" "),
    _c("div", { staticClass: "container margin_60_35" }, [
      _c("div", { staticClass: "row" }, [
        _c("div", { staticClass: "col-md-offset-3 col-md-6" }, [
          _c("div", { staticClass: "box_style_2" }, [
            _c("h2", { staticClass: "inner" }, [_vm._v("Order confirmed!")]),
            _vm._v(" "),
            _vm._m(2),
            _vm._v(" "),
            _c("h4", [_vm._v("Summary")]),
            _vm._v(" "),
            _c("table", { staticClass: "table table-striped nomargin" }, [
              _c(
                "tbody",
                [
                  _vm._l(_vm.orders, function(order) {
                    return _c("tr", { key: order.orderId }, [
                      _c("td", [
                        _c("strong", [_vm._v(_vm._s(order.quantity) + "x")]),
                        _vm._v(" " + _vm._s(order.name) + "\n\t\t\t\t\t")
                      ]),
                      _vm._v(" "),
                      _c("td", [
                        _c("strong", { staticClass: "pull-right" }, [
                          _vm._v("₵ " + _vm._s(order.price))
                        ])
                      ])
                    ])
                  }),
                  _vm._v(" "),
                  _vm._m(3),
                  _vm._v(" "),
                  _c("tr", [
                    _c("td", { staticClass: "total_confirm" }, [
                      _vm._v("\n\t\t\t\t\t\t TOTAL\n\t\t\t\t\t")
                    ]),
                    _vm._v(" "),
                    _c("td", { staticClass: "total_confirm" }, [
                      _c("span", { staticClass: "pull-right" }, [
                        _vm._v("₵ " + _vm._s(_vm.getTotal))
                      ])
                    ])
                  ])
                ],
                2
              )
            ])
          ])
        ])
      ])
    ])
  ])
}
var staticRenderFns = [
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c(
      "section",
      {
        staticClass: "parallax-window",
        attrs: {
          id: "short",
          "data-parallax": "scroll",
          "data-image-src": "img/sub_header_cart.jpg",
          "data-natural-width": "1400",
          "data-natural-height": "350"
        }
      },
      [
        _c(
          "div",
          {
            staticStyle: { background: "url(img/sub_header_cart.jpg)" },
            attrs: { id: "subheader" }
          },
          [
            _c("div", { staticClass: "overlay" }),
            _vm._v(" "),
            _c("div", { attrs: { id: "sub_content" } }, [
              _c("h1", [_vm._v("Place your order")]),
              _vm._v(" "),
              _c("div", { staticClass: "bs-wizard" }, [
                _c("div", { staticClass: "col-xs-4 bs-wizard-step complete" }, [
                  _c("div", { staticClass: "text-center bs-wizard-stepnum" }, [
                    _c("strong", [_vm._v("1.")]),
                    _vm._v(" Your details")
                  ]),
                  _vm._v(" "),
                  _c("div", { staticClass: "progress" }, [
                    _c("div", { staticClass: "progress-bar" })
                  ]),
                  _vm._v(" "),
                  _c("a", {
                    staticClass: "bs-wizard-dot",
                    attrs: { href: "/carts" }
                  })
                ]),
                _vm._v(" "),
                _c("div", { staticClass: "col-xs-4 bs-wizard-step complete" }, [
                  _c("div", { staticClass: "text-center bs-wizard-stepnum" }, [
                    _c("strong", [_vm._v("2.")]),
                    _vm._v(" Payment")
                  ]),
                  _vm._v(" "),
                  _c("div", { staticClass: "progress" }, [
                    _c("div", { staticClass: "progress-bar" })
                  ]),
                  _vm._v(" "),
                  _c("a", {
                    staticClass: "bs-wizard-dot",
                    attrs: { href: "/payments" }
                  })
                ]),
                _vm._v(" "),
                _c("div", { staticClass: "col-xs-4 bs-wizard-step complete" }, [
                  _c("div", { staticClass: "text-center bs-wizard-stepnum" }, [
                    _c("strong", [_vm._v("3.")]),
                    _vm._v(" Finish!")
                  ]),
                  _vm._v(" "),
                  _c("div", { staticClass: "progress" }, [
                    _c("div", { staticClass: "progress-bar" })
                  ]),
                  _vm._v(" "),
                  _c("a", {
                    staticClass: "bs-wizard-dot",
                    attrs: { href: "#" }
                  })
                ])
              ])
            ])
          ]
        )
      ]
    )
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { attrs: { id: "position" } }, [
      _c("div", { staticClass: "container" }, [
        _c("ul", [
          _c("li", [_c("a", { attrs: { href: "#0" } }, [_vm._v("Home")])]),
          _vm._v(" "),
          _c("li", [_c("a", { attrs: { href: "#0" } }, [_vm._v("Category")])]),
          _vm._v(" "),
          _c("li", [_vm._v("Page active")])
        ]),
        _vm._v(" "),
        _c(
          "a",
          { staticClass: "search-overlay-menu-btn", attrs: { href: "#0" } },
          [_c("i", { staticClass: "icon-search-6" }), _vm._v(" Search")]
        )
      ])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { attrs: { id: "confirm" } }, [
      _c("i", { staticClass: "icon_check_alt2" }),
      _vm._v(" "),
      _c("h3", [_vm._v("Thank you!")]),
      _vm._v(" "),
      _c("p", [
        _vm._v(
          "\n\t\t\t\t\t\tLorem ipsum dolor sit amet, nostrud nominati vis ex, essent conceptam eam ad. Cu etiam comprehensam nec. Cibo delicata mei an, eum porro legere no.\n\t\t\t\t\t"
        )
      ])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("tr", [
      _c("td", [
        _vm._v("\n\t\t\t\t\t\t Delivery schedule "),
        _c(
          "a",
          {
            staticClass: "tooltip-1",
            attrs: {
              href: "#",
              "data-placement": "top",
              title: "",
              "data-original-title":
                "Please consider 30 minutes of margin for the delivery!"
            }
          },
          [_c("i", { staticClass: "icon_question_alt" })]
        )
      ]),
      _vm._v(" "),
      _c("td", [
        _c("strong", { staticClass: "pull-right" }, [_vm._v("Today 07.30 pm")])
      ])
    ])
  }
]
render._withStripped = true



/***/ }),

/***/ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js":
/*!********************************************************************!*\
  !*** ./node_modules/vue-loader/lib/runtime/componentNormalizer.js ***!
  \********************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return normalizeComponent; });
/* globals __VUE_SSR_CONTEXT__ */

// IMPORTANT: Do NOT use ES2015 features in this file (except for modules).
// This module is a runtime utility for cleaner component module output and will
// be included in the final webpack user bundle.

function normalizeComponent (
  scriptExports,
  render,
  staticRenderFns,
  functionalTemplate,
  injectStyles,
  scopeId,
  moduleIdentifier, /* server only */
  shadowMode /* vue-cli only */
) {
  // Vue.extend constructor export interop
  var options = typeof scriptExports === 'function'
    ? scriptExports.options
    : scriptExports

  // render functions
  if (render) {
    options.render = render
    options.staticRenderFns = staticRenderFns
    options._compiled = true
  }

  // functional template
  if (functionalTemplate) {
    options.functional = true
  }

  // scopedId
  if (scopeId) {
    options._scopeId = 'data-v-' + scopeId
  }

  var hook
  if (moduleIdentifier) { // server build
    hook = function (context) {
      // 2.3 injection
      context =
        context || // cached call
        (this.$vnode && this.$vnode.ssrContext) || // stateful
        (this.parent && this.parent.$vnode && this.parent.$vnode.ssrContext) // functional
      // 2.2 with runInNewContext: true
      if (!context && typeof __VUE_SSR_CONTEXT__ !== 'undefined') {
        context = __VUE_SSR_CONTEXT__
      }
      // inject component styles
      if (injectStyles) {
        injectStyles.call(this, context)
      }
      // register component module identifier for async chunk inferrence
      if (context && context._registeredComponents) {
        context._registeredComponents.add(moduleIdentifier)
      }
    }
    // used by ssr in case component is cached and beforeCreate
    // never gets called
    options._ssrRegister = hook
  } else if (injectStyles) {
    hook = shadowMode
      ? function () {
        injectStyles.call(
          this,
          (options.functional ? this.parent : this).$root.$options.shadowRoot
        )
      }
      : injectStyles
  }

  if (hook) {
    if (options.functional) {
      // for template-only hot-reload because in that case the render fn doesn't
      // go through the normalizer
      options._injectStyles = hook
      // register for functional component in vue file
      var originalRender = options.render
      options.render = function renderWithStyleInjection (h, context) {
        hook.call(context)
        return originalRender(h, context)
      }
    } else {
      // inject component registration as beforeCreate hook
      var existing = options.beforeCreate
      options.beforeCreate = existing
        ? [].concat(existing, hook)
        : [hook]
    }
  }

  return {
    exports: scriptExports,
    options: options
  }
}


/***/ }),

/***/ "./resources/js/views/ConfirmPayment.vue":
/*!***********************************************!*\
  !*** ./resources/js/views/ConfirmPayment.vue ***!
  \***********************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _ConfirmPayment_vue_vue_type_template_id_27394b05___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./ConfirmPayment.vue?vue&type=template&id=27394b05& */ "./resources/js/views/ConfirmPayment.vue?vue&type=template&id=27394b05&");
/* harmony import */ var _ConfirmPayment_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./ConfirmPayment.vue?vue&type=script&lang=js& */ "./resources/js/views/ConfirmPayment.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _ConfirmPayment_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _ConfirmPayment_vue_vue_type_template_id_27394b05___WEBPACK_IMPORTED_MODULE_0__["render"],
  _ConfirmPayment_vue_vue_type_template_id_27394b05___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/views/ConfirmPayment.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/views/ConfirmPayment.vue?vue&type=script&lang=js&":
/*!************************************************************************!*\
  !*** ./resources/js/views/ConfirmPayment.vue?vue&type=script&lang=js& ***!
  \************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_ConfirmPayment_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../node_modules/babel-loader/lib??ref--4-0!../../../node_modules/vue-loader/lib??vue-loader-options!./ConfirmPayment.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/ConfirmPayment.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_ConfirmPayment_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/views/ConfirmPayment.vue?vue&type=template&id=27394b05&":
/*!******************************************************************************!*\
  !*** ./resources/js/views/ConfirmPayment.vue?vue&type=template&id=27394b05& ***!
  \******************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_ConfirmPayment_vue_vue_type_template_id_27394b05___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../node_modules/vue-loader/lib??vue-loader-options!./ConfirmPayment.vue?vue&type=template&id=27394b05& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/ConfirmPayment.vue?vue&type=template&id=27394b05&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_ConfirmPayment_vue_vue_type_template_id_27394b05___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_ConfirmPayment_vue_vue_type_template_id_27394b05___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ })

}]);