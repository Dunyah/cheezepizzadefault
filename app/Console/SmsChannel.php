<?php
/**
 * Created by PhpStorm.
 * User: dannyk
 * Date: 2020-11-25
 * Time: 12:14
 */

namespace App\Channels;
use App\SMS\Eazisend;
use Illuminate\Notifications\Notification;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Log;


class SmsChannel
{
    /**
     * Send the given notification.
     *
     * @param  mixed  $notifiable
     * @param  \Illuminate\Notifications\Notification  $notification
     * @return void
     */
    public function send($notifiable, Notification $notification)
    {
        $array = $notification->toSMS($notifiable);
        Log::info('sms channel to send sms ' . json_encode($array));
        Eazisend::sendMessage($array['phoneNumber'], $array['message']);
    }
}
