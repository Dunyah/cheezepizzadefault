<?php

namespace App\Http\Controllers;

use App\Branch;
use App\BranchProduct;
use App\Category;
use App\Notifications\OrderReceived;
use App\Order;
use App\OrderItem;
use App\Product;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;

class WebsiteController extends Controller
{
    public function __construct()
    {
        $this->middleware([]);
    }

    public function getBranches(){
        return response()->json(
            [
                'status' => true,
                'message' => 'success',
                'data' => Branch::all()
            ]
        );
    }

    public function getBranchProductsGroupedByCategories($branchId){

        //$branch = Branch::find($branchId);
        $output = [];
        $categoryWithProducts = Category::with('products.sizes')->with('products.branches')->whereHas('products.branches' , function ($query) use ($branchId, &$output){
             $query->where('branches.id',$branchId);
        })->get();

//        $categoryWithProducts = Category::with('products.sizes')->with('products.branches')->get();
//
//        $mutated = collect($categoryWithProducts)->map(function ($category) use ($branchId){
//
//            $products = collect($category->products)->map(function ($product) use ($branchId) {
//
//                return !$product->branches->where('id','=',$branchId)->first() ? $product : null;
//
//            });
//
//            //$category->products = null;
//
//            return collect($products)->filter(function ($p){
//                return $p != null;
//            });
//
//        });

        return response()->json(
            [
                'status' => true,
                'message' => 'success',
                'data' => $categoryWithProducts
            ]
        );

    }

    public function checkout(Request $request){

       $user = $request->user();

       $validator = Validator::make($request->all(),[

//            order table requirements
            'branch_id' => 'required',
            'address' => 'required',
            'expected_delivery_day' => 'required',
            'expected_delivery_time' => 'required',
            'recipient_phone' => 'required',
            'delivery' => 'required|bool', // delivery = true, take away = false
            'delivery_location' => 'required',
            'delivery_lat' => 'required',
            'delivery_lng' => 'required',
            'total_price' => 'required',
            'payment_option' => 'required', // 1 = pay on delivery

//            order items table requirement
            'items' => 'required|array'
        ]);

//       --------------------- Validations -------------------------
       if($validator->fails()){
           return response()->json(
               [
                   'status' => false,
                   'message' => $validator->errors()->first(),
               ]
           );
       }

       $isNewAccount = $request->get('is_new_account');
       $isNewAccount = ((bool) $isNewAccount);

       if($isNewAccount && !$request->has('name')){
//           its a new account check for the user name
           return response()->json(
               [
                   'status' => false,
                   'message' => 'Customer name required',
               ]
           );
       }

       $orderItems = $request->get('items');

       if(count($orderItems) < 1){
//           empty order items
           return response()->json(
               [
                   'status' => false,
                   'message' => 'Order items cannot be empty',
               ]
           );
       }


//       generate a new unique order number ---------------------------
      $lastOrderNumber = config('custom.last_order_number');
      $orderNumber = $lastOrderNumber + 1;
      setEnvironmentValue(['LAST_ORDER_NUMBER' => $orderNumber]);

      $branchId = $request->get('branch_id');
      $address = $request->get('address');
      $tempExpDate = $request->get('expected_delivery_day');
      $expectedDeliveryTime = $request->get('expected_delivery_time');
      $recipientPhone = $request->get('recipient_phone');
      $notes = $request->get('notes');
      $delivery = $request->get('delivery');
      $deliveryLocation = $request->get('delivery_location');
      $deliveryLat = $request->get('delivery_lat');
      $deliveryLng = $request->get('delivery_lng');
      $totalPrice = $request->get('total_price');
      $paymentOption = $request->get('payment_option');

      if($tempExpDate == 'tomorrow'){
          $expectedDeliveryDay = Carbon::tomorrow();
      }else{
          $expectedDeliveryDay = Carbon::today();
      }

      $deliveryFee = 0.00;
      //      calculate delivery fee

        $branch = Branch::find($branchId);
        $orderId = Str::uuid();

//      create order
        $order = $request->user()->orders()->create([

             'id' => $orderId,
            'number' => $orderNumber,
            'status' => 'pending',
            'branch_id' => $branchId,
            'branch_name' => $branch->name,
            'address' => $address,
            'expected_delivery_day' => $expectedDeliveryDay,
            'expected_delivery_time' => $expectedDeliveryTime,
            'recipient_phone' => $recipientPhone,
            'notes' => $notes,
            'delivery' => $delivery,
            'delivery_fee' => $deliveryFee,
            'delivery_location' => $deliveryLocation,
            'delivery_lat' => $deliveryLat,
            'delivery_lng' => $deliveryLng,
            'total_price' => $totalPrice,
            'payment_option' => $paymentOption,

        ]);

        $input = [];

        foreach ($orderItems as $item) {

            $product = Product::find($item['id']);

            array_push($input,[
                'id' => Str::uuid(),
                'order_id' => $orderId,
                'quantity' => $item['quantity'],
                'product_name' => $product->name,
                'product_description' => $product->description,
                'product_price' => $product->price,
                'product_image' => $product->image,
                'category_name' => $product->category->name,
                'size' => $item['size'],
                'additional_price' => $item['additional_price'],
                'created_at' => now(),
                'updated_at' => now()
            ]);
        }

//        later on, notify admin as well

        $order->items()->insert($input);

        //        notify user
        $user->notify(new OrderReceived($order, url('/')));

            return response()->json(
                [
                    'status' => true,
                    'message' => 'Order Successful',
                    'orderId' =>  $orderId
                ]
            );

        }

        public function  orderReceived($orderId){

            $order = Order::find($orderId);
            $dt = Carbon::parse($order->expected_delivery_day);
            if($dt->isToday()){
                $order->expected_delivery_day = 'Today';
            }else if($dt->isTomorrow()){
                $order->expected_delivery_day = 'Tomorrow';
            }else{
                $order->expected_delivery_day = $dt->toFormattedDateString();
            }


            return response()->json(
                [
                    'status' => true,
                    'message' => 'success',
                    'order' => $order,
                    'items' => $order->items
                ]
            );

        }

        public function orders(Request $request, $status = 'pending') {
            $orders = $request->user()->orders()->where('status', $status)->get();
            return response()->json(
                [
                    'status' => true,
                    'message' => 'success',
                    'orders' => $orders,
                ]
            );
        }

}
