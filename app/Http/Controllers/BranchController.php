<?php

namespace App\Http\Controllers;

use App\Branch;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class BranchController extends Controller
{

    public function __construct()
    {
        $this->middleware(['auth', 'role:Super Admin|Branch Manager']);
    }

    public function index()
    {
        // pass all created shop admins to this page
        $subShopadmins = User::role('Branch Manager')->get();

        // show branches assigned to this shop admin
        $branches = Branch::all();

        $data = [
            'branches' => $branches,
            'subShopadmins' => $subShopadmins
        ];

        return view('administration.branches.manage_branches')->with($data);

    }

    public function showAddBranch(){

        return view('administration.branches.add_branch');
    }

    public function selectBranch($branchId){

        $user = Auth::user();
        $oldBranch =  Auth::user()->{'selectedBranch'};
        $table = DB::table('user_selected_branch');
        $table->where('user_id', $user->{'id'})->where('branch_id',$oldBranch->id)->delete();

        $table->insert([
            'user_id' => $user->{'id'},
            'branch_id' => $branchId,
            'created_at' => now(),
            'updated_at' => now()
        ]);

        return back();
    }

    public function addNewBranch(Request $request){

    }


}
