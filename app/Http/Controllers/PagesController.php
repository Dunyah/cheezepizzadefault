<?php

namespace App\Http\Controllers;

use App\Order;
use Illuminate\Http\Request;

class PagesController extends Controller
{
    public function cart(){
        return view('orders.cart');
    }

    public function checkout(){
        return view('orders.checkout');
    }

    public function confirm(){
        return view('orders.confirmorder');
    }
    
    public function viewOrders(){
        return view('orders.vieworders');
    }
    
    public function OrderItemDetails(){
        return view('orders.orderItems');
    }
    public function ViewOrdersByOrderNumber(){
        return view('orders.viewordersbyorderNumber');
    }
}
