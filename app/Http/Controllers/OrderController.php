<?php

namespace App\Http\Controllers;

use App\Branch;
use App\Notifications\OrderCancelledNotification;
use App\Notifications\OrderConfirmedNotification;
use App\Notifications\OrderDeliveredNotification;
use App\Notifications\OrderShippedNotification;
use App\Order;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class OrderController extends Controller
{
    public function __construct()
    {
        $this->middleware(['auth', 'role:Super Admin|Branch Manager']);
    }

    public function orders(){
        return view('administration.orders.allorders');
    }

    public function orderDetail($id){

        $order = Order::find($id);
        return view('administration.orders.orderdetail')->with([
            'order' => $order,
            'count' => $order->items->count(),
            'branches' => Branch::where('id','!=',Auth::user()->{'selectedBranch'}->id)->get()
        ]);
    }

    public function fetchOrders($status = 'pending'){

//        begins here ///////////
        $orders = Auth::user()->{'selectedBranch'}->orders()->where('status', $status)->get();

        $transformed = collect([]);
        $t = collect($orders)->map(function($item){
            $item['phone'] = $item->reciever_phone;
            $item['customer'] = $item->user->name;
            $item['date_ordered'] = Carbon::parse($item->created_at)->toDayDateTimeString();
            $item['action'] = '<a href="'.\url("administration/order-detail/".$item->id).'" class="btn btn-outline-primary btn-min-width box-shadow-1 mr-1 mb-1">Details</a>';

            if ($item->payment_option == 1){
                $item['payment_option'] = "Paid Online";
            }elseif($item->payment_option == 2){
                $item['payment_option'] = "Pay on delivery";
            }

            return $item;

        });
        $transformed = $transformed->merge($t);

        return response()->json(['data' => $transformed]);

    }

    public function fetchOrderItems($id){

        $order = Order::find($id);
        $items = $order->items;

        $transformed = $items->map(function($item){
            $newItem['id'] = $item->id;
            $newItem['checkbox'] = '';
            $newItem['image'] = '<img class="img-thumbnail img-fluid" src="'.asset("storage/".$item->product_image).'" style="object-fit: contain;" itemprop="thumbnail" alt="Image description" />';
            $newItem['name'] = $item->product_name;
            $newItem['quantity'] = $item->quantity;
            $newItem['price'] = $item->product_price + $item->additional_price;

            $newItem['size'] = $item->size ?: 'N/A';

            $pendingClass = 'btn-warning';
            $status = $item->status;

            if ($item->status == 'delivered') {
                $status = $item->status;
                $pendingClass = 'btn-success';
            }else if ($item->status == 'cancelled'){
                $status = $item->status;
                $pendingClass = 'btn-danger';
            }


            $newItem['status'] = '<span class="btn '.$pendingClass.'">'.$status. '</span>';

            return $newItem;
        });

        return json_encode(['data' => $transformed]);

    }

    public function countOrders($status = 'pending'){
        $countPendingOrders = Auth::user()->{'selectedBranch'}->orders()->where('status',$status)->count();
        if($countPendingOrders > 0){
            return response()->json(['status' => true, 'message' => 'completed', 'count' => $countPendingOrders ]);
        }

        return response()->json(['status' => false, 'message' => 'completed', 'count' => $countPendingOrders ]);

    }

    // work on it
    public function assignOrderToBranch(Request $request,$orderId, $branchId)
    {
        $order = Order::find($orderId);
        $branch = Branch::find($branchId);
        if ($order->delivery_option == 1){
            // recalculate delivery fee
//            $resultArr = Utilities::processDeliveryAmount($branchId, $order->shop->id, $order->latitdue, $order->longitude);
//
//            if ($resultArr['status'] == true){
//                $order->update(['delivery_fee' => $resultArr['deliveryFee']]);
//            }
        }

        $updated =  $order->update(['branch_id' => $branchId]);
        if ($updated) {
            $request->session()->flash('success-message', 'Order has been re-assigned to '.$branch->name);
            return redirect('administration/orders');
        }

        $request->session()->flash('error-message', 'unable to re-assign order to '.$branch->name);
        return back();

    }


    public function manageOrders(Request $request, $orderId, $status){

        // update status of selected items
        $ids = collect($request->data)->map(function($item){
            return $item['id'];
        });


        // check if all the items in the order are delivered / cancelled

        if ($status == 'confirmed'){

            DB::table('order_items')->whereIn('id', $ids)->update(['status' => 'confirmed']);

            $order = $this->updateMainOrder($orderId);
            //$order->user->notify(new OrderConfirmedNotification($order));

            return response()->json(['status' => true]);

        }else if($status == 'shipped'){
            // status = 4 // order shipped
            DB::table('order_items')->whereIn('id', $ids)->update(['status' => 'shipped']);

            $order = $this->updateMainOrder($orderId);
            //$order->user->notify(new OrderShippedNotification($order));

            return response()->json(['status' => true]);

        }

        return response()->json(['status' => false,'message' => 'invalid request']);
    }


    public function markOrderItemAsDelivered(Request $request, $orderId)
    {
        // mark selected cart items as delivered
        $ids = collect($request->data)->map(function($item){
            return $item['id'];
        });

        DB::table('order_items')->whereIn('id', $ids)->update(['status' => 'delivered']);

        $order = $this->updateMainOrder($orderId);
        //$order->user->notify(new OrderDeliveredNotification($order));


        return response()->json(['status' => true, 'message' => 'Items delivered successfully']);


    }

    public function cancelOrders(Request $request, $orderId)
    {
        // mark selected cart items as delivered
        $ids = collect($request->data)->map(function($item){
            return $item['id'];
        });

        DB::table('order_items')->whereIn('id', $ids)->update(['status' => 'cancelled']);

        $order = $this->updateMainOrder($orderId);
        //$order->user->notify(new OrderCancelledNotification($order));


        return response()->json(['status' => true, 'message' => 'Items cancelled']);
    }


    private function updateMainOrder($orderId): Order{
        // check if all the items in the order are delivered / cancelled
        $order = Order::find($orderId);
        $carts = $order->items;

        $done = true;
        foreach ($carts as $value) {
            if ($value->status != 'delivered' && $value->status != 'cancelled') {
                $done = false;
                break;
            }
        }

        // if all cart items are attended to, flag order as done
        if ($done) {
            $order->status = 'completed'; // 1 = done
            $order->save();

            // send sms, and email
            $order = Order::find($orderId);

        }else{
            $order->status = 'pending';
            $order->save();
        }

        return $order;
    }


}
