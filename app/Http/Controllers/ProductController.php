<?php

namespace App\Http\Controllers;

use App\BranchProduct;
use App\Product;
use App\ProductSize;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;

class ProductController extends Controller
{

    public function __construct()
    {
        $this->middleware(['auth', 'role:Super Admin|Branch Manager']);
    }

    public function manageProducts(){
        return view('administration.products.manage_products');
    }

    public function showAddProduct(){
        return view('administration.products.add_product');
    }

    public function addNewProduct(Request $request){

//        return $request->all();

        $request->validate([
            'name' => 'required|string',
            'main_image' => 'mimes:jpeg,bmp,png,jpg'
        ]);

        $product_main_image = Storage::disk('public')->putFile('product_main_images', $request->file('main_image'));

        $price = $request->get('price');

        $product = Product::create(
            [
                'id' => Str::uuid(),
                'name' => $request->get('name'),
                'description' => $request->get('description'),
                'price' => $price,
                'image' => $product_main_image,
                'category_id' => $request->get('category')
            ]
        );

        Log::info('product created: ' . json_encode($product));

//        add additional prices if any
        if($price != '' && ((double)$price) == 0.00){
            // product has additional price

            $additionalPrices = $request->get('additional_price');
            $additionalSizeIds = $request->get('additional_size_id');

            $input = [];
            for ($i = 0; $i < count($additionalPrices); $i++){
                $sizeId = $additionalSizeIds[$i];
                $additionalPrice = $additionalPrices[$i];

                array_push($input, [
                    'product_id' => $product->id,
                    'size_id' => $sizeId,
                    'additional_price' => $additionalPrice,
                    'created_at' => now(),
                    'updated_at' => now()
                ]);
            }

            ProductSize::insert($input);
            Log::info('additional prices inserted: ' . json_encode($input));


        }

        $request->session()->flash('success-message', $product->name.' has been added. Click on the "or Manage product" button above for further actions');
        return \back();

    }

    public function fetchProducts(Request $request){
        // get all products for the entire shop
        $products = Product::all();
        $transformed = \collect($products)->map(function($product)
        {
            $newItem['checkbox'] = '';
            $newItem['image'] = '<img class="img-thumbnail img-fluid" src="'.asset("storage/".$product->image).'" style="width:80px; height:80px;" itemprop="thumbnail" alt="Image description" />';
            $newItem['name'] = $product->name;
            $newItem['price'] = $product->price;
            $newItem['type'] = $product->category->name;

            $newItem['visibility'] = $product->visibility == true ? '<span class="text-success">showing</span>' : '<span class="text-danger">hidden</span>';
            $newItem['details'] = '<a href="'.\url("administration/edit-product/".$product->id).'" class="btn btn-outline-primary btn-min-width box-shadow-1 mr-1 mb-1">Details</a>';
            $newItem['id'] = $product->id;
            return $newItem;

        });

        return response()->json(['data' => $transformed]);
    }

    public function showHideProduct(Request $request, $visibility = 1){
        $productsIds = $request->get('productsIds');
        Product::whereIn('id',$productsIds)->update(['visibility' => $visibility]);
        return response()->json(['status' => true, 'message' => 'products visibility updated']);
    }


    public function deleteProducts(Request $request){

        $products = $request->get('productsIds');


        Log::info('productsIds: ' . json_encode($products));

        $input = [];

        foreach ($products as $p) {
            array_push($input, $p['id']);
        }

        Log::info('productsIds: ' . json_encode($input));

        // remove product relation to branches
        BranchProduct::whereIn('product_id', $input)->delete();

        // remove product relation to  categories

        // remove product relation to  sizes
        ProductSize::whereIn('product_id', $input)->delete();

//        delete actual product
        Product::whereIn('id',$input)->delete();


        return response()->json(['status' => true, 'message' => 'products removed from the system']);

    }

    public function showEditProduct($id){
        $product = Product::find($id);
        return view('administration.products.edit_product')->with([
            'product' => $product,
            'main_image' => asset('storage/'.$product->image),
        ]);
    }

    public function updateProduct(Request $request, $id){
        $product = Product::find($id);

        // set default main image
        $product_mainimage = $product->image;

        // check if main image has changed
        if ($request->has('main_image')) {

            // remove old main image from the Storage
            $deleted = Storage::disk('public')->delete($product->image);

            // return $product->mainimage;

            if ($deleted) {

                // save new main image
                $product_mainimage = Storage::disk('public')->putFile('product_main_images', $request->file('main_image'));

            }

        }


    }
}
