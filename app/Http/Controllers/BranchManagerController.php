<?php

namespace App\Http\Controllers;

use App\Branch;
use App\BranchUser;
use App\Notifications\BranchManagerCreated;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;

/*
 *  NB: Branch managers are users with branch manager role
 * */
class BranchManagerController extends Controller
{

    public function __construct()
    {
        $this->middleware(['auth', 'role:Super Admin|Branch Manager']);
    }

    public function index(){

        $branchManagers = User::role(['Branch Manager'])->get();
        return view('administration.branchmanagers.branchmanagerlist')->with(['managers' => $branchManagers]);
    }

    public function add(){
        $branches = Branch::all();
        return view('administration.branchmanagers.addbranchmanager')->with(['branches' => $branches]);
    }

    public function edit($id){
        $user = User::find($id); // this is user should definitely a branch manager

        if(!$user->hasRole('Branch Manager')){
            Session::flash('error-message', 'Invalid manager');
            return redirect('administration/home');
        }

        return view('administration.branchmanagers.editbranchmanager')->with(['manager' => $user]);
    }

    public function addManager(Request $request){

        $validator = Validator::make($request->all(), [
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
        ]);

        if ($validator->fails()){
            Session::flash('error-message', $validator->errors()->first());
            return back()->withInput();
        }

        $email = $request->get('email');
        $phone = $request->get('phone');
        $branches = $request->get('assignbranch');

//        check if email already exists
        $existingUser = User::where('email',$request->get('email'))->exists();
        if($existingUser){
            Session::flash('error-message','Email already exists in the system');
            return back()->withInput();
        }

        if(!$branches || count($branches) < 1 || !is_array($branches)){
            Session::flash('error-message','Please select at least one branch');
            return back()->withInput();
        }

        $image = null;
        if ($request->has('image')) {
            $image = Storage::disk('public')->putFile('pics', $request->file('image'));
        }

//        Create User

        $tempPassword = generateRandomString(6);

        // generate a password for user
        $user = User::create([
            'name' => $request->get('name'),
            'email' => $email,
            'password' => Hash::make($tempPassword),
            'phone' => $phone,
            'image' => $image,
            'id' => Str::uuid()
        ]);

//        assign user the role of branch manager
        $user->refresh();
        $user->assignRole('Branch Manager');


//        Assign user to the branches
        $arr = [];
        foreach ($branches as $branchId) {
            // 4 is the column that contains the id
            array_push($arr, ['branch_id' => $branchId, 'user_id' =>$user->id]);
        }

        (new BranchUser())->insertOrUpdate($arr);

//        send a notification to the manager
        //$user->notify(new BranchManagerCreated($tempPassword));

//        return a flush message to admin
        Session::flash('success-message','Manager added Successfully');
        return back();


    }



}
