<?php

namespace App\Http\Controllers;

use App\Branch;
use App\Product;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('administration.home.index');
    }

    public function fetchSummary(){

        $pendingObj = Auth::user()->{'selectedBranch'}->orders()->where('status','pending');
        $pending = $pendingObj ? $pendingObj->count() : 0;

        $customersObj = Auth::user()->{'selectedBranch'}->orders()->groupBy('user_id');
        $customers  = $customersObj ? $customersObj->count() : 0;

        $totalproductsObj = Auth::user()->{'selectedBranch'}->products();
        $totalproducts  = $totalproductsObj ? $totalproductsObj->count() : 0;

        //$customers  = User::role('Customer')->count();
        //$totalproducts  = Auth::user()->{'selectedBranch'}->products->count();

        return ['pending'=>$pending,'customers'=>$customers,'totalproducts'=>$totalproducts, 'smsLeft' => 0];
    }


    public function fetchSmsBalance(){
        return response()->json([
            'data' => [
                'balance' =>  0
            ]
        ]);
    }

    public function fetchordersStats(Request $r){

       $branch = Auth::user()->{'selectedBranch'};


        $datearr = [];
        $orders = [];
        if(isset($r->custom)){
            $getdates = explode('-',$r->duration);
            $startdate = strtotime($getdates[0]);
            $enddate = strtotime($getdates[1]);


            for ($i=$startdate; $i<=$enddate; $i+=86400) {
                $shopOrders = $branch->orders()->where('created_at','like',date('Y-m-d',$i).'%');
                array_push($datearr, date('d/D', $i));
                array_push($orders, $shopOrders ? $shopOrders->count() : 0);
            }
        }else{
            // $shopOrdersObj = $shop->orders()->where('created_at','like',date('Y-m-d').'%');
            // array_push($datearr, date('d/D', strtotime('-'.$i.' days')));
            // array_push($orders,$shopOrdersObj ? $shopOrdersObj->count() : 0);

            for ($i=7; $i>=0; $i--){
                $shopOrdersObj = $branch->orders()->where('created_at','like',date('Y-m-d', strtotime('-'.$i.' days')).'%');
                array_push($datearr, date('d/D', strtotime('-'.$i.' days')));
                array_push($orders,$shopOrdersObj ? $shopOrdersObj->count() : 0);

            }
        }
        return ['datearr'=>$datearr,'orders'=>$orders];
    }


}
