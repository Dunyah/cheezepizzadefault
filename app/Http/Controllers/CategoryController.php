<?php

namespace App\Http\Controllers;

use App\Category;
use App\Product;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;

class CategoryController extends Controller
{

    public function __construct()
    {
        $this->middleware(['auth', 'role:Super Admin|Branch Manager']);
    }

    public function index(){

        return view('administration.category.categorylist')->with([
            'categories' => Category::all()
        ]);
    }

    public function addCategory(){
        return view('administration.category.addcategory');
    }

    public function showEditCategory($id){
        return view('administration.category.editcategory')->with([
            'category' => Category::find($id)
        ]);
    }

    public function saveCategory(Request $request){
        $validator = Validator::make($request->all(), [
            'name' => 'required'
        ]);

        if($validator->fails()){
            Session::flash('error-message', $validator->errors()->first());
            return back();
        }

        Category::create(
            [
                'id' => Str::uuid(),
                'name' => $request->get('name')
            ]
        );
        Session::flash('success-message','Category Added Successfully');
        return back();
    }

    public function updateCategory(Request $request, $id){
        $validator = Validator::make($request->all(), [
            'name' => 'required'
        ]);
        if($validator->fails()){
            Session::flash('error-message', $validator->errors()->first());
            return back();
        }


        Category::find($id)->update(
            [
                'name' => $request->get('name')
            ]
        );
        Session::flash('success-message','Category Updated Successfully');
        return back();

    }

    public function deleteCategory(Request $r){

        $del = Category::find($r->id)->delete();
        if($del){
            //        update all products which belongs to this category
            Product::where('category_id', $r->id)->update([
                'category_id' => null
            ]);
            return ['status'=>'success'];
        }else{
            return ['status'=>'error'];
        }


    }
}
