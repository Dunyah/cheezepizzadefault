<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Http\JsonResponse;
use Illuminate\Validation\ValidationException;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;


    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    public function showLoginForm()
    {
        return view('administration.auth.login');
    }

    protected function sendFailedLoginResponse(Request $request)
    {
        if($request->wantsJson()){
            return response()->json(['status' => false, 'message' => "These credentials do not match our records."]);
        }

        throw ValidationException::withMessages([
            $this->username() => [trans('auth.failed')],
        ]);

    }


    protected function sendLoginResponse(Request $request)
    {
       // $request->session()->regenerate();

        $this->clearLoginAttempts($request);

        if ($response = $this->authenticated($request, $this->guard()->user())) {
            return $response;
        }

        return $request->wantsJson()
            ? new JsonResponse([], 204)
            : redirect()->intended($this->redirectPath());
    }

    protected function authenticated(Request $request, $user)
    {

        if($user->hasRole('Customer')){

            $token = $user->createToken('master')->accessToken;
            return response()->json([

                'status' => true,
                'message'=>'Login Successful',
                'token' => $token,
                'userId' => $user->id,
                'name' =>  $user->name,
                'email' => $user->email,
                'phone' => $user->phone

            ], 200);

        }
    }




}
