<?php

namespace App;

use App\Traits\UUIDTrait;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\App;
use Laravel\Passport\HasApiTokens;
use Spatie\Permission\Traits\HasRoles;

class User extends Authenticatable
{
    use Notifiable, HasRoles, UUIDTrait, HasApiTokens;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'phone', 'id', 'image'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];


    public function branches(){
       return $this->belongsToMany(Branch::class, BranchUser::class);
    }

    public function selectedBranch(){
//       return $this->hasOne(UserSelectedBranch::class);
        return $this->hasOneThrough(Branch::class,UserSelectedBranch::class,'user_id','id','id','branch_id');

    }


    public function orders(){
        return $this->hasMany(Order::class);
    }

}
