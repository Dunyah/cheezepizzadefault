<?php

namespace App;

use App\Traits\UUIDTrait;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

class Branch extends Model
{

    use UUIDTrait;
    //use HasFactory;

    protected $fillable = [
        'id',
        'name',
        'location',
        'opening_time',
        'closing_time',
        'take_away',
        'delivery',
        'image'
    ];

    public function products(){
        return $this->belongsToMany(Product::class);
    }

    public function orders(){
        return $this->hasMany(Order::class);
    }
}
