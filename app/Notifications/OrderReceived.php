<?php

namespace App\Notifications;

use App\Order;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class OrderReceived extends Notification
{
    use Queueable;
    private $url;
    private $order;

    /**
     * Create a new notification instance.
     *
     * @param Order $order
     * @param $url
     */
    public function __construct(Order $order, $url)
    {
        $this->url = $url;
        $this->order = $order;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        $message = "Thank you for placing your order, we will review and get back to you as soon as possible. Thank you so much for using ".config('app.name').".<br> For enquiries <br> kindly reach us on <br> 0322498122 <br> 0322498151 <br> contact@cheezypizza.com <br> <p>Track your order in your account or use this link ".url('tracking')." and enter your order number ".$this->order->{'number'}." </p> </p>";

        return (new MailMessage)
            ->subject('Order Received')
            ->markdown('emails.order', [
                'url' => $this->url,
                'message' => $message,
                'notifiable' => $notifiable
            ]);
    }

}
