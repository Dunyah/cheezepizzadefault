<?php

namespace App\Notifications;

use App\Channels\SmsChannel;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class OrderCancelledNotification extends Notification
    //implements ShouldQueue
{
    use Queueable;
    private $order;
    private $url;
    private $message;

    /**
     * Create a new notification instance.
     *
     * @param $order
     */
    public function __construct($order)
    {
        $this->order = $order;
        $this->url = url("order/detail/".$this->order->id);

        $phone1 = config('custom.phone_number_1');
        $phone2 = config('custom.phone_number_2');
        $email = config('custom.contact_email');
        $website = config('app.url');

        $this->message =  "Hello esteemed customer ,\n\n Unfortunately your order #".$order->number." has been cancelled, Kindly contact us for assistance \n\n ".$phone1.",\n\n ".$phone2.",\n\n ".$email." ,\n\n ".$website;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        $message = sprintf($this->message);

        return (new MailMessage)
            ->subject('Orders Cancelled')
            ->markdown('emails.order', [
                'url' => $this->url,
                'message' => $message,
                'notifiable' => $notifiable
            ]);
    }

    public function toSMS($notifiable){

        // you'll pick from the db
        $message = sprintf($this->message);

        return [
            'phoneNumber' => $this->order->reciever_phone,
            'message' => $message
        ];

    }

}
