<?php

namespace App\Notifications;

use App\Channels\SmsChannel;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;
use Illuminate\Support\Facades\Log;

class OrderConfirmedNotification extends Notification
    //implements ShouldQueue
{
    use Queueable;
    private $order;
    private $url;
    private $message;

    /**
     * Create a new notification instance.
     *
     * @param $order
     */
    public function __construct($order)
    {
        $this->order = $order;
        $this->url = url("order/detail/".$order->id);
        $this->message = "Congratulations! %s ,\n\n Your order #".$order->number." has been confirmed for shipment , Thanks a million ";
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return [SmsChannel::class, 'mail'];
    }

    public function toSMS($notifiable){

        // you'll pick from the db
        $message = sprintf($this->message, $notifiable->name);

        return [
              'phoneNumber' => $this->order->reciever_phone,
              'message' => $message
        ];

    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        $message = sprintf($this->message, $notifiable->name);

        return (new MailMessage)
            ->subject('Orders Confirmed')
            ->markdown('emails.order', [
                'url' => $this->url,
                'message' => $message,
                'notifiable' => $notifiable
            ]);
    }



}
