<?php

namespace App\Notifications;

use App\Channels\SmsChannel;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class OrderDeliveredNotification extends Notification
    //implements ShouldQueue
{
    use Queueable;
    private $order;
    private $url;
    private $message;

    /**
     * Create a new notification instance.
     *
     * @param $order
     */
    public function __construct($order)
    {
        $this->order = $order;
        $this->url = url("order/detail/".$this->order->id);
        $app_name = config('app.name');
        $this->message = "Hi %s, \n\n Your order #".$order->number." has been delivered. We are overly proud of you , Thank you so much  for using ".$app_name;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail', SmsChannel::class];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        $message = sprintf($this->message, $notifiable->name);

        return (new MailMessage)
            ->subject('Orders Delivered')
            ->markdown('emails.order', [
                'url' => $this->url,
                'message' => $message,
                'notifiable' => $notifiable
            ]);
    }

    public function toSMS($notifiable){

        // you'll pick from the db
        $message = sprintf($this->message, $notifiable->name);

        return [
            'phoneNumber' => $this->order->reciever_phone,
            'message' => $message
        ];

    }
}
