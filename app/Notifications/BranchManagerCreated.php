<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class BranchManagerCreated extends Notification
{
    use Queueable;
    private $rawPassword;
    private $loginUrl;

    /**
     * Create a new notification instance.
     *
     * @param string $rawPassword
     */
    public function __construct(string $rawPassword)
    {
        $this->rawPassword = $rawPassword;
        $this->loginUrl = url('administration/auth/login');
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        //send use and email and sms here
        $message = "Dear ".$notifiable->name.' You have been assigned as a branch administrator. Kindly login with email - ' .$notifiable->email.' and password: ' . $this->rawPassword;

        return (new MailMessage)
            ->subject('Branch Manager')
            ->markdown('emails.branch_manager_created', [
                'url' => $this->loginUrl,
                'message' => $message,
                'notifiable' => $notifiable
            ]);
    }

}
