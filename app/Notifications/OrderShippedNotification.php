<?php

namespace App\Notifications;

use App\Channels\SmsChannel;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class OrderShippedNotification extends Notification
    //implements ShouldQueue
{
    use Queueable;
    private $order;
    private $url;
    private $message;

    /**
     * Create a new notification instance.
     *
     * @param $order
     */
    public function __construct($order)
    {
        $this->order = $order;
        $this->url = url("order/detail/".$order->id);
        $this->message =  "Hello Esteemed customer,\n\n Your order #".$order->number." has been shipped to your address.\n\n Thanks";
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail', SmsChannel::class];
    }


    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        $message = sprintf($this->message);

        return (new MailMessage)
            ->subject('Orders Shipped')
            ->markdown('emails.order', [
                'url' => url('profile'),
                'message' => $message,
                'notifiable' => $notifiable
            ]);
    }

    public function toSMS($notifiable){

        // you'll pick from the db
        $message = sprintf($this->message);

        return [
            'phoneNumber' => $this->order->reciever_phone,
            'message' => $message
        ];

    }



}
