<?php

namespace App;

use App\Traits\UUIDTrait;
use Illuminate\Database\Eloquent\Model;

class Product extends Model
{

    use UUIDTrait;

    protected $fillable = [
        'id',
        'name',
        'description',
        'price',
        'image',
        'category_id',
        'visibility'
    ];


    public function branches(){
        return $this->belongsToMany(Branch::class);
    }

    public function category(){
        return $this->belongsTo(Category::class);
    }

    public function sizes(){
        return $this->belongsToMany(Size::class)->withPivot(['additional_price']);
    }

    public function getPriceAttribute($value){
        return number_format($value,2);
    }
}
