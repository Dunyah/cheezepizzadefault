<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserSelectedBranch extends Model
{
    protected $table = 'user_selected_branch';
    protected $fillable = [
        'branch_id','user_id'
    ];

    public function branch(){
        return $this->belongsTo(Branch::class);
    }

}
