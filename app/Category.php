<?php

namespace App;

use App\Traits\UUIDTrait;
use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    use UUIDTrait;

    protected $fillable = [
        'id', 'name'
    ];

    public function products(){
        return $this->hasMany(Product::class);
    }
}
