<?php

namespace App;

use App\Traits\UUIDTrait;
use Illuminate\Database\Eloquent\Model;

class Order extends Model
{

    use UUIDTrait;

    protected $fillable = [
        'id',
        'number', 'status',
        'user_id', 'branch_id', 'branch_name',
        'address', 'expected_delivery_day',
        'expected_delivery_time', 'recipient_phone',
        'notes', 'delivery',
        'delivery_fee', 'delivery_lat',
        'delivery_lng', 'total_price',
        'payment_option'
    ];

    public function items(){
        return $this->hasMany(OrderItem::class);
    }

    public function user(){
        return $this->belongsTo(User::class);
    }

    public function branch(){
        return $this->belongsTo(Branch::class);
    }
}
