<?php

namespace App;

use App\Traits\UUIDTrait;
use Illuminate\Database\Eloquent\Model;

class OrderItem extends Model
{
    use UUIDTrait;

    protected $fillable = [
        'id',
        'order_id',
        'quantity',
        'product_name',
        'product_description',
        'product_price',
        'product_image',
        'category_name',
        'size',
        'additional_price',
        'status'
    ];

    public function product() {
        $this->belongsTo(Product::class);
    }
}
