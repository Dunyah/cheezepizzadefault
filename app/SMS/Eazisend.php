<?php
/**
 * Created by PhpStorm.
 * User: dannyk
 * Date: 2020-08-26
 * Time: 15:07
 */

namespace App\SMS;


use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Log;
use Mockery\Exception;

class Eazisend
{
    public static function checkBalance(){

        $url = config('custom.eazisend_url') . '/sms/balance';
        $response = Http::post($url, [
            'clientId' => config('custom.eazisend_client_id'),
            'apiKey' => env('custom.eazisend_api_key'),
        ]);

        $json = $response->json();
        return $json;

    }

    public static function sendMessage($recipientPhone, $message){

        //Http::fake();

       try{

           $url = config('custom.eazisend_url') . '/sms/single';

           $response = Http::post($url, [
               'clientId' => config('custom.eazisend_client_id'),
               'apiKey' => env('custom.eazisend_api_key'),
               'senderName' => 'SpeedoMall',
               'phoneNumber' => $recipientPhone,
               'message' => $message,
           ]);

           Log::info('response: ' . $response->body());

       }catch (Exception $exception){
           Log::info('error: ' . $exception->getMessage());
       }


    }

    public static function createContacts(Collection $users) : bool {

        if ($users->isEmpty()){
            return false;
        }

        $url = config('custom.eazisend_url') . '/add-contacts';

        $generatedContacts = [];
        foreach ($users as $user){
            if (!$user->phone){
                continue;
            }
            array_push($generatedContacts,[
                'phone' => $user->phone,
                'othernames' => $user->name
            ]);
        }

        $response = Http::post($url, [
            'clientId' => config('custom.eazisend_client_id'),
            'apiKey' => env('custom.eazisend_api_key'),
            'contact' => $generatedContacts
        ]);

        $data = json_decode($response->body(), true);
        Log::info('createContacts: ' . json_encode($data));
        return $data['status'];
    }
}
