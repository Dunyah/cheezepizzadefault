<?php

namespace App;

use App\Traits\UUIDTrait;
use Illuminate\Database\Eloquent\Model;

class Size extends Model
{
    use UUIDTrait;
}
