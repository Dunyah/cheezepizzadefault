<?php

namespace App;

use App\Traits\InsertOrUpdateTrait;
use Illuminate\Database\Eloquent\Model;

class BranchUser extends Model
{
    use InsertOrUpdateTrait;

    protected $table = 'branch_users';

    protected $fillable = [
        'branch_id','user_id'
    ];


}
