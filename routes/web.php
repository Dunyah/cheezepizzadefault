<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/products','PagesController@products');


Route::get('administration/test', function (){
    //return \App\User::role('Super Admin')->first()->selectedBranch;
    return view('administration.template');
});


Route::prefix('administration')->group(function(){

//    Authentication route

    Route::prefix('auth')->group( function (){
        Route::get('login', 'Auth\LoginController@showLoginForm')->name('login');
        Route::post('login', 'Auth\LoginController@login');
        Route::get('logout', 'Auth\LoginController@logout');
    });

//    dashboard route

    Route::prefix('/')->middleware(['auth'])->group( function (){

        Route::get('home', 'HomeController@index');
        Route::get('fetch-summary', 'HomeController@fetchSummary');
        Route::get('fetch-sms-balance', 'HomeController@fetchSmsBalance');
        Route::get('fetchordersstats', 'HomeController@fetchordersStats');


//      admin  products section  //////////////////////////
        Route::get('add-product','ProductController@showAddProduct');
        Route::post('add-product','ProductController@addNewProduct');
        Route::get('edit-product/{id}','ProductController@showEditProduct');


        Route::get('manage-products','ProductController@manageProducts');
        Route::get('fetch/products', 'ProductController@fetchProducts');
        Route::post('change-product-visibility/{id}', 'ProductController@showHideProduct');
        Route::post('delete/products', 'ProductController@deleteProducts');

//        End of admin products section

//        Categories section ///////////////

        Route::get('categories', 'CategoryController@index');
        Route::get('add-category', 'CategoryController@addCategory');
        Route::post('save-category', 'CategoryController@saveCategory');
        Route::post('update-category/{id}', 'CategoryController@updateCategory');
        Route::get('show-edit-category/{id}', 'CategoryController@showEditCategory');
        Route::get('delete-category', 'CategoryController@deleteCategory');
//        End fof Categories route //////////////////


        // branches ///////////////////////
        Route::get('manage-branches','BranchController@index');
        Route::get('add-branch','BranchController@showAddBranch');
        Route::get('edit-branch/{branchId}','BranchController@showEditBranch');
        Route::get('select-branch/{branchId}', 'BranchController@selectBranch');
//        end of branches section ////////////////

//        branch manager section /////////
        Route::get('branch-managers','BranchManagerController@index');
        Route::get('add-branch-manager','BranchManagerController@add');
        Route::get('edit-branch-manager/{id}','BranchManagerController@edit');

//        post branch manager
        Route::post('add-branch-manager', 'BranchManagerController@addManager');
        Route::post('update-branch-manager', 'BranchManagerController@updateManager');

//        end of branch manager section ////////////////////

//        orders section ////////
        Route::get('orders','OrderController@orders');
        Route::get('order-detail/{id}','OrderController@orderDetail');
        Route::get('/fetch/orders/{status}','OrderController@fetchOrders');
        Route::get('/fetch/order/{id}/items','OrderController@fetchOrderItems');
        Route::get('/count-orders/{status?}','OrderController@countOrders');
        // order detail section
        Route::get('assign/order/{orderId}/tobranch/{branchId}', 'OrderController@assignOrderToBranch');
        Route::post('order/{id}/delivered', 'OrderController@markOrderItemAsDelivered');
        Route::post('order/{id}/cancelled', 'OrderController@cancelOrders');
        Route::post('order/{id}/{status}', 'OrderController@manageOrders');

    });


});

Route::get('/', function () {
    return view('welcome');
});

Route::get('/products', function(){
  return view('products');
});
Route::get('/contactus', function(){
    return view('contact');
  });
  Route::get('/aboutus', function(){
    return view('aboutus');
  });

Route::get('carts','PagesController@cart');
Route::get('checkout','PagesController@checkout');
Route::get('confirm','PagesController@confirm');
Route::get('vieworders','PagesController@viewOrders');
Route::get('orderDetails','PagesController@OrderItemDetails');
Route::get('viewbyordernumber','PagesController@ViewOrdersByOrderNumber');

Route::get('/home', 'HomeController@index')->name('home');


