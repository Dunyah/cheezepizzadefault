<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

//Route::middleware('auth:api')->get('/user', function (Request $request) {
//    return $request->user();
//});

Route::prefix('v1')->group(function (){

    Route::get('branches', 'WebsiteController@getBranches');
    Route::get('branch/{branchId}/products', 'WebsiteController@getBranchProductsGroupedByCategories');

//    Auth user route //////////////////////////////////////
    Route::post('login', 'Auth\LoginController@login');
    Route::post('register', 'Auth\RegisterController@register');

//    Authenticated user routes
    Route::middleware('auth:api')->group(function (){
        Route::post('checkout', 'WebsiteController@checkout');
        Route::get('order/{orderId}', 'WebsiteController@orderReceived');
        Route::get('orders/{status?}', 'WebsiteController@orders');
    });
});


Route::get('test',function (){
    return \App\Branch::find('3679b400-29bd-47cb-8488-29f6a7d0a625')->products;
});
