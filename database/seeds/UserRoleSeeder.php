<?php

use Illuminate\Database\Seeder;

class UserRoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
       $user = \App\User::where('email','admin@mail.com')->first();

        $user->refresh();
        $user->assignRole(['Super Admin']);
    }
}
