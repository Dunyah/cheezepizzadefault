<?php

use Illuminate\Database\Seeder;

class CategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $dataToInsert = [];

        array_push($dataToInsert, [
            'id' => \Illuminate\Support\Str::uuid(),
           'name' => 'COLD BEVERAGES',
           'created_at' => now(),
           'updated_at' => now(),
        ]);

        array_push($dataToInsert, [
            'id' => \Illuminate\Support\Str::uuid(),
            'name' => 'MILK SHAKES',
            'created_at' => now(),
            'updated_at' => now(),
        ]);

        array_push($dataToInsert, [
            'id' => \Illuminate\Support\Str::uuid(),
            'name' => 'CHICKEN WINGS',
            'created_at' => now(),
            'updated_at' => now(),
        ]);

        array_push($dataToInsert, [
            'id' => \Illuminate\Support\Str::uuid(),
            'name' => 'FRIES',
            'created_at' => now(),
            'updated_at' => now(),
        ]);

        array_push($dataToInsert, [
            'id' => \Illuminate\Support\Str::uuid(),
            'name' => 'RICE',
            'created_at' => now(),
            'updated_at' => now(),
        ]);

        array_push($dataToInsert, [
            'id' => \Illuminate\Support\Str::uuid(),
            'name' => 'PIZZAS',
            'created_at' => now(),
            'updated_at' => now(),
        ]);

        (new \App\Category())->insert($dataToInsert);
    }
}
