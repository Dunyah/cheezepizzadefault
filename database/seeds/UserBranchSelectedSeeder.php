<?php

use Illuminate\Database\Seeder;

class UserBranchSelectedSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $branch = \App\Branch::first();
        $user = \App\User::role(['Super Admin'])->first();
        \App\UserSelectedBranch::create(
            [
                'branch_id' => $branch->id,
                'user_id' => $user->id
            ]
        );
    }
}
