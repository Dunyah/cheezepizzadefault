<?php

use Illuminate\Database\Seeder;

class SizeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $dataToInsert = [];

        array_push($dataToInsert, [
            'name' => '8"',
            'id' => \Illuminate\Support\Str::uuid(),
            'created_at' => now(),
            'updated_at' => now()
        ]);

        array_push($dataToInsert, [
            'name' => '12"',
            'id' => \Illuminate\Support\Str::uuid(),
            'created_at' => now(),
            'updated_at' => now()
        ]);

        array_push($dataToInsert, [
            'name' => '14"',
            'id' => \Illuminate\Support\Str::uuid(),
            'created_at' => now(),
            'updated_at' => now()
        ]);

        array_push($dataToInsert, [
            'name' => '16"',
            'id' => \Illuminate\Support\Str::uuid(),
            'created_at' => now(),
            'updated_at' => now()
        ]);

        array_push($dataToInsert, [
            'name' => '18"',
            'id' => \Illuminate\Support\Str::uuid(),
            'created_at' => now(),
            'updated_at' => now()
        ]);

        \App\Size::insert($dataToInsert);
    }
}
