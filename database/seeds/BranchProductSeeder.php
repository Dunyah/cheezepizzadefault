<?php

use Illuminate\Database\Seeder;

class BranchProductSeeder extends Seeder
{
    private $failures = 0;
    use \App\Traits\UUIDTrait;
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        try{
            factory(App\BranchProduct::class, 20)->create();
        }catch (\Exception $exception){
            if($this->failures > 5) {
               // print_r("Seeder Error. Failure count for current entity: " . $this->failures);
                return;
            }

            $this->failures++;
            $this->run(); // retry again until the number of failure is greater than 5
        }
    }
}
