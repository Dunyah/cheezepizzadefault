<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {

         $this->call([
//             UserSeeder::class,
//             CategorySeeder::class,
//             BranchSeeder::class,
//             RoleSeeder::class,
//             UserRoleSeeder::class,
//             SizeSeeder::class,
//             ProductSeeder::class,
//             BranchProductSeeder::class,
//             ProductSizeSeeder::class,
             UserBranchSelectedSeeder::class
         ]);
//
//         \Illuminate\Support\Facades\Artisan::call('passport:client --personal');
//
//         $client = \Illuminate\Support\Facades\DB::table('oauth_clients')->first();
//
//         setEnvironmentValue([
//            'PASSPORT_PERSONAL_ACCESS_CLIENT_ID' => $client->id,
//            'PASSPORT_PERSONAL_ACCESS_CLIENT_SECRET' => $client->secret
//         ]);



    }
}
