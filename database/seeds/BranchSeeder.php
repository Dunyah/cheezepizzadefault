<?php

use Carbon\Carbon;
use Illuminate\Database\Seeder;

class BranchSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $dataToInsert = [];
        array_push($dataToInsert, [
                'id' => \Illuminate\Support\Str::uuid(),
                'name' => 'Accra Mall',
                'location' => 'Accra Mall, Opposit KFC',
                'opening_time' =>  Carbon::parse('today 6am'),
                'closing_time' => Carbon::parse('today 6pm'),
                'take_away' => true,
                'delivery' => true,
                'image' => null,
                'created_at' => now(),
                'updated_at' => now(),
            ]
        );

        array_push($dataToInsert, [
                'id' => \Illuminate\Support\Str::uuid(),
                'name' => 'East Legon',
                'location' => 'Lagos Avenue, East Legon',
                'opening_time' =>  Carbon::parse('today 6am'),
                'closing_time' => Carbon::parse('today 6pm'),
                'take_away' => true,
                'delivery' => true,
                'image' => null,
                'created_at' => now(),
                'updated_at' => now(),
            ]
        );

        array_push($dataToInsert, [
                'id' => \Illuminate\Support\Str::uuid(),
                'name' => 'Weija',
                'location' => 'Weija, Accra',
                'opening_time' =>  Carbon::parse('today 6am'),
                'closing_time' => Carbon::parse('today 6pm'),
                'take_away' => true,
                'delivery' => true,
                'image' => null,
                'created_at' => now(),
                'updated_at' => now(),
            ]
        );

        array_push($dataToInsert, [
                'id' => \Illuminate\Support\Str::uuid(),
                'name' => 'Spintex',
                'location' => 'Adjacent Ecobank Sakumono Branch, Spintex Road, Accra',
                'opening_time' =>  Carbon::parse('today 6am'),
                'closing_time' => Carbon::parse('today 6pm'),
                'take_away' => true,
                'delivery' => true,
                'image' => null,
                'created_at' => now(),
                'updated_at' => now(),
            ]
        );


        (new \App\Branch())->insert($dataToInsert);
    }
}
