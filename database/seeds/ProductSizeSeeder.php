<?php

use Illuminate\Database\Seeder;

class ProductSizeSeeder extends Seeder
{
    private $failures = 0;
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //print ('hello world');
        try{
            factory(\App\ProductSize::class, 11)->create();
        }catch (\Exception $e){
            if($this->failures > 5) {
                // print_r("Seeder Error. Failure count for current entity: " . $this->failures);
                return;
            }

            $this->failures++;
            $this->run(); // retry again until the number of failure is greater than 5
        }

    }
}
