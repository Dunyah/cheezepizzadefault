<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Product;
use Faker\Generator as Faker;

$factory->define(Product::class, function (Faker $faker) {
    return [
        'id' => \Illuminate\Support\Str::uuid(),
        'name' => $faker->realText(11),
        'description' => $faker->paragraph,
        'price' => $faker->numberBetween(20,100),
        'image' => $faker->imageUrl($width = 200, $height = 200),
        'category_id' => \App\Category::select('id')->orderByRaw("RAND()")->first()->id,
        'created_at' => now(),
        'updated_at' => now(),

    ];
});
