<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Branch;
use App\BranchProduct;
use App\Product;
use Faker\Generator as Faker;

$factory->define(BranchProduct::class,  function (Faker $faker) {

    $branchId = Branch::select('id')->orderByRaw("RAND()")->first();
    $productId = Product::select('id')->orderByRaw("RAND()")->first()->id;

    return [
        'branch_id' => $branchId,
        'product_id' => $productId,
    ];
});
