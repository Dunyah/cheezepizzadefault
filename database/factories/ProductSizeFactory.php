<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use Faker\Generator as Faker;

$factory->define(\App\ProductSize::class, function (Faker $faker) {
    //print_r(json_encode(\App\Category::where('name','PIZZAS')->first()));
    return [
        'product_id' => \App\Category::where('name','PIZZAS')->first()->products()->select('id')->orderByRaw("RAND()")->first()->id,
        'size_id' => \App\Size::select('id')->orderByRaw("RAND()")->first()->id,
        'additional_price' => $faker->numberBetween(20,100)
    ];
});
