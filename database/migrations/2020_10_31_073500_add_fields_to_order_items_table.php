<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddFieldsToOrderItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('order_items', function (Blueprint $table) {

            $table->string('product_name');
            $table->text('product_description')->nullable()->default(null);
            $table->double('product_price')->nullable()->default(0.0);
            $table->string('product_image')->nullable()->default(null);
            $table->string('category_name')->nullable()->default(null);
            $table->string('size')->nullable()->default(null);
            $table->string('additional_price')->nullable()->default(null);

            $table->dropColumn('product_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('order_items', function (Blueprint $table) {
            $table->string('product_id');
        });
    }
}
