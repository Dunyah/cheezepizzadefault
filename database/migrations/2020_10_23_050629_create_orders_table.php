<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->uuid('id')->primary();
            $table->string('number');
            $table->string('status');
            $table->string('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->string('branch_id')->references('id')->on('branches')->onDelete('cascade');
            $table->string('address');
            $table->date('expected_delivery_day');
            $table->time('expected_delivery_time');
            $table->string('recipient_phone');
            $table->text('notes')->nullable()->default(null);
            $table->boolean('delivery');
            $table->double('delivery_fee')->nullable()->default(0.00);
            $table->string('delivery_lat')->nullable()->default(null);
            $table->string('delivery_lng')->nullable()->default(null);
            $table->string('total_price')->nullable()->default(null);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
}
