<?php

return [

    'clientId' => env('PASSPORT_CLIENT_ID', null),
    'last_order_number' => env('LAST_ORDER_NUMBER', 1000),
    'eazisend_url' => env('EAZISEND_API_URL', null),
    'eazisend_client_id' => env('EAZISEND_CLIENT_ID', null),
    'eazisend_api_key' => env('EAZISEND_API_KEY', null),

    'phone_number_1' => env('PHONE_NUMBER_1', '0322498151'),
    'phone_number_2' => env('PHONE_NUMBER_2', '0322498151'),
    'contact_email' => env('EMAIL', 'contact@cheezypizza.com'),

];
