<!DOCTYPE html>
<!--[if IE 9]><html class="ie ie9"> <![endif]-->
<html>
    <head>
   @include('layouts.head')
</head>
<body>
    <!-- Header ================================================== -->
    @include('layouts.header')
    <!-- End Header =============================================== -->
<div id="app"></div>
    <!-- SubHeader =============================================== -->
    <section class="parallax-window" id="short" data-parallax="scroll" data-image-src="img/sub_header_cart.jpg" data-natural-width="1400" data-natural-height="350">
    <div id="subheader">
    	<div id="sub_content">
    	 <h1>Contact us</h1>
         <p>Qui debitis meliore ex, tollit debitis conclusionemque te eos.</p>
         <p></p>
        </div><!-- End sub_content -->
	</div><!-- End subheader -->
</section>
    <!-- End SubHeader ============================================ -->

    <div id="position">
        <div class="container">
            <ul>
                <li><a href="/">Home</a></li>
                <li><a href="#0">Contact us</a></li>
                <li>Page active</li>
            </ul>
            <!-- <a href="#0" class="search-overlay-menu-btn"><i class="icon-search-6"></i> Search</a> -->
        </div>
    </div><!-- Position -->

    <!-- Content ================================================== -->
    <div class="container margin_60_35">
        <div class="row" id="contacts">
            <div class="col-md-6 col-sm-6">
                <div class="box_style_2">
                    <h2 class="inner">Customer service</h2>
                    <p class="add_bottom_30">Adipisci conclusionemque ea duo, quo id fuisset prodesset, vis ea agam quas. <strong>Lorem iisque periculis</strong> id vis, no eum utinam interesset. Quis voluptaria id per, an nibh atqui vix. Mei falli simul nusquam te.</p>
                    <p><a href="tel://004542344599" class="phone"><i class="icon-phone-circled"></i>0544133397</a></p>
                    <p class="nopadding"><a href="mailto:customercare@quickfood.com"><i class="icon-mail-3"></i> customercare@quickfood.com</a></p>
                </div>
            </div>
            <div class="col-md-6 col-sm-6">
                <div class="box_style_2">
                    <h2 class="inner">Restaurant Support</h2>
                    <p class="add_bottom_30">Quo ex rebum petentium, cum alia illud molestiae in, pro ea paulo gubergren. Ne case constituto pro, ex vis delenit complectitur, per ad <strong>everti timeam</strong> conclusionemque. Quis voluptaria id per, an nibh atqui vix.</p>
                    <p><a href="tel://004542344599" class="phone"><i class="icon-phone-circled"></i> 0544133397</a></p>
                    <p class="nopadding"><a href="mailto:support@cheezepizza.com"><i class="icon-mail-3"></i> support@cheezepizza.com</a></p>
                </div>
            </div>
        </div><!-- End row -->
    </div><!-- End container -->
<!-- End Content =============================================== -->

<!-- Footer ================================================== -->
@include('layouts.footer')
<!-- End Footer =============================================== -->

<div class="layer"></div><!-- Mobile menu overlay mask -->

<!-- Login modal -->   
@include('layouts.modal')
    
     <!-- Search Menu -->
	<div class="search-overlay-menu">
		<span class="search-overlay-close"><i class="icon_close"></i></span>
		<form role="search" id="searchform" method="get">
			<input value="" name="q" type="search" placeholder="Search..." />
			<button type="submit"><i class="icon-search-6"></i>
			</button>
		</form>
	</div>
	<!-- End Search Menu -->
    @include('layouts.script')

</body>
</html>