@extends('administration.layout.base')
@section('content')
<div class="app-content content">
      <div class="content-wrapper">
          <div class="content-header row">
              <div class="content-header-left col-md-12 col-12 mb-2 breadcrumb-new">
                  <h3 class="content-header-title mb-0 d-inline-block">Category List</h3>
                  <div class="row breadcrumbs-top d-inline-block">
                      <div class="breadcrumb-wrapper col-12">
                          <ol class="breadcrumb">
                              <li class="breadcrumb-item"><a class="btn btn-danger btn-lg" href="{{url('administration/add-category')}}"><i class="fa fa-plus-square"></i> Add new category</a>
                              </li>
                          </ol>
                      </div>
                  </div>
              </div>

          </div>
        <div class="content-body">

<section id="configuration">
    <div class="row">
        <div class="col-md-12">
            @include('administration.layout.notifications')
        </div>
        <div class="col-12">
            <div class="card">
                <div class="card-header">
                    <h4 class="card-title">Product Category List</h4>
                </div>
                <div class="card-body">
                    <div class="card-block card-dashboard">
                        <table class="table table-striped table-bordered tagstable">
                            <thead>
                                <tr>
                                   {{--  <th><input type="checkbox" name="checkall" class="form-control"></th> --}}
                                    <th>Name</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                            	@foreach($categories as $c)
                                <tr>

                                  {{--   <td><input type="checkbox" name="checkall" class="form-control"></td> --}}
                                    <td>{{$c->name}}</td>
                                    <td>
                                    	<a data="{{$c}}" href="{{url('administration/show-edit-category/'.$c->id)}}" class="btn btn-outline-primary btn-sm"><i class="icon-head"></i> Edit</a>
                                    	<a class="btn btn-outline-danger btn-sm remove" data="{{$c->id}}"><i class="icon-trash4"></i> Delete</a>
                                    </td>
                                </tr>
                                {{-- recurse here --}}

                            @endforeach
                            </tbody>
                           {{--  <tfoot>
                                <tr>
                                    <th><input type="checkbox" name="checkall" class="form-control"></th>
                                    <th>#</th>
                                    <th>Image</th>
                                    <th>Name</th>
                                    <th>Description</th>
                                    <th>Count</th>
                                    <th>Action</th>
                                </tr>
                            </tfoot> --}}
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
 </div>
   </div>
</div>
 
@endsection

@section('scripts-below')
<script type="text/javascript">
 var table = $('.tagstable').DataTable({
    "aaSorting": []
 });
	$(document).on('click', '.remove', function (e) {
    var data = $(this).attr('data');
    e.preventDefault();  
     $tr = $(this).closest('tr');
        swal({
                 title: "Are you Sure?",
            text: "Delete Category Permanently",
            icon: "info",
            showCancelButton: true,
            buttons: {
                cancel: {
                    text: "No",
                    value: null,
                    visible: true,
                    className: "btn-danger",
                    closeModal: true,
                },
                confirm: {
                    text: "Yes",
                    value: true,
                    visible: true,
                    className: "btn-success",
                    closeModal: false
                }
            }
            }).then(function(isConfirm){
                if (isConfirm){

                    console.log(data);

                 $.get("{{url('administration/delete-category')}}",{id:data},function(response){
                 	console.log(response);
                 if(response.status ==='success'){
                     console.log(table.row($tr).remove().draw());
                     swal("Success!","Deleted Successfully","success");
                  }
                  else{
                      swal("Error!","Error Deleting Category","error");
                  }
                }).fail(function(){
                        swal("Error!","Error Deleting Tag.Check Network Connection","error");
                });
      }

  });
});


</script>
@stop

