@extends('administration.layout.base')
@section('content')
<div class="app-content content">
      <div class="content-wrapper">
        <div class="content-header row">
          <div class="content-header-left col-md-8 col-12 mb-2 breadcrumb-new">
            <div class="row breadcrumbs-top d-inline-block">
              <div class="breadcrumb-wrapper col-12">
                <ol class="breadcrumb">
                  <li class="breadcrumb-item"><a href="{{url('administration/home')}}">Home</a>
                  </li>
                  <li class="breadcrumb-item active">Add Category
                  </li>
                </ol>
              </div>
            </div>
          </div>
         
        </div>
        <div class="content-body">
<section id="configuration">
    <div class="row">
        <div class="col-md-12">
            @include('administration.layout.notifications')
        </div>
        <div class="col-12">
            <div class="card">
                <div class="card-header">
                    <h4 class="card-title">Product Categories</h4>
                </div>
                
                <div class="card-body">
                    <div class="card-block card-dashboard">
                       {{-- add category  --}}
                       <form class="form form-horizontal" method="post" action="{{url('administration/save-category')}}">
                          {{csrf_field()}}

                  <div class="form-body">
                    <h4 class="form-section"><i class="ft-user"></i>Enter New Product Category</h4>
                    <div class="form-group row">
                      <label class="col-md-3 label-control" for="name">Name</label>
                      <div class="col-md-7">
                        <input class="form-control" id="name" name="name" type="text" placeholder="Category Name" required>
                      </div>
                  </div>


            </div>
 
  <div class="form-actions center">
  <a href="{{url()->previous()}}" class="btn btn-warning mr-1">
                  <i class="ft-x"></i> Cancel
                </a>
                <button type="submit" class="btn btn-primary">
                  <i class="ft-save"></i> Save
                </button>
              </div>
   
    </form>
                       {{-- end --}}
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
 </div>
   </div>
</div>
@endsection


