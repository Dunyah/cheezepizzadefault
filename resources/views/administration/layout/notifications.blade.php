@if ($errors->any())

	@foreach ($errors->all() as $error)
		<div class="alert bg-danger alert-icon-left alert-arrow-left alert-dismissible mt-2" role="alert">
			<span class="alert-icon"><i class="fa fa-thumbs-o-down"></i></span>
			<button type="button" class="close" data-dismiss="alert" aria-label="Close">
				<span aria-hidden="true">&times;</span>
			</button>
			<strong>Whoops!</strong> {{$error}}
		</div>
	@endforeach

@endif

@if(Session::has('success-message'))
<div class="alert alert-success bg-success alert-icon-left alert-arrow-left alert-dismissible mt-2" role="alert">
	<span class="alert-icon"><i class="fa fa-thumbs-o-up"></i></span>
	<button type="button" class="close" data-dismiss="alert" aria-label="Close">
		<span aria-hidden="true">&times;</span>
	</button>
	<strong>Well done!</strong>
	{!! \Illuminate\Support\Facades\Session::get('success-message')!!}
</div>
@endif

@if(Session::has('error-message'))
<div class="alert bg-danger alert-icon-left alert-arrow-left alert-dismissible mt-2" role="alert">
	<span class="alert-icon"><i class="fa fa-thumbs-o-down"></i></span>
	<button type="button" class="close" data-dismiss="alert" aria-label="Close">
		<span aria-hidden="true">&times;</span>
	</button>
	<strong>Whoops!</strong> {!! Session::get('error-message')!!}
</div>
@endif



@if(Session::has('warning-message'))
<div class="alert bg-warning alert-icon-left alert-arrow-left alert-dismissible mt-2" role="alert">
	<span class="alert-icon"><i class="fa fa-warning"></i></span>
	<button type="button" class="close" data-dismiss="alert" aria-label="Close">
		<span aria-hidden="true">&times;</span>
	</button>
	<strong>Warning!</strong>
	{!! Session::get('warning-message')!!}
</div>
@endif

@if(Session::has('general-message'))
	<div class="alert bg-primary alert-icon-left alert-arrow-left alert-dismissible mt-2" role="alert">
		<span class="alert-icon"><i class="fa fa-heart"></i></span>
		<button type="button" class="close" data-dismiss="alert" aria-label="Close">
			<span aria-hidden="true">&times;</span>
		</button>
		<strong>Please Note!</strong>
		{!! Session::get('general-message')!!}
	</div>

@endif