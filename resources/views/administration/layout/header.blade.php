<nav class="header-navbar navbar-expand-md navbar navbar-with-menu fixed-top navbar-semi-dark navbar-shadow">
  <div class="navbar-wrapper">
    <div class="navbar-header">
      <ul class="nav navbar-nav flex-row">
        <li class="nav-item mobile-menu d-md-none mr-auto"><a class="nav-link nav-menu-main menu-toggle hidden-xs" href="#"><i class="ft-menu font-large-1"></i></a></li>
        <li class="nav-item"><a class="navbar-brand" target="_blank" href="">
            <h3 class="brand-text">Cheezy Pizza</h3></a></li>
        <li class="nav-item d-md-none"><a class="nav-link open-navbar-container" data-toggle="collapse" data-target="#navbar-mobile"><i class="fa fa-ellipsis-v"></i></a></li>
      </ul>
    </div>
    <div class="navbar-container content">
      <div class="collapse navbar-collapse" id="navbar-mobile">
        <ul class="nav navbar-nav mr-auto float-left">
          <li class="nav-item d-none d-md-block"><a class="nav-link nav-menu-main menu-toggle hidden-xs" href="#"><i class="ft-menu">         </i></a></li>

            <li class="dropdown dropdown-notification nav-item"><a class="nav-link text-primary" href="#" data-toggle="dropdown" aria-expanded="true">{{ \Illuminate\Support\Facades\Auth::user()->{'selectedBranch'}->name }} Branch</a>
                <ul class="dropdown-menu dropdown-menu-media dropdown-menu-left">
                    <li class="dropdown-menu-header">
                        <h6 class="dropdown-header m-0"><span class="grey darken-2">Switch Branch</span></h6><span class="notification-tag badge badge-default badge-danger float-right m-0">{{\App\Branch::count()}} branches</span>
                    </li>
                    <li class="scrollable-container media-list w-100 ps-container ps-theme-dark ps-active-y" data-ps-id="cdcf0dc9-db78-0d68-d56f-4f759186c9b0">

                        @foreach(\App\Branch::all() as $branch)
                        <a href="{{url('administration/select-branch/'.$branch->id)}}">
                            <div class="media">
                                <div class="media-left align-self-center"><i class="ft-plus-square icon-bg-circle bg-cyan"></i></div>
                                <div class="media-body">
                                    <h6 class="media-heading">{{$branch->name}}</h6>
                                </div>
                            </div>
                        </a>

                        @endforeach

                    </li>
                </ul>
            </li>


        </ul>
        <ul class="nav navbar-nav float-right"> 
          <li class="nav-item"></li><a class="nav-link" href="{{url('administration/auth/logout')}}">Logout</a>
        </ul>
      </div>
    </div>
  </div>
</nav>
