
<div class="main-menu menu-fixed menu-dark menu-accordion    menu-shadow " data-scroll-to-active="true">
  <div class="main-menu-content">

    <ul class="navigation navigation-main" id="main-menu-navigation" data-menu="menu-navigation">

    <li class=" nav-item"><a target="_blank" href="{{url('')}}"><i class="icon-globe"></i><span
                class="menu-title"
                data-i18n="nav.dash.main">Visit website</span></a>
    </li>

      <li class=" nav-item"><a href="{{url('administration/home')}}"><i class="icon-home"></i><span class="menu-title" data-i18n="nav.dash.main">Dashboard</span></a>
      </li>

      {{--<li class=" nav-item"><a href="{{url('administration/orders')}}"><i class="icon-folder"></i><span class="menu-title" data-i18n="nav.dash.main">Manage Orders</span><span class="badge badge-info badge-pill float-right mr-2 pending_orders"></span></a>--}}
      {{--</li>--}}

      <li class=" nav-item"><a href="#"><i class="icon-list"></i><span class="menu-title" data-i18n="nav.templates.main">Products</span></a>
          <ul class="menu-content">
              <li><a class="menu-item" href="{{url('administration/manage-products')}}" data-i18n="nav.templates.vert.classic_menu">Manage Products</a>
              </li>
              <li><a class="menu-item" href="{{url('administration/add-product')}}" data-i18n="nav.templates.vert.compact_menu">Add New Product</a>
              </li>
          </ul>
      </li>

    <li class=" nav-item"><a href="#"><i class="icon-grid"></i><span class="menu-title"
                                                                     data-i18n="nav.templates.main">Categories</span></a>
        <ul class="menu-content">
            <li><a class="menu-item" href="{{url('administration/add-category')}}"
                   data-i18n="nav.templates.vert.classic_menu">Add
                    New</a>
            </li>
            <li><a class="menu-item" href="{{url('administration/categories')}}"
                   data-i18n="nav.templates.vert.compact_menu">Category
                    List</a>
            </li>
        </ul>
    </li>


      <li class=" nav-item"><a href="{{url('administration/orders')}}"><i class="icon-folder"></i><span class="menu-title" data-i18n="nav.dash.main">Manage Orders</span><span class="badge badge-info badge-pill float-right mr-2 pending_orders none"> </span></a>
      </li>

        <li class=" nav-item"><a href="#"><i class="icon-layers"></i><span class="menu-title"
                                                                              data-i18n="nav.templates.main">Branches</span></a>
            <ul class="menu-content">
                <li><a class="menu-item" href="{{url('administration/add-branch')}}"
                       data-i18n="nav.templates.vert.classic_menu">Add
                        Add Branch</a>
                </li>
                <li><a class="menu-item" href="{{url('administration/manage-branches')}}"
                       data-i18n="nav.templates.vert.compact_menu">Branch List</a>
                </li>
            </ul>

        </li>

        <li class=" nav-item"><a href="#"><i class="icon-briefcase"></i><span class="menu-title"
                                                                              data-i18n="nav.templates.main">Administration</span></a>
            <ul class="menu-content">
                <li><a class="menu-item" href="{{url('administration/add-branch-manager')}}"
                       data-i18n="nav.templates.vert.classic_menu">Add
                        Branch Manager</a>
                </li>
                <li><a class="menu-item" href="{{url('administration/branch-managers')}}"
                       data-i18n="nav.templates.vert.compact_menu">Branch Managers</a>
                </li>
            </ul>

        </li>


        <li class=" nav-item"><a href="#"><i class="icon-graph"></i><span class="menu-title"
                                                                          data-i18n="nav.page_layouts.main">Reports</span></a>
            <ul class="menu-content">
                {{-- show graph of orders with time --}}
                <li><a href="{{url('/administration/graphs')}}" class="menu-item"
                       data-i18n="nav.page_layouts.2_columns">Graphs</a>
                </li>
                {{-- show orders in order of highest number of orders --}}
                {{--<li><a href="{{url('orders')}}" class="menu-item"--}}
                {{--data-i18n="nav.page_layouts.1_column">Report--}}
                {{--on customers</a>--}}
                {{--</li>--}}
                {{-- show products in order of sales --}}
                {{--  <li><a href="{{url('/report-on-products')}}" class="menu-item text-warning"
                data-i18n="nav.page_layouts.2_columns">Report on products</a> --}}
                {{--</li>--}}
                {{-- show order history --}}
                <li><a href="{{url('/order-history')}}" class="menu-item"
                       data-i18n="nav.page_layouts.2_columns">Order
                        history</a>
                </li>
                <li><a href="{{url('/payment-history')}}" class="menu-item"
                       data-i18n="nav.page_layouts.2_columns">Payment
                        history</a>
                </li>
            </ul>
        </li>

        <li class="navigation-header"><span data-i18n="nav.category.layouts">Useful Links</span><i
                class="ft-more-horizontal ft-minus" data-toggle="tooltip" data-placement="right"
                data-original-title="Layouts"></i>
        </li>


        <li class=" nav-item"><a target="_blank"
                                 href="https://www.zoho.com/mail/login.html"><i
                    class="icon-link"></i><span class="menu-title" data-i18n="nav.templates.main">Zoho Email</span></a>
        </li>

        <li class=" nav-item"><a target="_blank"
                                 href="https://app.eazisend.com/home"><i
                    class="icon-link"></i><span class="menu-title" data-i18n="nav.templates.main">SMS</span></a>
        </li>

        <li class=" nav-item"><a target="_blank"
                                 href="https://analytics.google.com/analytics/web/?authuser=1#/report-home/a180841992w249715209p231441972"><i
                    class="icon-link"></i><span class="menu-title" data-i18n="nav.templates.main">Google Analytics</span></a>
        </li>




        {{--<li class=" nav-item"><a target="_blank"--}}
        {{--href="https://pixinvent.com/bootstrap-admin-template/robust/html/ltr/vertical-menu-template/"><i--}}
        {{--class="icon-equalizer"></i><span class="menu-title" data-i18n="nav.templates.main">Go To Template</span></a>--}}

        {{--</li>--}}


    </ul>

  </div>
</div>
