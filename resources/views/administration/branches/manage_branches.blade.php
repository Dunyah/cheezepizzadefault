@extends('administration.layout.base')
@section('styles-below')
    <link rel="stylesheet" type="text/css" href="{{asset('admin-assets/app-assets/css/core/colors/palette-gradient.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('admin-assets/app-assets/css/plugins/animate/animate.css')}}">
@endsection
@section('content')
    <div class="app-content content">
        <div class="content-wrapper">
            <div class="content-header row">
                <div class="content-header-left col-md-12 col-12 breadcrumb-new">
                    @include('administration.layout.notifications')
                </div>

            </div>

            <div class="content-body"><!--Grid options-->

                <div class="row">
                    <div class="col-12 mt-3 mb-1">
                        <h4 class="text-uppercase">Branches</h4>
                        <p>You can quickly add more branches if you have any.</p>
                    </div>
                </div>

                <div class="row" id="content-section">
                    {{-- A branch --}}
                    <div class="col-lg-4 col-md-12">
                        <div class="card text-white box-shadow-0 bg-gradient-directional-info text-center">
                            <div class="card-header">
                                <h4 class="card-title text-white">New Branch <small><code>(important info)</code></small></h4>
                                <a class="heading-elements-toggle"><i class="fa fa-ellipsis-v font-medium-3"></i></a>
                            </div>
                            <div class="card-content collapse show">
                                <div class="card-body">
                                    <p class="card-text">  You can also assign people (Branch Managers) to these branches by giving out your <strong> secret branch link </strong>.</p>
                                    <p class="card-text"> These assigned people see and act on orders associated with the branch only</p>
                                    <a href="{{url('administration/add-branch')}}" type="button" class="btn btn-warning btn-min-width box-shadow-5 mr-1 mb-1"><i class="fa fa-code-fork"></i> Add New Branch</a>
                                </div>
                            </div>
                        </div>
                    </div>

                    {{-- Loop other branches here --}}
                        @foreach($branches as $branch)

                            <div class="col-lg-4 col-md-12">
                                <div class="card text-center">
                                    <div class="card-content">
                                        <div class="card-body">
                                            <h4 class="card-title success">{{$branch->name}}</h4>

                                            <table class="table table-bordered">
                                                {{--<tr>--}}
                                                    {{--<td>Located at: </td>--}}
                                                    {{--<td>{{$branch->location}}</td>--}}
                                                {{--</tr>--}}
                                                <tr>
                                                    <td>Opening time: </td>
                                                    <td>{{$branch->opening_time}}</td>
                                                </tr>
                                                <tr>
                                                    <td>Closing Time: </td>
                                                    <td>{{$branch->closing_time}}</td>
                                                </tr>
                                            </table>

                                            <a href="{{url('administration/edit-branch/'.$branch->id)}}" class="btn btn-success btn-lg btn-block mr-1 mt-1 mb-1"> Edit Branch </a>
                                            <button type="button" branch="{{$branch->id}}" name="{{$branch->name}}" class="btn btn-outline-warning assign-admins"><i class="fa fa-link"></i> Assign Admins</button>
                                            <button type="button" branch="{{$branch->id}}" class="btn btn-outline-primary manage-admins"> <i class="fa fa-eye"></i> Manage Admins</button>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        @endforeach


                    {{-- End of other branches loop --}}

                    {{--  assign managers to a branch modal  --}}

                    <div class="modal animated bounceInUp text-left" id="assignAdminsModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel77" aria-hidden="true">
                        <div class="modal-dialog modal-lg" role="document">
                            <div class="modal-content">
                                <div class="modal-header bg-warning white">
                                    <h4 class="modal-title white" id="myModalLabel77">Select administrators to assign to <span class="branchName">this branch<span></h4>
                                    <button type="button" class="close white" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <div class="modal-body">
                                    <div class="table-responsive">
                                        <table id="users-contacts" class="sub-shopadmins table table-white-space table-bordered row-grouping display no-wrap icheck table-middle">
                                            <thead>
                                            <tr>
                                                <th></th>
                                                <th>Name</th>
                                                <th>Email</th>
                                                <th>Phone</th>
                                                <th></th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            @foreach ($subShopadmins as $s)
                                                <tr>
                                                    <td></td>
                                                    <td>
                                                        <div class="media">
                                                            <div class="media-left pr-1"><span class="avatar avatar-sm avatar-online rounded-circle"><img src="{{asset('admin-assets/app-assets/images/portrait/small/avatar-s-2.png')}}" alt="avatar"><i></i></span></div>
                                                            <div class="media-body media-middle">
                                                                <a href="#" class="media-heading">{{$s->firstname}}</a>
                                                            </div>
                                                        </div>
                                                    </td>
                                                    <td class="text-center">
                                                        <a href="mailto:{{$s->email}}">{{$s->email}}</a>
                                                    </td>
                                                    <td>{{($s->phoneNumber) ? $s->phoneNumber : 'Not available'}}</td>
                                                    <td class="none">{{$s->id}}</td>
                                                </tr>
                                            @endforeach

                                            </tbody>
                                            <tfoot>
                                            <tr>
                                                <th></th>
                                                <th>Name</th>
                                                <th>Email</th>
                                                <th>Phone</th>
                                                <th></th>
                                            </tr>
                                            </tfoot>
                                        </table>
                                        <a href="{{url('addbranchmanager')}}" class="btn btn-outline-primary btn-lg btn-block mr-1 mt-1 mb-1">or create new branch manager</a>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn grey btn-outline-secondary" data-dismiss="modal">Close</button>
                                        <button type="button" class="btn btn-outline-primary assign-managers">Assign selected</button>
                                    </div>
                                </div>
                            </div>
                        </div>

                        {{--  end of modal  --}}

                    </div>

                </div>
                {{--End of A branch--}}

            </div>


            {{--  View assigned managers modal  --}}
            <div class="modal text-left animated bounceInDown" id="manageAdminsModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
                <div class="modal-dialog modal-lg" role="document">
                    <div class="modal-content">
                        <div class="modal-header bg-primary white">
                            <h4 class="modal-title white" id="myModalLabel1">Select administrators to remove from branch</h4>
                            <button type="button" class="close white" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <input type="hidden" id="currentBranch">
                            <div class="table-responsive">
                                <table style="width:100%" class="view-sub-shopadmins table table-striped table-bordered product-list dataex-select-checkbox">
                                    <thead>
                                    <tr>
                                        <th></th>
                                        <th>Name</th>
                                        <th>Email</th>
                                        <th>Phone</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    </tbody>
                                    <tfoot>
                                    <tr>
                                        <th></th>
                                        <th>Name</th>
                                        <th>Email</th>
                                        <th>Phone</th>
                                    </tr>
                                    </tfoot>
                                </table>

                            </div>

                            <div class="modal-footer">
                                <button type="button" class="btn grey btn-outline-secondary" data-dismiss="modal">Close</button>
                                <a href="#" class="btn btn-outline-primary" id="remove-admins">Remove them</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            {{--  End of modal  --}}

        </div>

    </div>

@endsection
@section('scripts-below')

    <script>

        var block_ele = $('#content-section');

        // View and remove branch managers -------------------
            {{--@if(count($branches) > 0)--}}
        {{--var selectedBranch = "{{$branches[0]->id}}"--}}
            {{--@endif--}}
        {{--var viewAdminInitObj = {--}}

                {{--"ajax":{--}}
                    {{--"url" : '{{url("administration/fetch/branchadministrators/")}}'+ "/" + selectedBranch,--}}
                    {{--"data" : function (data) {--}}
                        {{--console.log(data);--}}
                        {{--//return JSON.stringify(data);--}}
                    {{--}--}}
                {{--},--}}
                {{--"columns": [--}}
                    {{--{ "data": "checkbox" },--}}
                    {{--{ "data": "name" },--}}
                    {{--{ "data": "email" },--}}
                    {{--{ "data": "phone" }--}}

                {{--],--}}
                {{--columnDefs: [ {--}}
                    {{--orderable: false,--}}
                    {{--className: 'select-checkbox',--}}
                    {{--targets:   0--}}
                {{--} ],--}}
                {{--select: {--}}
                    {{--style:    'multi',--}}
                    {{--selector: 'td:first-child'--}}
                {{--}--}}

            {{--};--}}

        {{--var view_admins_table = $('.view-sub-shopadmins').DataTable(viewAdminInitObj);--}}

        // manage admins button clicked
        $('.manage-admins').click(function(){
            selectedBranch = $(this).attr('branch');
            console.log(selectedBranch)

            $('#currentBranch').val(selectedBranch);
            //table.ajax.reload();

            $('#manageAdminsModal').modal('show');
            var newUrl = '{{url("administration/fetch/branchadministrators/")}}'+ "/" + selectedBranch;
            view_admins_table.ajax.url(newUrl);
            view_admins_table.ajax.reload();

        });

        $('#remove-admins').click(function(e){
            e.preventDefault();
            var url = "{{url('administration/remove-admins-from-branch')}}"

            if(view_admins_table.rows('.selected').data().length < 1){
                swal("Whoops!", "You forgot to select the administrators");
                return;
            }

            //continue here
            var pData = view_admins_table.rows('.selected').data();
            console.log(pData);

            var jsonData = {};

            for(var i = 0; i< pData.length; i++){
                jsonData[i] = pData[i];
            }

            var data = {
                '_token':'{{csrf_token()}}',
                'branchId': $('#currentBranch').val(),
                'shopadmins': jsonData
            };

            $('#manageAdminsModal').modal('hide');
            load(block_ele);

            $.post(url, data, function(result){
                console.log(result)
                unload(block_ele);

                if(result.status){
                    successToast('selected managers removed from branch');
                    return
                }

                errorToast('unable to remove selected managers')


            })

        });

        // assign branch managers ---------------
        var table = $('.sub-shopadmins').DataTable({
            columnDefs: [ {
                orderable: false,
                className: 'select-checkbox',
                targets:   0
            } ],
            select: {
                style:    'multi',
                selector: 'td:first-child'
            }
        });

        // assign admins button clicked

        $('.assign-admins').click(function(){
            var branchName = $(this).attr('name');
            var currentBranch = $(this).attr('branch');
            $('#currentBranch').val(currentBranch);
            $('.branchName').html(branchName);
            $('#assignAdminsModal').modal('show');
        });


        $('.assign-managers').click(function(){

            // console.log(table.rows('.selected').data());
            if(table.rows('.selected').data().length < 1){
                swal("Whoops!", "You forgot to select the administrators");
                return;
            }

            var pData = table.rows('.selected').data();
            console.log(pData);

            var jsonData = {};

            for(var i = 0; i< pData.length; i++){
                jsonData[i] = pData[i];
            }


            // ajax request to assign branch to managers
            var url = "{{url('administration/assignbranchtomanagers')}}";


            var data = {
                '_token':'{{csrf_token()}}',
                'branchId': $('#currentBranch').val(),
                'shopadmins': jsonData
            };

            $('#assignAdminsModal').modal('hide');

            load(block_ele);

            $.post(url,data,function (result) {

                console.log(result);
                unload(block_ele);

                if(result.status === false){
                    errorToast(result.message);
                    return;
                }

                successToast(result.message);

//                        table.ajax.reload();
            });

        });

        $('#add-new-branch').on('click', function () {
            swal("Enter Branch Name", {
                content: "input",
                buttons: {
                    cancel: {
                        text: "No, cancel!",
                        value: null,
                        visible: true,
                        className: "",
                        closeModal: true
                    },
                    confirm: {
                        text: "Yes, Continue!",
                        value: true,
                        visible: true,
                        className: "",
                        closeModal: true
                    }
                }
            }).then(function (value) {
                if (value === false || value === null) return false;
                if (value === "") {
                    swal("Enter a branch name", "", "error");
                    return false;
                }

                var data = {
                    '_token':'{{csrf_token()}}',
                    'branchName':value
                };

                load(block_ele);
                $.post("addnewbranch",data,function (result) {
                    unload(block_ele);
                    console.log(result);
                    if(result.status === true){
                        location.reload();
                        return;
                    }
                    errorToast(result.message);

                });
            });
        });

        function load(block_ele) {
            $(block_ele).block({
                message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                overlayCSS: {
                    backgroundColor: '#fff',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        }

        function unload(block_ele) {
            if(block_ele !== 'undefined'){
                $(block_ele).unblock();
            }
        }

        // remove selected administrators from branch ---------
        $('.remove-selected-admin').click(function(){
            // check if any rows are selected
            // console.log(table.rows('.selected').data());
            if(view_admins_table.rows('.selected').data().length < 1){
                swal("Whoops!", "You forgot to select the administrators");
                return;
            }

            // ask person if he's sure to remove admins
            swal({
                title: "Are you sure?",
                text: "Permanently remove selected managers",
                icon: "warning",
                buttons: {
                    cancel: {
                        text: "No, cancel!",
                        value: null,
                        visible: true,
                        className: "",
                        closeModal: true
                    },
                    confirm: {
                        text: "Yes, Continue!",
                        value: true,
                        visible: true,
                        className: "",
                        closeModal: true
                    }
                }
            }).then(function (isConfirm) {
                if(isConfirm){
                    console.log('i am here');
                    var url = "remove/shopadministratorsfrombranch";

                    var mData = view_admins_table.rows('.selected').data();
                    console.log(mData);

                    var jsonData = {};

                    for(var i = 0; i< mData.length; i++){
                        jsonData[i] = mData[i];
                    }

                    var currentBranch = $('#currentBranch').val();
                    console.log('current branch after confirm is ' + currentBranch);

                    var data = {
                        '_token':'{{csrf_token()}}',
                        'branchId': currentBranch,
                        'shopadmins': jsonData
                    };

                    // $('#manageAdminsModal').modal('hide');

                    load(block_ele);
                    $.post(url,data,function(result){

                        $('#currentBranch').val(selectedBranch);
                        //table.ajax.reload();
                        var newUrl = '{{url("administration/fetch/branchadministrators/")}}'+ "/" + selectedBranch;
                        view_admins_table.ajax.url(newUrl);
                        view_admins_table.ajax.reload();

                        console.log(result);
                        unload(block_ele);

                        if(result.status){
                            successToast(result.message)
                            return
                        }

                        errorToast(result.message)

                    });
                }
                else{
                    swal("Cancelled", "action aborted :)", "error");
                }
            });

            // ajax request to remove admin
        });
    </script>
@endsection



