@extends('administration.layout.base')
@section('content')
    <div class="app-content content">
        <div class="content-wrapper">
            <div class="content-header row">
                <div class="content-header-left col-md-8 col-12 mb-2 breadcrumb-new">
                    <div class="row breadcrumbs-top d-inline-block">
                        <div class="breadcrumb-wrapper col-12">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="{{url('administration/home')}}">Home</a>
                                </li>
                                <li class="breadcrumb-item active">Edit Branch Details
                                </li>
                            </ol>
                        </div>
                    </div>
                </div>

            </div>
            <div class="content-body">
                <section id="configuration">
                    <div class="row">
                        <div class="col-md-12">
                            @include('administration.layout.notifications')
                        </div>
                        <div class="col-12">
                            <div class="card">
                                <div class="card-header">
                                    <h4 class="card-title">Edit Branch</h4>
                                </div>

                                <div class="card-body">
                                    <div class="card-block card-dashboard">
                                        {{-- add category  --}}
                                        <form class="form update-branch-info-form" action="{{url('administration/update/branch/'.$branch->id)}}">
                                            <div class="form-body">
                                                <h4 class="form-section"><i class="ft-user"></i> Branch Info</h4>
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="projectinput1">Branch Name</label>
                                                            <input type="text" class="form-control" id="branch-name" placeholder="Branch Name" value="{{$branch->name}}" name="name">
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="projectinput2">Active Phone Number</label>
                                                            <input type="text" id="phone" value="{{$branch->phone}}" class="form-control" name="phone">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    {{--<div class="col-md-6">--}}
                                                    {{--<div class="form-group">--}}
                                                    {{--<label for="projectinput1">Region</label>--}}
                                                    {{--<select class="form-control" id="region">--}}
                                                    {{--<option value=''>Select Region</option>--}}
                                                    {{--<option value="Greater Accra">Greater Accra</option>--}}
                                                    {{--<option value="Brong Ahafo">Brong Ahafo</option>--}}
                                                    {{--<option value="Ashanti">Ashanti</option>--}}
                                                    {{--<option value="Eastern">Eastern</option>--}}
                                                    {{--<option value="Western">Western</option>--}}
                                                    {{--<option value="Central">Central</option>--}}
                                                    {{--<option value="Upper East">Upper East</option>--}}
                                                    {{--<option value="Upper West">Upper West</option>--}}
                                                    {{--<option value="Northern">Northern</option>--}}
                                                    {{--<option value="Volta">Volta</option>--}}
                                                    {{--</select>--}}
                                                    {{--</div>--}}
                                                    {{--</div>--}}
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <input type="hidden" id="longitude" value="{{$branch->longitude}}">
                                                            <input type="hidden" id="latitude" value="{{$branch->latitude}}">
                                                            <label for="search_location">Location on map</label>
                                                            <input type="text" id="search_location" value="{{$branch->place}}" class="form-control" name="search_location">
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="description">Opening/Closing Time</label>
                                                            <select name="openclose" id="openclose" class="form-control">
                                                                <option value="allday" {{$branch->opentime === null?"selected='selected'":''}}>We are open 24 Hours</option>
                                                                <option value="custom" {{$branch->opentime !== null?"selected='selected'":''}}>Select Custom Duration</option>
                                                            </select>

                                                        </div>
                                                    </div>
                                                    <div class="col-md-3 pickatime1" {{$branch->opentime === null?"style=display:none":''}}>
                                                        <div class="form-group">
                                                            <label for="description">Opening Time</label>
                                                            <input type='text' name="opentime" class="form-control pickatime" placeholder="Select Start Time" id="opentime" value="{{$branch->opentime}}"/>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-3 pickatime1" {{$branch->opentime === null?"style=display:none":''}}>
                                                        <div class="form-group">
                                                            <label for="description">Closing Time</label>
                                                            <input type='text' id="closetime" name="closetime" class="form-control pickatime" placeholder="Select Closing Time" value="{{$branch->closetime}}"/>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-6 px-3">
                                                        <div class="form-group">
                                                            <label for="branches">Your Services</label>


                                                            <div class="row skin skin-flat">
                                                                <fieldset style="display:block">
                                                                    <input type="checkbox" name="customerdelivery" value="1" {{$branch->customerdelivery=='1'?'checked="checked"':''}}>
                                                                    <label for="input-15">We do delivery </label>
                                                                </fieldset>&nbsp;&nbsp;&nbsp;
                                                            </div>

                                                            <div class="row skin skin-flat">
                                                                <fieldset style="display:block">
                                                                    <input type="checkbox" name="customerpickup" value="1" {{$branch->customerpickup=='1'?'checked="checked"':''}}>
                                                                    <label for="input-15">Customer can pick up item</label>
                                                                </fieldset>&nbsp;&nbsp;&nbsp;
                                                            </div>

                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="row">
                                                    {{--  <div class="col-md-12">
                                                        <div class="form-group">
                                                            <label for="projectinput2">Region</label>
                                                            <input type="text" id="projectinput2" class="form-control" placeholder="Last Name" name="lname">
                                                        </div>
                                                    </div>  --}}

                                                    {{--<div class="col-md-12">--}}
                                                    {{--<div class="form-group">--}}
                                                    {{--<label for="description">Description</label>--}}
                                                    {{--<textarea id="description" rows="5" class="form-control" name="description" placeholder="Further notes about this branch">{{$branch->description}}</textarea>--}}
                                                    {{--</div>--}}
                                                    {{--</div>--}}

                                                </div>

                                            </div>

                                            <div class="form-actions">
                                                <button type="button" class="btn btn-warning mr-1">
                                                    <i class="ft-x"></i> Cancel
                                                </button>
                                                <button type="button" class="btn btn-primary update-branch-info">
                                                    <i class="fa fa-check-square-o"></i> Save
                                                </button>
                                            </div>
                                        </form>
                                        {{-- end --}}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        </div>
    </div>
@endsection


