@extends('administration.layout.base')
@section('styles-below')
    <link rel="stylesheet" type="text/css" href="{{asset('admin-assets/app-assets/css/core/menu/menu-types/vertical-menu.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('admin-assets/app-assets/css/core/colors/palette-gradient.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('admin-assets/app-assets/fonts/simple-line-icons/style.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('admin-assets/app-assets/vendors/css/cryptocoins/cryptocoins.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('admin-assets/app-assets/vendors/css/tables/datatable/select.dataTables.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('admin-assets/app-assets/vendors/css/tables/extensions/buttons.dataTables.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('admin-assets/app-assets/vendors/css/tables/datatable/buttons.bootstrap4.min.css')}}">
@endsection
@section('content')
    <div class="app-content content">
        <div class="content-wrapper">
            <div class="content-header row">
                <div class="content-header-left col-md-12 col-12 mb-2 breadcrumb-new">
                    <h3 class="content-header-title mb-0 d-inline-block">Manage Orders</h3>
                    <div class="row breadcrumbs-top d-inline-block">
                        <div class="breadcrumb-wrapper col-12">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="{{url('/shopadmin/home')}}">Home</a>
                                </li>
                            </ol>
                        </div>
                    </div>
                </div>



            </div>

            <div class="content-body"><!--Grid options-->
                {{-- Figure Statistics --}}
                @include('administration.layout.notifications')

                <div class="row">
                    <div class="col-xl-6 col-lg-6 col-12">
                        <div class="card">
                            <div class="card-content">
                                <div class="media align-items-stretch">
                                    <div class="p-2 text-center bg-warning rounded-left">
                                        <i class="fa fa-exclamation-circle font-large-2 text-white"></i>
                                    </div>
                                    <div class="p-2 media-body">
                                        <h5>Pending orders</h5>
                                        <h5 class="text-bold-400 mb-0 pending_orders"></h5>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-6 col-lg-6 col-12">
                        <div class="card">
                            <div class="card-content">
                                <div class="media align-items-stretch">
                                    <div class="p-2 text-center bg-success rounded-left">
                                        <i class="fa fa-truck-loading font-large-2 text-white"></i>
                                    </div>
                                    <div class="p-2 media-body">
                                        <h5>Completed <a href="{{url('order-history')}}" class="btn btn-sm btn-success pull-right">view</a></h5>
                                        <h5 class="text-bold-400 mb-0 completed_orders"></h5>
                                    
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    
                </div>
            
            
        
                        <div class="row">
                            <div class="col-12">
                                {{-- Data table starts here --}}
                                <div class="card">
                                    <div class="card-header">
                                        <h4 class="card-title text-white btn btn-warning"> Pending Orders  
                                        </h4>
                                      
                                        
                                        <a class="heading-elements-toggle"><i class="fa fa-ellipsis-v font-medium-3"></i></a>
                                    
                                        <div class="heading-elements">
                                            <ul class="list-inline mb-0">
                                                <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                                                <li><a class="overlay-unblock"><i class="ft-rotate-cw"></i></a></li>
                                                <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                                            </ul>
                                        </div>

                                    </div>
                                    <div class="card-content collapse show">
                                        <div class="card-body card-dashboard">
                                            {{--<p class="card-text">DataTables has most features enabled by default, so all you need to do to use it with your own ables is to call the construction function: $().DataTable();.</p>--}}
                                            <div class="table-responsive">
                                                <table class="table table-striped table-bordered order-list dataex-select-checkbox">
                                                    <thead>
                                                    <tr>
                                                        <th>Order Number</th>
                                                        <th>Customer</th>
                                                        <th>Amount GHC</th>
                                                        <th>Date Ordered</th>
                                                        <th>Action</th>
                                                    </tr>
                                                    </thead>
                                                    <tbody>
                                                        
                                                    </tbody>
                                                    <tfoot>
                                                    <tr>
                                                        <th>Order Number</th>
                                                        <th>Customer</th>
                                                        <th>Amount GHC</th>
                                                        <th>Date Ordered</th>
                                                        <th>Action</th>
                                                    </tr>
                                                    </tfoot>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                            {{-- Data table ends here --}}
                            </div>

                        </div>
                {{-- Figure Statistics --}}
            </div>

        </div>

    </div>

@endsection
@section('scripts-below')

    <script src="{{asset('admin-assets/app-assets/vendors/js/tables/datatable/dataTables.responsive.min.js')}}" type="text/javascript"></script>
    <script src="{{asset('admin-assets/app-assets/vendors/js/tables/datatable/dataTables.select.min.js')}}" type="text/javascript"></script>
    <script src="{{asset('admin-assets/app-assets/vendors/js/tables/datatable/dataTables.buttons.min.js')}}" type="text/javascript"></script>
    <script src="{{asset('admin-assets/app-assets/vendors/js/tables/datatable/buttons.bootstrap4.min.js')}}" type="text/javascript"></script>
    <script>
        // data tables here -------------------
$(document).ready(function(){


        var block_ele = $('.overlay-unblock').closest('.card');
        $('.product-details').on('click', function () {
            alert();
        });

        var table = $('.order-list').DataTable({
            "ajax":{
                "url" : "{{url('administration/fetch/orders/pending')}}",
                "dataSrc" : function (data) {
                    if(data !== 'undefined'){
                        return data.data;
                    }
                }
            },
            "columns": [
                { "data": "number" },
                { "data": "customer" },
                { "data": "total_price" },
                { "data" : "date_ordered" },
                { "data" : "action" }

            ]
        });
    })
    </script>

    {{--             $('.pending').html(data.pending);
                        $('.completed').html(data.completed);--}}
    <script>
        $.get('{{url("administration/count-orders/completed")}}', function (result) {
            var pendingOrder = $('.completed_orders');
            if(result.status){
                pendingOrder.removeClass('none');
            }
            pendingOrder.html(result.count);

        });
    </script>

@endsection
