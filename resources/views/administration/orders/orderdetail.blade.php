@extends('administration.layout.base')
@section('styles-below')
    <link rel="stylesheet" type="text/css" href="{{asset('admin-assets/app-assets/vendors/css/tables/datatable/select.dataTables.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('admin-assets/app-assets/vendors/css/tables/extensions/buttons.dataTables.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('admin-assets/app-assets/vendors/css/tables/datatable/buttons.bootstrap4.min.css')}}">
@endsection
@section('content')
    <div class="app-content content">
        <div class="content-wrapper">
            <div class="content-header row">
                <div class="content-header-left col-md-12 col-12 mb-2 breadcrumb-new">
                        <div class="row breadcrumbs-top d-inline-block mr-2">
                            <div class="breadcrumb-wrapper col-12">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="{{url('/administration/orders')}}"><i class="fa fa-arrow-left mr-1"></i> back to orders</a>
                                    </li>
                                </ol>
                            </div>
                        </div>
                    <h3 class="content-header-title mb-0 d-inline-block">Order Detail</h3>

                    <div class="row breadcrumbs-top d-inline-block">
                        <div class="breadcrumb-wrapper col-12">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="{{url('/administration/orders')}}"> #{{$order->number}}</a>
                                </li>
                            </ol>
                        </div>
                    </div>
                </div>

            </div>

            <div class="content-body"><!--Grid options-->
                @include('administration.layout.notifications')
                {{-- Figure Statistics --}}
                <div class="row">
                    <div class="col-md-3">
                            <div class="card">
                                <div class="text-center">
                                        {{-- <div class="card-body">
                                            <img src="{{asset('admin-assets/app-assets/images/portrait/medium/avatar-m-4.png')}}" class="rounded-circle  height-150" alt="Card image">
                                        </div> --}}
                                    <div class="card-body">
                                        <h4 class="card-title">{{$order->user->name}}</h4>
                                        <h6 class="card-subtitle text-muted">{{$count}} item{{($count > 1) ? 's':''}} ordered <br> on <br> {{Carbon\Carbon::parse($order->created_at)->toDayDateTimeString()}} </h6>
                                    </div>
                                </div>
                                <div class="list-group list-group-flush">
                                    <a href="mailto:{{$order->user->email}}" class="list-group-item"><i class="ft-mail"></i> {{$order->user->email}} <br><small class="text-success">(Tap to email customer directly)</small></a>
                                    <a href="tel:{{$order->user->phone}}" class="list-group-item"><i class="ft-phone"></i> {{$order->user->phone}} <br><small class="text-success">(Tap to call customer)</small></a>
                                    <a href="#" class="list-group-item"> <i class="ft-compass"></i> {{$order->address}} <br><small class="text-success">(Tap to view location on map)</small></a>
                                    <a href="#" class="list-group-item"> <i class="fa fa-truck"></i> {{$order->delivery == '1' ? 'Deliver to me' : "I'll pick it up"}}</a>
                                    <a href="#" class="list-group-item"> <i class="ft-credit-card"></i> {{$order->payment_option == '1' ? 'Pay on delivery' : 'Pay online' }}</a>
                                    <a href="#" class="list-group-item"> <i class="fa fa-code-fork"></i> Ordered to {{$order->branch->name}}</a> 
                                    <a href="#" class="list-group-item btn btn-primary btn-min-width" data-toggle="modal" data-target="#defaultSize"> <i class="fa fa-code-fork"></i> Re-assign order to another branch </a>
                                </div>
                            </div>
                    </div>
                    <div class="col-md-9">
                        <div class="card">
                            <?php
                                $subtotal = $order->total_price;
                                $deliveryFee = $order->delivery_fee ? $order->delivery_fee : 0.00 ;
                                $couponValue = $order->coupon_value ? $order->coupon_value : 0.00;

                                $total = $subtotal + $deliveryFee - $couponValue;
                            ?>
                            <div class="card-header"> <h3> Order summary : GHC {{$total}}</h3></div>
                            <div class="card-content">
                                <div class="card-body">
                                    <div class="table-responsive">
                                        <table class="table table-bordered table-striped">
                                            <tr>
                                                <td>Number of items ordered</td>
                                                <td>{{count($order->items)}}</td>
                                            </tr>
                                            <tr>
                                                <td>Total amount of items ordered </td>
                                                <td>GHC {{$order->total_price}}</td>
                                            </tr>
                                            <tr>
                                                <td>Delivery Fee</td>
                                                <td class="text-success">GHC {{$order->delivery_fee ? $order->delivery_fee : 0.00}} &nbsp;&nbsp;@if($order->haspaiddelivery) <span class="badge badge-success">Paid</span>@endif</td>
                                            </tr>
                                            {{--<tr>--}}
                                                {{--<td>Coupon used</td>--}}
                                                {{--<td class="text-danger">--}}
                                                   {{--- GHC {{$order->coupon_value ? $order->coupon_value : 0.00}}&nbsp;&nbsp;--}}
                                                   {{--@if($order->hasappliedcoupon) <span class="badge badge-info">Applied</span>@endif--}}
                                                {{--</td>--}}
                                            {{--</tr>--}}
                                            @if($order->coupon)
                                                <tr>
                                                    <td>Coupon was provided by </td>
                                                    <td>
                                                        @if($order->coupon->shop_id == 0)
                                                            ShopWithVim &nbsp;&nbsp;
                                                            @if($order->coupon->date_paid)
                                                            &nbsp;&nbsp;&nbsp; <a class="btn btn-success btn-sm text-white">Payment made on {{\Carbon\Carbon::parse($order->date_paid)->toDayDateTimeString()}}</a>
                                                                @elseif($order->coupon->date_requested)
                                                                <a class="btn btn-warning btn-sm ">Payment pending. requested on {{\Carbon\Carbon::parse($order->date_requested)->toDayDateTimeString()}}</a>
                                                                @else
                                                                <a href="{{url('administration/request/for/coupon/'.$order->coupon->id.'/payment/'.$order->id)}}" class="btn btn-danger btn-sm">Request for payment</a>
                                                            @endif

                                                            @else
                                                            You
                                                        @endif

                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>Coupon code </td>
                                                    <td>
                                                        <code>{{$order->coupon->coupon_code}}</code>

                                                    </td>
                                                </tr>
                                            @endif
                                            <tr>
                                                <td>Date to be delivered</td>
                                                <td class=" btn btn-block btn-success">{{$order->expected_delivery_day ? \Carbon\Carbon::parse($order->expected_delivery_day)->toFormattedDateString() : " - "}} &nbsp;
                                                    {{--&nbsp;@if($order->haspaiddelivery) <span class="badge badge-success">Paid</span>@endif</td>--}}
                                            </tr>
                                        </table>
                                    </div>
                                </div>

                            </div>
                        </div>
                            <div class="card overlay-unblock">
                                    <div class="card-header">
                                            <h4 class="card-title">
    
                                                {{-- actions on order --}}
                                                <div class="btn-group">
                                                    <button class="btn btn-info dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="icon-settings mr-1"></i> Select items and take action</button>
                                                    <div class="dropdown-menu arrow" >
                                                        {{--<a class="dropdown-item isAvailable text-success" href="{{url('/administration/availability/1')}}"><i class="fa fa-life-ring mr-1"></i>  Mark selected items as available </a>--}}
                                                        {{--<div class="dropdown-divider"></div>--}}
                                                        {{--<a class="dropdown-item isAvailable text-danger" href="{{url('/administration/availability/0')}}"><i class="fa fa-life-ring mr-1"></i> Mark selected items as not available </a>--}}
                                                        {{--<div class="dropdown-divider"></div>--}}
                                                        <a class="dropdown-item" href="{{url('administration/order/'.$order->id.'/confirmed')}}" id="mark-as-confirmed"><i class="fa fa-refresh mr-1"></i>Confirm Order </a>
                                                        <div class="dropdown-divider"></div>
                                                        <a class="dropdown-item" href="{{url('administration/order/'.$order->id.'/shipped')}}" id="mark-as-shipped"><i class="fa fa-truck mr-1"></i> Mark order as shipped </a>
                                                        <div class="dropdown-divider"></div>
                                                        <a class="dropdown-item text-success" href="{{url('administration/order/'.$order->id.'/delivered')}}" id="mark-as-delivered"><i class="fa fa-check mr-1"></i> Mark selected as delivered</a>
                                                        <div class="dropdown-divider"></div>
                                                        <a class="dropdown-item text-danger" href="{{url('administration/order/'.$order->id.'/cancelled')}}" id="cancel-selected"><i class="fa fa-trash mr-1"></i> Cancel selected items</a>
                                                        <div class="dropdown-divider"></div>

                                                    </div>
                                    
                                                </div>
                             
                                            </h4>
                                          
                                            
                                            <a class="heading-elements-toggle"><i class="fa fa-ellipsis-v font-medium-3"></i></a>
                                        
                                            <div class="heading-elements">
                                                <ul class="list-inline mb-0">
                                                    <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                                                    <li><a class="overlay-unblock"><i class="ft-rotate-cw"></i></a></li>
                                                    <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                                                </ul>
                                            </div>
    
                                    </div>
                                    <div class="card-content collapse show">
                                        <div class="card-body card-dashboard">
                                                <div class="table-responsive">
                                                        <table class="table table-striped table-bordered order-list dataex-select-checkbox" url="{{url('administration/fetch/order/'.$order->id.'/items')}}">
                                                            <thead>
                                                            <tr>
                                                                <th></th>
                                                                <th>Item</th>
                                                                <th>Name</th>
                                                                <th>Quantity</th>
                                                                <th>Size</th>
                                                                <th>Sub Total (GHS)</th>
                                                                <th>Order Status</th>
                                                    
                                                            </tr>
                                                            </thead>
                                                            <tbody>
                                                            </tbody>
                                                            <tfoot>
                                                            <tr>
                                                                    <th></th>
                                                                    <th>Item</th>
                                                                    <th>Name</th>
                                                                    <th>Quantity</th>
                                                                    <th>Size</th>
                                                                    <th>Sub Total</th>
                                                                    <th>Order Status</th>
                                                                  
                                                            </tr>
                                                            </tfoot>
                                                        </table>
                                                    </div>
                                        </div>
                                    </div>
                                   
                            </div>
                        {{-- end to cart list table  --}}
                            <div class="modal animated pulse text-left" id="defaultSize" tabindex="-1" role="dialog" aria-labelledby="myModalLabel18" aria-hidden="true">
                                <div class="modal-dialog" role="document">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h4 class="modal-title" id="myModalLabel18"><i class="fa fa-code-fork mr-1"></i> Select branch to assign order</h4>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                            </button>
                                        </div>
                                        <div class="modal-body">
                                                @foreach ($branches as $b)
                                                    <h5><a href="{{url('administration/assign/order/'.$order->id.'/tobranch/'.$b->id)}}"> <i class="fa fa-arrow-right mr-2"></i> {{$b->name}} </a></h5>
                                                    <hr>
                                                @endforeach 
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn grey btn-outline-secondary" data-dismiss="modal">Close</button>
                    
                                        </div>
                                    </div>
                                </div>
                            </div> 
                        {{-- end of col-md-9 --}}

                    </div>
                </div>
                {{-- Figure Statistics --}}
            </div>

        </div>

     </div>
{{-- added modal here --}}
{{--  assign managers to a branch modal  --}}

<div class="modal animated bounceInUp text-left" id="assignCourierModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel77" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header bg-warning white">
                <h4 class="modal-title white" id="myModalLabel77">Select Courier to dispatch product</h4>
                <button type="button" class="close white" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                </div>
                <div class="modal-body">
                    <div class="table-responsive">
                        <table id="users-contacts" class="sub-shopadmins table table-white-space table-bordered row-grouping display no-wrap icheck table-middle view-sub-shopadmins" style="width: 100%">
                            <thead>
                                <tr>
                                    <th></th>
                                    <th>Image</th>
                                    <th>Courier Name</th>
                                    <th>Email</th>
                                    <th>Phone</th>
                                </tr>
                            </thead>
                            <tbody>
                         
                                
                            </tbody>
                            <tfoot>
                                    <th></th>
                                    <th>Image</th>
                                    <th>Courier Name</th>
                                    <th>Email</th>
                                    <th>Phone</th>
                            </tfoot>
                        </table>
                        <a href="{{url('administration/couriers')}}" class="btn btn-outline-primary btn-lg btn-block mr-1 mt-1 mb-1">or connect to a new delivery service</a>
                    </div>  
                <div class="modal-footer">
                <button type="button" class="btn grey btn-outline-secondary" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-outline-primary assign-managers" id="assign">Assign to Selected</button>
                </div>
            </div>
        </div>
    </div>

    {{--  end of modal  --}}

</div>

{{-- assign to different shop modal  --}}
<div class="modal animated bounceInUp text-left" id="reassignModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel77" aria-hidden="true">
        <div class="modal-dialog modal-md" role="document">
            <div class="modal-content">
                <div class="modal-header bg-warning white">
                <h4 class="modal-title white" id="myModalLabel777">Reassign Order</h4>
                <button type="button" class="close white" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                </div>
            <form action="{{url('/reassignorder')}}" method="POST" id="reassignform">
                {{csrf_field()}}
                <input type="hidden" name="orderid" value="{{$order->id}}">
                <input type="hidden" name="ordersid" id="ordersid">
                <div class="modal-body">
                    <div class="row">
                        <div class="col-12">
                                <div class="form-group">
                        <label for="shop">Shop</label>
                        <select name="shop" id="shop" class="form-control" required>
                            <option value="">Select Shop</option>
                            {{--@foreach(\App\Shop::where('published',1)->get() as $s)--}}
                        {{--<option value="{{$s->id}}">{{$s->name}} - ({{$s->shopcategory->name}})</option>--}}
                            {{--@endforeach--}}
                        </select>
                        </div>
                        </div>
                    </div>

                    <div class="row">
                            <div class="col-12">
                                    <div class="form-group">
                            <label for="branch">Branch</label>
                            <select name="branch" id="branch" class="form-control" required>
                                <option value="">Select Branch</option>
                            </select>
                            </div>
                            </div>
                        </div>
                   
              
                <div class="modal-footer">
                <button type="button" class="btn grey btn-outline-secondary" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-outline-primary reassignorderbtn" id="assign">Assign to Selected</button>
                </div>
                
            </div>
                </form>
        </div>
    </div>

    {{--  end of modal  --}}

</div>

{{-- end --}}

{{-- end of modal --}}
@endsection
@section('scripts-below')

    <script src="{{asset('admin-assets/app-assets/vendors/js/tables/datatable/dataTables.responsive.min.js')}}" type="text/javascript"></script>
    <script src="{{asset('admin-assets/app-assets/vendors/js/tables/datatable/dataTables.select.min.js')}}" type="text/javascript"></script>
    <script src="{{asset('admin-assets/app-assets/vendors/js/tables/datatable/dataTables.buttons.min.js')}}" type="text/javascript"></script>
    <script src="{{asset('admin-assets/app-assets/vendors/js/tables/datatable/buttons.bootstrap4.min.js')}}" type="text/javascript"></script>
    <script>
        var _token = '{{csrf_token()}}';
    </script>
    <script src="{{asset('admin-assets/assets/js/orders.js')}}" type="text/javascript"></script>
    <script>
        var arr = Array('{{$order->user->id}}')
        $('#message-receipients').val(arr);
    </script>
    <script>
$(function(){


    $('.reassigntodiffshop').on('click',function(e){
        e.preventDefault();
        if(table.rows('.selected').data().length < 1){
                swal("Whoops!", "You forgot to select Products");
                return;
            }
            var pData = table.rows('.selected').data();
            console.log(pData);
            var jsonData = [];
            for(var i = 0; i< pData.length; i++){
                //jsonData[i] = pData[i];
                jsonData.push(pData[i]['id']);
            }
            $('#ordersid').val(jsonData.join(','));
        $('#reassignModal').modal('show');
    });
    $('#shop').change(function(){
        $('select[name="branch"]').html('<option value="">Select Branch</option>');
        if($(this).val()!==''){
            $.get("{{url('/fetchbranch/')}}"+'/'+$(this).val(),function(res){
                if(res){
                    var data = res.data;
                    var html = '';
                    for(var i=0;i<data.length;i++){
                        html+='<option value="'+data[i].id+'">'+data[i].name+'</option>';
                    }
                    $('select[name="branch"]').html(html);
                }
            })
        }
    });

    $('.reassignorderbtn').on('click',function(){
        if(confirm('Sure to continue with the Assignment')){
            $('#reassignform').submit();
        }

    })
    
    $('#assign').on('click',function(){
        if(window.view_admins_table.rows('.selected').data().length < 1){
                swal("Whoops!", "You forgot to select Courier");
                return;
            }

        var pData = table.rows('.selected').data();
            console.log(pData);
            var jsonData = {};
            for(var i = 0; i< pData.length; i++){
                jsonData[i] = pData[i];
            }

      var courierdata = view_admins_table.rows('.selected').data();
      var courierjson = courierdata[0]['id'];
      var courieremail = courierdata[0]['email'];
      var courierphone = courierdata[0]['phone'];

      var url = "{{url('administration/assignordertocourier')}}";

       
var data = {
    '_token':'{{csrf_token()}}',
    'branchId': $('#currentBranch').val(),
    'orders': jsonData,
    'courier':courierjson,
    'courieremail':courieremail,
    'courierphone':courierphone,
    'branch':"{{$order->branch->id}}"
};

loadTable()

$.post(url,data,function (result) {
    console.log(result);
    table.ajax.reload();

    if(result.status === false){
        errorToast(result.message);
        return;
    }

    successToast(result.message);
    $('#assignCourierModal').modal('hide');

//                        table.ajax.reload();
});

    })
})

@if(!$order->haspaiddelivery)
        $('#extraoptions').append(`<fieldset>
                        <div class="custom-control custom-checkbox">
                          <input type="checkbox" class="custom-control-input" name="applydelivery" id="applydelivery">
                          <label class="custom-control-label" for="applydelivery">Add Delivery Fee</label>
                        </div>
                    </fieldset>`);
        @endif
        @if(!$order->hasappliedcoupon)
                    $('#extraoptions').append(`<fieldset>
                        <div class="custom-control custom-checkbox">
                          <input type="checkbox" class="custom-control-input"  name="applycoupon" id="applycoupon">
                          <label class="custom-control-label" for="applycoupon">Apply Coupon</label>
                        </div>
                    </fieldset>`);
      @endif
window.__amounttopay = 0;
$('.ask-customer-to-make-payment-online').on('click',function(e){
        e.preventDefault();

    if(table.rows('.selected').data().length < 1){
        swal("Whoops!", "You forgot to select the products");
        return;
    }

        var pData = table.rows('.selected').data();
        console.log(pData);
        //var amount = 0;
    window.__amounttopay = 0;
            for(var i = 0; i< pData.length; i++){
                window.__amounttopay+= Number(pData[i]['price']) * Number(pData[i]['quantity']);
            }
        $('#paymenttext').text("Amount to Pay: "+window.__amounttopay);
        $('#applydelivery').prop('checked',false);
        $('#applycoupon').prop('checked',false);
        $('#ask-to-pay-modal').modal('show');
});
$('.isAvailable').on('click', function (e) {
    e.preventDefault();
    if(table.rows('.selected').data().length < 1){
        swal("Whoops!", "You forgot to select the products");
        return;
    }

    var jsonData = {};
    var jsonNames = {};
    var pData = table.rows('.selected').data();
    for(var i = 0; i< pData.length; i++){
        jsonData[i] = pData[i]['id'];
        jsonNames[i] = pData[i]['name'];
    }

    var formdata = {
        _token:"{{csrf_token()}}",
        carts:jsonData,
        itemNames: jsonNames,
        branch_id:"{{$order->branch->id}}",
        order_id:"{{$order->id}}",
        phone:"{{$order->user->phone}}",
    };
    var url = $(this).attr('href');

    loadTable()
    $.post(url,formdata,function (result) {
        console.log(result);
        table.ajax.reload();

        if(result.status === false){
            errorToast(result.message);
            return;
        }

        successToast(result.message);


//                        table.ajax.reload();
    });
});

$(document).on('change','#applydelivery',function(){
    if($(this).is(':checked')){
        window.__amounttopay+=Number("{{$order->delivery_fee}}");
        $('#paymenttext').text("Amount to Pay: "+window.__amounttopay);
    }else{
        window.__amounttopay-=Number("{{$order->delivery_fee}}");
        $('#paymenttext').text("Amount to Pay: "+window.__amounttopay);
    }
})
$(document).on('change','#applycoupon',function(){
    if($(this).is(':checked')){
        window.__amounttopay-=Number("{{$order->coupon_value}}");
        $('#paymenttext').text("Amount to Pay: "+window.__amounttopay);
    }else{
        window.__amounttopay+=Number("{{$order->coupon_value}}");
        $('#paymenttext').text("Amount to Pay: "+window.__amounttopay);
    }
})

$('#proceed-to-pay-online').on('click',function(){
    var pData = table.rows('.selected').data();
            console.log(pData);
            var jsonData = {};
            var amount = 0;
            for(var i = 0; i< pData.length; i++){
                amount+=Number(pData[i]['price'])*Number(pData[i]['quantity']);
                jsonData[i] = pData[i]['id'];
            }
            var formdata = {
                _token:"{{csrf_token()}}",
                carts:jsonData,
                branch_id:"{{$order->branch->id}}",
                order_id:"{{$order->id}}",
                user_id:"{{$order->user->id}}",
                amount:amount,
                email:"{{$order->user->email}}",
                phone:"{{$order->user->phone}}",
                applydelivery:false,
                applycoupon:false,
                sendemail:false,
                sendsms:false,
            }

            if($('#applydelivery').is(':checked')){
               // formdata.amount+=Number("{{$order->delivery_fee}}");
                formdata.applydelivery = true;                
            }else{
                formdata.applydelivery = false;
            }

            if($('#applycoupon').is(':checked')){
                //formdata.amount-=Number("{{$order->coupon_value}}");
                formdata.applycoupon = true;
            }else{
                formdata.applycoupon = false;
            }


            if($('#email-channel1').is(':checked')){
                //formdata.amount-=Number("{{$order->coupon_value}}");
                formdata.sendemail = true;
            }else{
                formdata.sendemail = false;
            }


            if($('#sms-channel1').is(':checked')){
                //formdata.amount-=Number("{{$order->coupon_value}}");
                formdata.sendsms = true;
            }else{
                formdata.sendsms = false;
            }

            swal('Sending Payment Link... Please Wait');
            console.log(formdata);
    $.post("{{url('/sendpaymentlink')}}",formdata,function(res){
        console.log(res);
        if(res && res.status==='success'){
            table.ajax.reload()
            $('#ask-to-pay-modal').modal('hide');
            swal('Success','Payment link sent successfully to Customer','success');
        }else{
            swal('Error','Payment Link not sent. Please try again','error');
        }
    }).fail(function(){
        swal('Error','Payment Link not sent. Please try again','error');
    })
            
});
    </script>
@endsection



