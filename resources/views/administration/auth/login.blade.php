@extends('administration.layout.auth')
@section('content')
    <div class="app-content content">
        <div class="content-wrapper">
            <div class="content-header row">
            </div>
            <div class="content-body">
                <section class="flexbox-container">
                    <div class="col-12 d-flex align-items-center justify-content-center">
                        <div class="col-md-4 col-10 box-shadow-2 p-0">
                            @if ($errors->has('email'))
                                <div class="alert alert-danger">
                                    <strong style="color:white">Invalid Login Credentials.. Try Again</strong>
                                </div>
                            @endif
                            <div class="card border-grey border-lighten-3 m-0">
                                <div class="card-header border-0">
                                    <div class="card-title text-center">
                                        <div class="p-1"> <b>Cheezy Pizza Administrator</b></div>
                                    </div>
                                    <h6 class="card-subtitle line-on-side text-muted text-center font-small-3 pt-2"><span>Login</span></h6>
                                </div>
                                <div class="card-content">
                                    <div class="card-body">
                                        <form class="form-horizontal form-simple" method="POST" action="{{ url('/administration/auth/login') }}">
                                            {{ csrf_field() }}
                                            <fieldset class="form-group position-relative has-icon-left mb-0 {{$errors->has('email') ? ' has-error' : '' }}">
                                                <input type="text" name="email" class="form-control form-control-lg input-lg" id="user-name" placeholder="Email" required value="{{ old('email') }}">
                                                <div class="form-control-position">
                                                    <i class="ft-user"></i>
                                                </div>
                                            </fieldset>
                                            <fieldset class="form-group position-relative has-icon-left {{$errors->has('password') ? ' has-error' : '' }}">
                                                <input type="password" name="password" class="form-control form-control-lg input-lg" id="user-password" placeholder="Enter Password" required>
                                                <div class="form-control-position">
                                                    <i class="fa fa-key"></i>
                                                </div>
                                            </fieldset>
                                            <div class="form-group row">
                                                <div class="col-md-6 col-12 text-center text-md-left">
                                                    <fieldset>
                                                        <input type="checkbox" id="remember-me" name="remember" class="chk-remember">
                                                        <label for="remember-me"> Remember Me</label>
                                                    </fieldset>
                                                </div>
                                                {{--  <div class="col-md-6 col-12 text-center text-md-right"><a href="{{ url('/shopadmin/password/reset') }}" class="card-link">Forgot Password?</a></div> --}}
                                            </div>
                                            <button type="submit" class="btn btn-info btn-lg btn-block"><i class="ft-unlock"></i> Login</button>
                                            <br>
                                        <!-- <p align="center"><a href="{{url('/shopadmin/password/reset')}}">Forgot Password ?</a></p> -->
                                        </form>
                                    </div>
                                </div>
                                <div class="card-footer">
                                    <div class="">
                                    <!--<p class="float-sm-right text-center m-0">New to ShopwithVim? <a href="{{url('/shopadmin/register')}}" class="card-link">Sign Up</a></p>-->
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>

            </div>
        </div>
    </div>

@endsection
