@extends('administration.layout.base')
@section('styles-below')
@endsection
@section('content')
    <div class="app-content content">
        <div class="content-wrapper">
            <div class="content-header row">
                <div class="content-header-left col-md-12 col-12 mb-2 breadcrumb-new">
                    <h3 class="content-header-title mb-0 d-inline-block">Starter</h3>
                    <div class="row breadcrumbs-top d-inline-block">
                        <div class="breadcrumb-wrapper col-12">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="{{url('/administration/home')}}">Home</a>
                                </li>
                            </ol>
                        </div>
                    </div>
                </div>
            </div>

            <div class="content-body"><!--Grid options-->
                @include('administration.layout.notifications')
                {{-- Content--}}

                <div class="form-group row">
                    <div class="col-md-4" data-toggle="tooltip" title="Main Image Here! This is first product image customers will see">
                        <div class="file-loading">
                            <input id="file-0d" type="file" class="file" name="main_image" accept=".jpg,.png.jpeg" required>
                        </div>
                    </div>
                    <div class="col-md-8" data-toggle="tooltip" title="Supporting Images! add other sides of the image if any. Maximum is 5">
                        <div class="file-loading">
                            <input id="file-1" type="file" multiple class="file" accept=".jpg,.png.jpeg" name="supporting_images[]" data-overwrite-initial="false" data-max-file-count="5">
                        </div>
                    </div>
                </div>
                
                {{-- Content --}}
            </div>

        </div>

     </div>

@endsection
@section('scripts-below')

@endsection



