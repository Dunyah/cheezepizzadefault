@extends('administration.layout.base')
@section('content')
<div class="app-content content">
      <div class="content-wrapper">
        <div class="content-header row">
          <div class="content-header-left col-md-8 col-12 mb-2 breadcrumb-new">
            <div class="row breadcrumbs-top d-inline-block">
              <div class="breadcrumb-wrapper col-12">
                <ol class="breadcrumb">
                  <li class="breadcrumb-item"><a href="{{url('home')}}">Home</a>
                  </li>
                  <li class="breadcrumb-item active">Edit Branch Manager
                  </li>
                </ol>
              </div>
            </div>
          </div>
         
        </div>
        <div class="content-body">
<section id="configuration">
    <div class="row">
        <div class="col-md-12">
            @include('administration.layout.notifications')
        </div>
        <div class="col-12">
            {{-- starts here --}}
            <section id="basic-form-layouts">
<div class="row">
    <div class="col-xl-3 col-md-3 col-xs-3" >
        <div class="card">
          <div class="card-body">
                <div class="card-block">
                  @if($manager->image=='')
                    <img src="{{asset('admin-assets/app-assets/images/profile.png')}}" class="rounded-circle" alt="Card image" style="display: block;margin: auto;width: 200px;height: 200px" id="output">
                    @else
                    <img src="{{asset('storage/'.$manager->image)}}" class="rounded-circle" alt="Card image" style="display: block;margin: auto;width: 200px;height: 200px" id="output">
                    @endif
                </div>
                <div class="card-block">
                    <h4 class="card-title" style="overflow:hidden;word-wrap:break-word"></h4>
                    <h6 class="card-subtitle text-muted" style="overflow:hidden;word-wrap:break-word"></h6>
                </div>
     
          </div>
        </div>
    </div> 
  <div class="col-md-9">
    <div class="card">
      <div class="card-header">
        <h4 class="card-title" id="basic-layout-form">Edit Manage's Profiler</h4>
        <a class="heading-elements-toggle"><i class="icon-ellipsis font-medium-3"></i></a>
        <div class="heading-elements">
          <ul class="list-inline mb-0">
           
          </ul>
        </div>
      </div>
      <div class="card-body">
        <div class="card-block">
          <form class="form" method="POST" action="{{url('/update-branch-manager')}}" enctype="multipart/form-data">
            {{csrf_field()}}
            <input type="hidden" name="id" value="{{$manager->id}}">
            <div class="form-body">
              <div class="row">
                <div class="col-md-12">
                  <div class="form-group">
                    <label for="firstname">Manager's name <span class="text-danger">*</span></label>
                    <input type="text" id="firstname" class="form-control" placeholder="Full name" name="firstname" required value="{{$manager->name}}">
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-md-6">
                  <div class="form-group">
                    <label for="email">Email <span class="text-danger">*</span></label>
                    <input type="text" id="email" class="form-control" placeholder="Email" name="email" required value="{{$manager->email}}">
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="form-group">
                    <label for="phone">Phone Number <span class="text-danger">*</span></label>
                    <input type="text" id="phone" class="form-control" placeholder="Mobile Number" name="phone" required value="{{$manager->phone}}">
                  </div>
                </div>
              </div>

             
                <div class="row">
                    <div class="col-md-6">
                      <div class="form-group">
                        <label for="projectinput3">Upload Picture</label>
                        <input type="file" id="image" class="form-control" name="image" onchange="loadFile(event)">
                      </div>
                    </div>
                  </div>
            </div>
            <div class="form-actions">
                <button type="submit" class="btn btn-primary">
                    <i class="icon-check2"></i> Update Manager <img src="assets/app-assets/images/loading.gif" style="max-width: 30px;display: none">
                  </button>
              <button type="button" class="btn btn-danger mr-1">
                <i class="icon-cross2"></i> Cancel
              </button>
             
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
</div>
</section>
            {{-- ends here --}}
        </div>
    </div>
</section>
 </div>
   </div>
</div>
@endsection
@section('scripts-below')
<script type="text/javascript">
  
</script>
<script type="text/javascript">
     var loadFile = function(event) {
    var output = document.getElementById('output');
    output.src = URL.createObjectURL(event.target.files[0]);
     };
</script>
@endsection


