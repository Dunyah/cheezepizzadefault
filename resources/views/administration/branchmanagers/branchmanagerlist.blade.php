@extends('administration.layout.base')
@section('content')
<div class="app-content content">
      <div class="content-wrapper">
        <div class="content-header row">
          <div class="content-header-left col-md-8 col-12 mb-2 breadcrumb-new">
            <div class="row breadcrumbs-top d-inline-block">
              <div class="breadcrumb-wrapper col-12">
                <ol class="breadcrumb">
                  <li class="breadcrumb-item"><a href="{{url('home')}}">Home</a>
                  </li>
                  <li class="breadcrumb-item active">Managers List
                  </li>
                </ol>
              </div>
            </div>
          </div>
         
        </div>
        <div class="content-body">
<section id="configuration">
    <div class="row">
        <div class="col-md-12">
            @include('administration.layout.notifications')
        </div>
        <div class="col-12">
            <div class="card">
                <div class="card-header">
                    <h4 class="card-title">Managers List</h4>
                </div>
                <div class="card-body">
                    <div class="card-block card-dashboard">
                        <table class="table table-striped table-bordered tagstable">
                            <thead>
                                <tr>
                                    <th>Image</th>
                                    <th>Name</th>
                                    <th>Email</th>
                                    <th>Phone</th>
                                    <th>
                                        Managed Branches
                                    </th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                            	@foreach($managers as $m)
                                <tr>

                                  {{--   <td><input type="checkbox" name="checkall" class="form-control"></td> --}}
                                    <td>
                                      @if(!is_null($m->image))
                                      <img src="{{asset('storage/'.$m->image)}}" style="width: 50px;height: 50px">
                                      @else
                                       <img src="{{asset('admin-assets/app-assets/images/profile.png')}}" style="width: 50px;height: 50px">
                                      @endif
                                    </td>
                                    <td>{{$m->name}}</td>
                                    <td>{{$m->email}}</td>
                                    <td>{{$m->phone}}</td>
                                    <td>
                                        @foreach ($m->branches as $b)
                                            <span class="badge badge-primary">{{$b->name}}</span>
                                        @endforeach
                                    </td>
                                    <td>
                                    	<a data="{{$m}}" href="{{url('/administration/edit-branch-manager/'.$m->id)}}" class="btn btn-outline-primary btn-sm"><i class="icon-head"></i> Edit</a>
                                      @if(\Illuminate\Support\Facades\Auth::user()->{'id'}!==$m->id)
                                    	<a class="btn btn-outline-danger btn-sm remove" data="{{$m->id}}"><i class="icon-trash4"></i> Delete</a>
                                      @endif
                                    </td>
                                </tr>
                                {{-- recurse here --}}                          
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
 </div>
   </div>
</div>
 
@endsection

@section('scripts-below')
<script type="text/javascript">
 var table = $('.tagstable').DataTable();
	$(document).on('click', '.remove', function (e) {
    var datas = $(this).attr('data');
    e.preventDefault();  
     $tr = $(this).closest('tr');
        swal({
                 title: "Are you Sure?",
            text: "Delete Branch Manager Permanently",
            icon: "info",
            showCancelButton: true,
            buttons: {
                cancel: {
                    text: "No",
                    value: null,
                    visible: true,
                    className: "btn-danger",
                    closeModal: true,
                },
                confirm: {
                    text: "Yes",
                    value: true,
                    visible: true,
                    className: "btn-success",
                    closeModal: false
                }
            }
            }).then(function(isConfirm){
                if (isConfirm){
                    var data = table.row($tr).data();
                 $.get("{{url('/deletebranchmanager')}}",{id:datas},function(response){
                 	console.log(response);
                 if(response.status=='success'){
                     console.log(table.row($tr).remove().draw()); 
                     swal("Success!","Deleted Successfully","success");
                  }
                  else{
                      swal("Error!","Error Deleting Manager.Please Try Again","error");
                  }
    }).fail(function(){
            swal("Error!","Error Deleting Tag.Check Network Connection","error");
    });
      }

  });
});


</script>
@stop

