@extends('administration.layout.base')
@section('styles-below')
<link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-fileinput/4.4.9/css/fileinput.min.css" media="all" rel="stylesheet" type="text/css" />
<link href="https://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.9/summernote.css" rel="stylesheet">
@endsection
@section('content')
    <div class="app-content content">
        <div class="content-wrapper">
            <div class="content-header row">
                <div class="content-header-left col-md-12 col-12 mb-2 breadcrumb-new">
                    <h3 class="content-header-title mb-0 d-inline-block">Edit Product</h3>
                    <div class="row breadcrumbs-top d-inline-block">
                        <div class="breadcrumb-wrapper col-12">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="{{url('/dashboard')}}">Home</a>
                                </li>
                            </ol>
                        </div>
                    </div>
                </div>


            </div>

            <div class="content-body"><!--Grid options-->
                @include('administration.layout.notifications')
                {{-- body is here --}}

                <div class="row">
                    <div class="col-md-12">

                        <div class="card">
                            <div class="card-header">
                                <h4 class="card-title" id="striped-row-layout-basic">Fill the form below to add prodcut</h4>
                                <a class="heading-elements-toggle"><i class="fa fa-ellipsis-v font-medium-3"></i></a>
                                <div class="heading-elements">
                                    <ul class="list-inline mb-0">
                                        <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                                        <li><a data-action="reload"><i class="ft-rotate-cw"></i></a></li>
                                        <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                                        <li><a data-action="close"><i class="ft-x"></i></a></li>
                                    </ul>
                                </div>
                            </div>
                            <div class="card-content collpase show">
                                <div class="card-body">

                                    <div class="card-text">
                                        <p> Carefully add your product. <code> kindly read the tips for clarification if any of the fields are not clear </code></p>
                                    </div>

                                    <form action="{{url('administration/update-product')}}" method="post" class="form form-horizontal striped-rows " enctype="multipart/form-data">
                                        {{csrf_field()}}
                                        <div class="form-body">
                                            <h4 class="form-section text-danger"><i class="fa fa-list"></i> Basic Required Info</h4>

                                            <div class="form-group row">
                                                <label class="col-md-3 label-control" for="selProductCat">Product Category</label>
                                                <div class="col-md-9">
                                                    <select id="selProductCat" name="category" class="form-control" data-toggle="tooltip" title="How best will you classify your product ? This will aid customers to locate this product faster" required>
                                                        <option value="" >Select Category</option>
                                                        @foreach(\App\Category::all() as $c)
                                                            <option data="{{$c->name}}" value="{{$c->id}}" {{$c->id == $product->category->id ? 'selected' : ''}} >{{$c->name}}</option>
                                                        @endforeach

                                                    </select>
                                                </div>
                                            </div>


                                            <div class="form-group row">
                                                <label class="col-md-3 label-control" for="productName">Product Name</label>
                                                <div class="col-md-9">
                                                    <input type="text" id="productName" class="form-control" value="{{$product->name}}"  data-toggle="tooltip" title="Any name to identify your product" placeholder="Product Name" name="name" required>
                                                </div>

                                            </div>

                                            <div class="form-group row" id="price_div">
                                                <label class="col-md-3 label-control" for="price">Product Price</label>
                                                <div class="col-md-9">
                                                    <input type="text" id="price" value="{{ $product->price }}" class="form-control" data-toggle="tooltip" title="How much do you want customers to buy product ?" placeholder="Product Price" name="price" required>
                                                </div>

                                            </div>

                                            <div class="form-group row none" id="size_div">
                                                <label class="col-md-3 label-control" for="productName">Configure Prices</label>
                                                <div class="col-md-9">
                                                    <table class="table table-bordered">
                                                        <tr>
                                                            <th>Size</th>
                                                            <th>Price</th>
                                                        </tr>
                                                        @foreach(\App\Size::all() as $s)
                                                            <tr>
                                                                <td>{{ $s->name }}</td>
                                                                <td>
                                                                    <input type="hidden" name="additional_size_id[]" value="{{$s->id}}">
                                                                    <input type="text" value="{{old('price') ?: '0.00' }}" class="form-control" data-toggle="tooltip" title="How much do you want customers to buy product ?" placeholder="Product Price" name="additional_price[]">
                                                                </td>
                                                            </tr>
                                                        @endforeach
                                                    </table>
                                                </div>

                                            </div>





                                            <h4 class="form-section text-primary"><i class="fa fa-image"></i>Add Image of the menu <small></small></h4>

                                            <div class="form-group row">
                                                <div class="col-md-12" data-toggle="tooltip" title="Main Image Here! This is first product image customers will see">
                                                    <div class="file-loading">
                                                        <input id="file-0d" type="file" class="file" name="main_image" required>
                                                    </div>
                                                </div>
                                            </div>

                                            <h4 class="form-section text-info"><i class="fa fa-clipboard"></i>Basic Optional Info</h4>



                                            <div class="form-group row last">
                                                <label class="col-md-3 label-control" for="summernote">Description & Specification</label>
                                                <div class="col-md-9">

                                                    <textarea id="summernote" name="description" data-toggle="tooltip" title="Give a brief description about the product. " rows="15">{!! $product->description !!}</textarea>

                                                </div>
                                            </div>


                                        </div>

                                        <div class="form-actions">
                                            <a href="{{url('administration/add-product')}}" id="reset-ap" class="btn btn-warning mr-1 confirm">
                                                <i class="ft-x"></i> Reset
                                            </a>
                                            <button type="submit" id="addProduct" class="btn btn-primary">
                                                <i class="fa fa-check-square-o"></i> Save
                                            </button>
                                        </div>

                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                {{-- body is here --}}
            </div>

        </div>

    </div>

@endsection
@section('scripts-below')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-fileinput/4.4.9/js/fileinput.min.js"></script>
    
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-fileinput/4.4.9/themes/fa/theme.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.9/summernote.js"></script>
    <script>

        $(function () {
            // $('.alert').hide(500);
            $('.alert-success').fadeIn().delay(2000).slideUp(1000);
        });



        $('#addProduct').click(function (e) {
            //    validations ---------------------
            var price = $('#price').val();
            // check if price entered is valid ----------------

            if(!$.isNumeric(price) || price < 1){
                errorToast("Product price is invalid");
                e.preventDefault();
                return;
            }

//    check if a file has been uploaded provided he selected virtual as product
            if ($('#selProductType').val() === 'virtual'){
                var virtualItem = $('#virtual_item').val();
                if(virtualItem === '' && fileUploaded === ''){
                    errorToast("Please select a virtual item")
                    e.preventDefault();
                    return;
                }

                if (fileUploaded === '') {
                    errorToast("Upload your virtual product before submitting form");
                    e.preventDefault();
                    return
                }
            }

            if($('#selProductCat').val() === '0'){
                var input_category = $('#input_category').val();
                if(input_category === ''){
                    errorToast("Please enter your category");
                    e.preventDefault();
                    return;
                }
            }


        });


        $(document).ready(function() {
            $('#summernote').summernote({
                tabsize: 2,
                height: 100,
                toolbar: [
                    // [groupName, [list of button]]
                    ['style', ['bold', 'italic', 'underline', 'clear']],
                    ['font', ['strikethrough', 'superscript', 'subscript']],
                    ['fontsize', ['fontsize']],
                    ['color', ['color']],
                    ['para', ['ul', 'ol', 'paragraph']],
                    ['height', ['height']],
                    ['misc', ['undo','redo','help']],
                    ['insert', ['table','hr']]
                ]
            });
        });  


        console.log("{{$main_image}}")
        {{-- main image contoller--}}
        var mainImage = $("#file-0d");
        mainImage.fileinput({
            theme: 'fa',
            showUpload:false,
            showCancel:false,
            showRemove:false,
            uploadTitle:"Main Images Here",
            initialPreviewShowDelete:false,
                'allowedFileExtensions': ['jpg', 'png', 'gif', 'jpeg'],
            initialPreviewAsData: true,
            initialPreview: [
                "{{$main_image}}"
            ],
            initialPreviewConfig: [
                {
                    showRemove: false
                }
            ]

        });

//       end of edit product features ------------------------------------

    </script>
@endsection



