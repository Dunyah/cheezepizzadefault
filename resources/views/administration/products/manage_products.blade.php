@extends('administration.layout.base')
@section('styles-below')
    <link rel="stylesheet" type="text/css" href="{{asset('admin-assets/app-assets/vendors/css/tables/datatable/select.dataTables.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('admin-assets/app-assets/vendors/css/tables/extensions/buttons.dataTables.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('admin-assets/app-assets/vendors/css/tables/datatable/buttons.bootstrap4.min.css')}}">
@endsection
@section('content')
    <div class="app-content content">
        <div class="content-wrapper">
            <div class="content-header row">
                <div class="content-header-left col-md-12 col-12 mb-2 breadcrumb-new">
                    <h3 class="content-header-title mb-0 d-inline-block">Manage all products</h3>
                    <div class="row breadcrumbs-top d-inline-block">
                        <div class="breadcrumb-wrapper col-12">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a class="btn btn-danger btn-lg" href="{{url('administration/add-product')}}"><i class="fa fa-plus-square"></i> Add new product</a>
                                </li>
                            </ol>
                        </div>
                    </div>

                    <h3 class="content-header-title mb-0 d-inline-block pull-right text-primary">{{\App\Product::count()}} uploaded products </h3>

                </div>

            </div>

            <div class="content-body"><!--Grid options-->
                {{-- Figure Statistics --}}
                @include('administration.layout.notifications')
                        <div class="row">
                            <div class="col-12">
                                {{-- Data table starts here --}}
                                <div class="card">
                                    <div class="card-header">
                                        <h4 class="card-title">

                                            <div class="btn-group">
                                                
                                                <button class="btn btn-info dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="icon-settings mr-1"></i> Select products and take further actions </button>
                                                <div class="dropdown-menu arrow">
                                                    <a class="dropdown-item toggle-products" href="{{url('administration/change-product-visibility/1')}}"><i class="fa fa-eye mr-1"></i> Show selected products</a>
                                                    <div class="dropdown-divider">
                                                    </div><a class="dropdown-item toggle-products" href="{{url('administration/change-product-visibility/0')}}"><i class="fa fa-eye-slash mr-1"></i> Hide selected products</a>
                                                    <div class="dropdown-divider"></div>
                                                    <a class="dropdown-item text-danger" href="#" id="delete-products"><i class="fa fa-trash mr-1"></i> Delete Selected</a>
                                                    <div class="dropdown-divider"></div>
                                                    {{--<a class="dropdown-item text-info" href="#" id="assign-new-branch"><i class="fa fa-code-fork mr-1"></i> Assign selected to a new branch</a>--}}
                                                    {{--<div class="dropdown-divider"></div>--}}

                                                </div>
                                
                                            </div>
                         
                                        </h4>
                                      
                                        
                                        <a class="heading-elements-toggle"><i class="fa fa-ellipsis-v font-medium-3"></i></a>
                                    
                                        <div class="heading-elements">
                                            <ul class="list-inline mb-0">
                                                <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                                                <li><a class="overlay-unblock"><i class="ft-rotate-cw"></i></a></li>
                                                <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                                            </ul>
                                        </div>

                                    </div>
                                    <div class="card-content collapse show">
                                        <div class="card-body card-dashboard">
                                            {{--<p class="card-text">DataTables has most features enabled by default, so all you need to do to use it with your own ables is to call the construction function: $().DataTable();.</p>--}}
                                            <div class="table-responsive">
                                            <table class="table table-striped table-bordered product-list dataex-select-checkbox">
                                                <thead>
                                                <tr>
                                                    <th></th>
                                                    <th>Item</th>
                                                    <th>Name</th>
                                                    <th>Price</th>
                                                    <th>Product Type</th>
                                                    <th>Visibility</th>
                                                    <th>Action</th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                </tbody>
                                                <tfoot>
                                                <tr>
                                                    <th></th>
                                                    <th>Item</th>
                                                    <th>Name</th>
                                                    <th>Price</th>
                                                    <th>Type</th>
                                                    <th>Visibility</th>
                                                    <th>Action</th>
                                                </tr>
                                                </tfoot>
                                            </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                            {{-- Data table ends here --}}
                            </div>

                        </div>
                {{-- Figure Statistics --}}
            </div>

        </div>

    </div>

@endsection
@section('scripts-below')
    <script src="{{asset('admin-assets/app-assets/vendors/js/tables/datatable/dataTables.responsive.min.js')}}" type="text/javascript"></script>
    <script src="{{asset('admin-assets/app-assets/vendors/js/tables/datatable/dataTables.select.min.js')}}" type="text/javascript"></script>
    <script src="{{asset('admin-assets/app-assets/vendors/js/tables/datatable/dataTables.buttons.min.js')}}" type="text/javascript"></script>
    <script src="{{asset('admin-assets/app-assets/vendors/js/tables/datatable/buttons.bootstrap4.min.js')}}" type="text/javascript"></script>

    <script>
        // data tables here -------------------

        var block_ele = $('.overlay-unblock').closest('.card');
        $('.product-details').on('click', function () {
            alert();
        });

        $(function () {
            var table = $('.product-list').DataTable({
                    "ajax":{
                        "url" : "{{url('administration/fetch/products')}}",
                        "data" : function (data) {
                            if(data !== 'undefined'){
                                console.log(data);
                                if(block_ele !== 'undefined'){
                                    $(block_ele).unblock();
                                }

                                infoToast("Products table updated");
                                return JSON.stringify(data);
                            }
                        }
                    },
                    "columns": [
                        { "data": "checkbox" },
                        { "data": "image" },
                        { "data": "name" },
                        { "data": "price" },
                        { "data" : "type" },
                        { "data" : "visibility" },
                        { "data" : "details" }

                    ],columnDefs: [ {
                        orderable: false,
                        className: 'select-checkbox',
                        targets:   0
                    } ],
                    select: {
                        style:    'multi',
                        selector: 'td:first-child'
                    },
                    order: [[ 1, 'asc' ]],
                    dom: 'Bfrtip',
                    buttons: [
                        'pageLength',
                        {
                            text: 'Select all',
                            action: function () {
                                table.rows().select();
                            }
                        },
                        {
                            text: 'Select none',
                            action: function () {
                                table.rows().deselect();
                            }
                        }
                    ],
                    className: 'my-1',
                    lengthMenu: [
                        [ 10, 25, 50, -1 ],
                        [ '10 rows', '25 rows', '50 rows', 'Show all' ]
                    ]
                });

            $('#delete-products').on('click',function (e) {
               // console.log(table.rows('.selected').data());
                if(table.rows('.selected').data().length < 1){
                    swal("Whoops!", "You forgot to select the products");
                    return;
                }
                swal({
                    title: "Are you sure?",
                    text: "Permanently delete selected products",
                    icon: "warning",
                    buttons: {
                        cancel: {
                            text: "No, cancel!",
                            value: null,
                            visible: true,
                            className: "",
                            closeModal: true
                        },
                        confirm: {
                            text: "Yes, Continue!",
                            value: true,
                            visible: true,
                            className: "",
                            closeModal: true
                        }
                    }
                }).then(function (isConfirm) {
                    if (isConfirm) {

                        loadTable();

                        var pData = table.rows('.selected').data();
                        //console.log(pData);

                        var jsonData = {};

                        for(var i = 0; i< pData.length; i++){
                            jsonData[i] = pData[i];
                        }

                        var postData = {
                            '_token':'{{csrf_token()}}',
                            'productsIds' : jsonData
                        };

                        console.log("postData loading ..");
                        console.log(postData);

                        $.post("{{url('administration/delete/products')}}", postData ,function (result) {
                            table.ajax.reload();
                            console.log("result is here ...");
                            successToast("Products removed");
                        } );

                    } else {
                        swal("Cancelled", "action aborted :)", "error");
                    }
                });
            });





            // Unblock on overlay click ---------------
            $('.overlay-unblock').on('click', function() {
                loadTable();
                table.ajax.reload();
            });

            function loadTable() {
                $(block_ele).block({
                    message: '<div class="ft-refresh-cw icon-spin font-medium-2"></div>',
                    overlayCSS: {
                        backgroundColor: '#fff',
                        opacity: 0.8,
                        cursor: 'wait'
                    },
                    css: {
                        border: 0,
                        padding: 0,
                        backgroundColor: 'transparent'
                    }
                });
            }

            $('.my-branch').on('click', function () {
                if(table.rows('.selected').data().length < 1){
                    swal("Whoops!", "You forgot to select the products");
                    return;
                }

                var branchSelected = $(this).attr('text');
                var text = $(this).is("[text]") ? 'Assign selected products to ' + branchSelected : '';
                var branch_id = $(this).attr('id');
                var id = branch_id.split('-')[1];
                var pData = table.rows('.selected').data();
                //console.log(id);

                var jsonData = {};

                for(var i = 0; i< pData.length; i++){
                    jsonData[i] = pData[i];
                }

                var url = "{{url('administration/assignproductstobranch')}}";
                var data = {
                    '_token':'{{csrf_token()}}',
                    'branchid':id,
                    'branchName' : branchSelected,
                    'products': jsonData
                };

                swal({
                    title: "Are you sure?",
                    text: text,
                    icon: "warning",
                    buttons: {
                        cancel: {
                            text: "No, cancel!",
                            value: null,
                            visible: true,
                            className: "",
                            closeModal: true
                        },
                        confirm: {
                            text: "Yes, Continue!",
                            value: true,
                            visible: true,
                            className: "",
                            closeModal: true
                        }
                    }
                }).then(function (isConfirm) {
                    if (isConfirm) {
                        //window.location.href = url.toString();
                        loadTable();
                       $.post(url,data,function (result) {
                           console.log(result);
                          table.ajax.reload();
                           if(result.status === false){
                               errorToast(result.message);
                               return;
                           }

                           successToast(result.message);
                       });

                    } else {
                        swal("Cancelled", "action aborted :)", "error");
                    }
                });
            });



        $('.toggle-products').click(function (e) {
            e.preventDefault()
            let url = $(this).attr('href');

            if(table.rows('.selected').data().length < 1){
                swal("Whoops!", "You forgot to select the products");
                return;
            }

            var pData = table.rows('.selected').data();
            // console.log(pData);

            var jsonData = {};

            for(var i = 0; i< pData.length; i++){
                jsonData[i] = pData[i]['id'];
            }

            var data = {
                '_token':'{{csrf_token()}}',
                'data' : jsonData
            };

            loadTable();
            $.post(url,data, function(result){
                table.ajax.reload()
                console.log(result);
                if(result.status){
                    successToast(result.message);
                }

            });

        })


        });





    </script>
@endsection



