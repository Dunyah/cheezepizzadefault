@extends('administration.layout.base')
@section('styles-below')

@endsection
@section('content')
    <div class="app-content content">
        <div class="content-wrapper">
            <div class="content-header row">
                <div class="content-header-left col-md-12 col-12 mb-2 breadcrumb-new">
                    <h3 class="content-header-title mb-0 d-inline-block">Home</h3>
                    <div class="row breadcrumbs-top d-inline-block">
                        <div class="breadcrumb-wrapper col-12">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="{{url('/administration/home')}}">Home</a>
                                </li>
                            </ol>
                        </div>
                    </div>
                </div>
            </div>

            <div class="content-body"><!--Grid options-->
                @include('administration.layout.notifications')
                {{-- Content--}}

                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-content">
                                <div class="card-body">
                                    <div class="row">
                                        <div class="col-lg-3 col-sm-12 border-right-blue-grey border-right-lighten-5">
                                            <div class="pb-1">
                                                <div class="clearfix mb-1">
                                                    <i class="icon-folder font-large-1 blue-grey float-left mt-1"></i>
                                                    <span class="font-large-2 text-bold-300 info float-right pending"></span>
                                                </div>
                                                <div class="clearfix">
                                                    {{-- orders pending out of the orders received today --}}
                                                    <span class="text-muted">Pending Orders</span>
                                                    <a href="{{url('administration/orders')}}" class="btn btn-info btn-sm float-right"> manage </a>
                                                </div>
                                            </div>
                                            {{-- <div class="progress mb-0" style="height: 7px;">
                                                <div class="progress-bar bg-info" role="progressbar" style="width: 80%" aria-valuenow="80" aria-valuemin="0" aria-valuemax="100"></div>
                                            </div> --}}
                                        </div>
                                        <div class="col-lg-3 col-sm-12 border-right-blue-grey border-right-lighten-5">
                                            <div class="pb-1">
                                                <div class="clearfix mb-1">
                                                    <i class="icon-envelope font-large-1 blue-grey float-left mt-1"></i>
                                                    <span class="font-large-2 text-bold-300 danger float-right sms-left"></span>
                                                </div>
                                                <div class="clearfix">
                                                    {{-- sms left out of current sms purchased --}}
                                                    <span class="text-muted">SMS remaining</span>
                                                    <a href="#" class="btn btn-danger btn-sm float-right top-up-sms"> top up </a>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-3 col-sm-12 border-right-blue-grey border-right-lighten-5">
                                            <div class="pb-1">
                                                <div class="clearfix mb-1">
                                                    <i class="icon-user font-large-1 blue-grey float-left mt-1"></i>
                                                    <span class="font-large-2 text-bold-300 success float-right customers"></span>
                                                </div>
                                                <div class="clearfix">
                                                    <span class="text-muted">Total customers</span>
                                                    <a href="{{url('administration/managecustomers')}}" class="btn btn-success btn-sm float-right"> details </a>
                                                </div>
                                            </div>
                                            {{-- <div class="progress mb-0" style="height: 7px;">
                                                <div class="progress-bar bg-success" role="progressbar" style="width: 75%" aria-valuenow="75" aria-valuemin="0" aria-valuemax="100"></div>
                                            </div> --}}
                                        </div>
                                        <div class="col-lg-3 col-sm-12">
                                            <div class="pb-1">
                                                <div class="clearfix mb-1">
                                                    <i class="icon-list font-large-1 blue-grey float-left mt-1"></i>
                                                    <span class="font-large-2 text-bold-300 warning float-right products"></span>
                                                </div>
                                                <div class="clearfix">
                                                    <span class="text-muted">{{\Illuminate\Support\Facades\Auth::user()->{'selectedBranch'}->name }}'s products</span>
                                                    <a href="{{url('administration/addproduct')}}" class="btn btn-warning btn-sm float-right"> add new </a>
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12">
                        {{-- start here --}}
                        <section id="minimal-statistics">
                            <div class="row">
                                <div class="col-12 mb-1">
                                    <h4 class="text-uppercase">Quick Links</h4>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6 col-12">
                                    <a href="{{url('/administration/manage-products')}}"> <div class="card">
                                            <div class="card-content">
                                                <div class="card-body">
                                                    <div class="media d-flex">
                                                        <div class="align-self-center">
                                                            <i class="icon-list info font-large-2 float-left"></i>
                                                        </div>
                                                        <div class="media-body text-right">
                                                            <h3>PRODUCTS</h3>

                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div></a>
                                </div>
                                {{--<div class="col-xl-3 col-lg-6 col-12">--}}
                                {{--<a href="{{url('/administration/branches')}}"><div class="card">--}}
                                {{--<div class="card-content">--}}
                                {{--<div class="card-body">--}}
                                {{--<div class="media d-flex">--}}
                                {{--<div class="align-self-center">--}}
                                {{--<i class="icon-layers warning font-large-2 float-left"></i>--}}
                                {{--</div>--}}
                                {{--<div class="media-body text-right">--}}
                                {{--<h3>BRANCHES</h3>--}}
                                {{----}}
                                {{--</div>--}}
                                {{--</div>--}}
                                {{--</div>--}}
                                {{--</div>--}}
                                {{--</div></a>--}}
                                {{--</div>--}}
                                <div class="col-md-6">
                                    <a href="{{url('administration/order-history')}}"><div class="card">
                                            <div class="card-content">
                                                <div class="card-body">
                                                    <div class="media d-flex">
                                                        <div class="align-self-center">
                                                            <i class="icon-graph success font-large-2 float-left"></i>
                                                        </div>
                                                        <div class="media-body text-right">
                                                            <h3>REPORTS</h3>

                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div></a>
                                </div>
                                {{--<div class="col-xl-3 col-lg-6 col-12">--}}
                                {{--<a href="{{url('administration/payment')}}"><div class="card">--}}
                                {{--<div class="card-content">--}}
                                {{--<div class="card-body">--}}
                                {{--<div class="media d-flex">--}}
                                {{--<div class="align-self-center">--}}
                                {{--<i class="icon-wallet danger font-large-2 float-left"></i>--}}
                                {{--</div>--}}
                                {{--<div class="media-body text-right">--}}
                                {{--<h3>Account renewal</h3>--}}
                                {{----}}
                                {{--</div>--}}
                                {{--</div>--}}
                                {{--</div>--}}
                                {{--</div>--}}
                                {{--</div></a>--}}
                                {{--</div>--}}
                            </div>

                            {{-- <div class="row">
                                <div class="col-xl-3 col-lg-6 col-12">
                                    <div class="card">
                                        <div class="card-content">
                                            <div class="card-body">
                                                <div class="media d-flex">
                                                    <div class="media-body text-left">
                                                        <h3 class="danger">278</h3>
                                                        <span>New Projects</span>
                                                    </div>
                                                    <div class="align-self-center">
                                                        <i class="icon-rocket danger font-large-2 float-right"></i>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-xl-3 col-lg-6 col-12">
                                    <div class="card">
                                        <div class="card-content">
                                            <div class="card-body">
                                                <div class="media d-flex">
                                                    <div class="media-body text-left">
                                                        <h3 class="success">156</h3>
                                                        <span>New Clients</span>
                                                    </div>
                                                    <div class="align-self-center">
                                                        <i class="icon-user success font-large-2 float-right"></i>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-xl-3 col-lg-6 col-12">
                                    <div class="card">
                                        <div class="card-content">
                                            <div class="card-body">
                                                <div class="media d-flex">
                                                    <div class="media-body text-left">
                                                        <h3 class="warning">64.89 %</h3>
                                                        <span>Conversion Rate</span>
                                                    </div>
                                                    <div class="align-self-center">
                                                        <i class="icon-pie-chart warning font-large-2 float-right"></i>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-xl-3 col-lg-6 col-12">
                                    <div class="card">
                                        <div class="card-content">
                                            <div class="card-body">
                                                <div class="media d-flex">
                                                    <div class="media-body text-left">
                                                        <h3 class="info">423</h3>
                                                        <span>Support Tickets</span>
                                                    </div>
                                                    <div class="align-self-center">
                                                        <i class="icon-support info font-large-2 float-right"></i>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div> --}}


                        </section>
                        {{-- ends here --}}
                    </div>
                    <div class="col-12">
                        <div class="card">
                            <div class="card-header">
                                <h4 class="card-title">Orders Within last 7 Days</h4>
                            </div>
                            <div class="card-body">
                                <div class="card-block card-dashboard">
                                    {{-- add category  --}}
                                    <div class="height-500">
                                        <canvas id="line-chart"></canvas>
                                    </div>
                                    {{-- end --}}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                {{-- Content --}}
            </div>

        </div>

    </div>

@endsection
@section('scripts-below')
    <script>
        $.get("{{url('/administration/fetch-summary')}}",function(res){
            if(res){
                $('.pending').text(res.pending);
                $('.customers').text(res.customers);
                $('.products').text(res.totalproducts);
            }
        });

        $.get("{{url('/administration/fetch-sms-balance')}}", function (res) {
            $('.sms-left').text(res.data.balance);
        }).fail(function(err){
            console.log(err);
        });

    </script>

    <script src="{{asset('admin-assets/app-assets/vendors/js/charts/chart.min.js')}}"></script>
    <script type="text/javascript">
        $(window).on("load",function(){
            $.get("{{url('/administration/fetchordersstats')}}",function(res){
                console.log(res);
                var o=$("#line-chart");
                new Chart(o,{type:"bar",options:{responsive:!0,maintainAspectRatio:!1,legend:{position:"bottom"},hover:{mode:"label"},
                        scales:{xAxes:[{display:!0,gridLines:{color:"#f3f3f3",drawTicks:!1},scaleLabel:{display:!0,
                                    labelString:"Days"}}],
                            yAxes:[{display:!0,gridLines:{color:"#f3f3f3",drawTicks:!1},scaleLabel:{display:!0,labelString:"Orders"}}]},title:{display:!0,text:"Weekly Orders Stats"}},

                    data:{labels:res.datearr,

                        datasets:[
                            {label:"",data:res.orders,fill:!1,borderColor:"#00A5A8",pointBorderColor:"#00A5A8",pointBackgroundColor:"#FFF",backgroundColor:"#28D094",pointBorderWidth:2,pointHoverBorderWidth:2,pointRadius:4},

                        ]}})

            })
        });
    </script>

@endsection



