<!DOCTYPE html>
<html>

<head>
    @include('layouts.head')
    <link href="css/skins/square/grey.css" rel="stylesheet">
</head>

<body>

    <!-- Header ================================================== -->
    @include('layouts.header')
    <!-- End Header =============================================== -->

    <!-- SubHeader =============================================== -->
    <div id="app">
        <section class="parallax-window" data-parallax="scroll" data-image-src="img/sub_header_2.jpg"
            data-natural-width="1400" data-natural-height="470">
            <div id="subheader" style="background: url(img/sub_header_cart.jpg);">
                <div class="overlay"></div>
                <div id="sub_content" class="animated zoomIn">
                    <div id="thumb"><img src="img/thumb_restaurant.jpg" alt=""></div>
                    <div class="rating"><i class="icon_star voted"></i><i class="icon_star voted"></i><i
                            class="icon_star voted"></i><i class="icon_star voted"></i><i class="icon_star"></i>
                        (<small><a href="#">Read 98 reviews</a></small>)</div>
                    <h1>[[ branchName ]]</h1>
                    <div><em>[[ branchId ]]</em></div>
                    <div><i class="icon_pin"></i>[[location]]</div>
                </div><!-- End sub_content -->
            </div><!-- End subheader -->
        </section>

        <!-- End SubHeader ============================================ -->

        <div id="position">
            <div class="container">
                <ul>
                    <li><a href="#0">Home</a></li>
                    <li><a href="#0">Menus</a></li>
                    <li>Page active</li>
                </ul>
                <!-- <a href="#0" class="search-overlay-menu-btn"><i class="icon-search-6"></i> Search</a> -->
            </div>
        </div><!-- Position -->

        <!-- Content ================================================== -->
        <div class="container margin_60_35">
            <div class="row">
                <div class="col-md-3">
                    <!-- <p><a href="list_page.html" class="btn_side">Back to search</a></p> -->
                    <div class="box_style_1">
                        <ul id="cat_nav" v-for="cat in categories" :key="cat.id">
                            <li><a :href="'#'+cat.id" class="active">[[cat.name]] <span>( [[cat.products.length
                                        ]])</span></a></li>
                        </ul>
                    </div><!-- End box_style_1 -->

                    <div class="box_style_2 hidden-xs" id="help">
                        <i class="icon_lifesaver"></i>
                        <h4>Need <span>Help?</span></h4>
                        <a href="#" class="phone">0544133397</a>
                        <small>[[open_time]]</small>
                    </div>
                </div><!-- End col-md-3 -->

                <div class="col-md-6">
                    <div class="box_style_2" id="main_menu">
                        <h2 class="inner">Menu</h2>
                        <div v-for="cat in categories" :key="cat.id">
                            <h3 class="nomargin_top" :id="cat.id">[[cat.name]]</h3>
                            <p>
                                Te ferri iisque aliquando pro, posse nonumes efficiantur in cum. Sensibus reprimique eu
                                pro. Fuisset mentitum deleniti sit ea.
                            </p>
                            <table class="table table-striped cart-list">
                                <thead>
                                    <tr>
                                        <th>
                                            Item
                                        </th>
                                        <th>
                                            Price
                                        </th>
                                        <th>
                                            Order
                                        </th>
                                    </tr>
                                </thead>
                                <tbody>

                                    <tr v-for="product in cat.products" :key="product.id">
                                        <td>
                                            <figure class="thumb_menu_list"><img src="img/menu-thumb-1.jpg" alt="thumb">
                                            </figure>
                                            <h5> [[ product.name ]]</h5>
                                            <p>
                                                [[product.description]]
                                            </p>
                                        </td>
                                        <td>
                                            <strong>₵[[product.price]]</strong>
                                        </td>
                                        <td class="options">
                                            <div class="dropdown dropdown-options dropup">
                                                <a v-if="product.sizes" href="#" class="dropdown-toggle"
                                                    data-toggle="dropdown"
                                                    v-on:click="GetQuantityOrSize(product.sizes.length, product)"
                                                    aria-expanded="true"><i class="icon_plus_alt2"></i></a>
                                                <div class="dropdown-menu" style="" v-if="product.sizes.length > 0">
                                                    <h5>Select an option</h5>
                                                    <div v-for="size in product.sizes" :key="size.id">
                                                        <label>
                                                            <input @change="prepAdditionalData($event, size, product)"
                                                                type="radio" value="option1" name="options_1"
                                                                checked="">[[size.name]] <span> ₵ [[product.price +
                                                                size.pivot.additional_price ]] </span>
                                                        </label>
                                                    </div>
                                                    <a href="#0" class="add_to_basket"
                                                        v-on:click="GetProductQuantity(product)">Add to cart</a>
                                                </div>
                                            </div>
                                        </td>
                                    </tr>


                                </tbody>
                            </table>
                            <hr>
                        </div>
                    </div><!-- End box_style_1 -->
                </div><!-- End col-md-6 -->
                <div class="col-md-3" id="sidebar">
                    <div class="theiaStickySidebar">
                        <div id="cart_box">
                            <h3>Your order <i class="icon_cart_alt pull-right"></i></h3>
                            <table class="table table_summary">
                                <tbody>
                                    <tr v-for="order of orders" :key="order.orderId">
                                        <td>
                                            <a href="#0" class="remove_item"
                                                v-on:click="removeOrderItem(order.orderId)"><i
                                                    class="icon_minus_alt"></i></a> <strong>[[ order.quantity
                                                ]]x</strong> [[ getOrderName(order) ]]
                                        </td>
                                        <td>
                                            <strong class="pull-right">₵[[ getOrderPrice(order) ]]</strong>
                                        </td>
                                    </tr>
                                    <span v-if="!orders.length" style="width: 100%;" class="text-center">You have no
                                        orders</span>
                                </tbody>
                            </table>
                            <hr>
                            <div class="row" id="options_2">
                                <div class="col-lg-6 col-md-12 col-sm-12 col-xs-6">
                                    <label><input type="radio" value="" checked name="option_2"
                                            class="icheck">Delivery</label>
                                </div>
                                <div class="col-lg-6 col-md-12 col-sm-12 col-xs-6">
                                    <label><input type="radio" value="" name="option_2" class="icheck">Take Away</label>
                                </div>
                            </div><!-- Edn options 2 -->
                            <hr>
                            <table class="table table_summary">
                                <tbody>
                                    <tr>
                                        <td>
                                            Subtotal <span class="pull-right" v-if="this.orders.length">₵[[ getSubTotal
                                                ]]</span> <span class="pull-right" v-else>----</span>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Delivery fee <span class="pull-right">₵0</span>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="total">
                                            TOTAL <span class="pull-right" v-if="this.orders.length">₵[[ getTotal
                                                ]]</span> <span class="pull-right" v-else>----</span>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                            <hr>
                            <a class="btn_full" href="/carts">Order now</a>
                        </div><!-- End cart_box -->
                    </div><!-- End theiaStickySidebar -->
                </div><!-- End col-md-3 -->

            </div><!-- End row -->
        </div><!-- End container -->
        <!-- End Content =============================================== -->
    </div>

    <!-- Footer ================================================== -->
    @include('layouts.footer')
    <!-- End Footer =============================================== -->

    <div class="layer"></div><!-- Mobile menu overlay mask -->

    <!-- Login modal -->

    @include('layouts.modal')
    <!-- Search Menu -->
    <div class="search-overlay-menu">
        <span class="search-overlay-close"><i class="icon_close"></i></span>
        <form role="search" id="searchform" method="get">
            <input value="" name="q" type="search" placeholder="Search..." />
            <button type="submit"><i class="icon-search-6"></i>
            </button>
        </form>
    </div>
    <!-- End Search Menu -->
    <!-- COMMON SCRIPTS -->
    @include('layouts.script')
    <!-- SPECIFIC SCRIPTS -->

    <!-- SPECIFIC SCRIPTS -->

    <script>
        let app = new Vue({
            el: '#app',
            name: 'main',
            delimiters: ['[[', ']]'],
            data: {
                categories: [],
                branchId: '',
                branchName: '',
                location: '',
                open_time: '',
                orders: [

                ],
                enableOdDisablebtn: false
            },
            methods: {
                removeOrderItem(orderId) {

                    console.log('test')

                    const otherOrders = this.orders.filter(order => order.orderId != orderId)

                    this.orders = [...otherOrders]

                    const storedOrdersData = localStorage.getItem('orders')
                    // nice one last issue when u dont add any quantity it adds it to the cart eg
                    if (storedOrdersData) {

                        localStorage.setItem('orders', JSON.stringify(this.orders))

                    }
                },
                GetAllCategoriesAndProducts() {
                    axios.get('/api/v1/branch/' + this.branchId + '/products')
                        .then((response) => {
                            // handle success
                            // console.log(response.data, 'untamed')
                            // console.log("My data :" + response.data.data, ' full auto')

                            let datalist = response.data['data']
                            // console.log(datalist, ' All data, its type: ', typeof datalist)
                            if (response.data.status) {
                                // console.log(" Categories :" + response.data.data);
                                for (let lit of datalist) {
                                    this.categories.push(lit)
                                }
                            } else {
                                console.log("empty " + response.data)
                            }
                        })
                        .catch(function (error) {
                            // handle error
                            console.log(error);
                        });
                },
                GetQuantityOrSize(size, product) {
                    if (size <= 0) {
                        this.GetProductQuantity(product);
                    }
                },
                GetProductQuantity(product) {
                    Swal.fire({
                        title: 'Choose Quantity',
                        input: 'number',
                        inputAttributes: {
                            autocapitalize: 'off'
                        },
                        showCancelButton: true,
                        confirmButtonText: 'confirm',
                        showLoaderOnConfirm: false,
                        allowOutsideClick: () => !swal.isLoading()
                    }).then((result) => {
                        if (result.isConfirmed) {
                            if (result.value < 1) {
                                result.value = 1
                            }
                            const quantity = result.value

                            if (product) {
                                const storedOrdersData = localStorage.getItem('orders')

                             console.log("My order data Product : ", product)
                                 
                                const orderData = {                                  
                                    orderId: product.id,
                                    name: product.name,
                                      quantity,
                                    price: product.price,
                                //    additional: product.additional,
                                    additionalPrice: product.additional != null | undefined | 'null' | 'undefined' ? product.additional.price : 0,
                                    size: product.additional != null | undefined | 'null' | 'undefined' ? product.additional.name : ""
                                }
                                this.orders.push(orderData)

                                if (storedOrdersData) {

                                    let storeOrders = JSON.parse(storedOrdersData)
                                    console.log(" My orderse :" , storeOrders);
                                    storeOrders = [...storeOrders, orderData]

                                    localStorage.setItem('orders', JSON.stringify(storeOrders))

                                } else {

                                    localStorage.setItem('orders', JSON.stringify([orderData]))
                                }

                            }
                        }
                    })
                },
                removeOrderItem(orderId) {

                    const otherOrders = this.orders.filter(order => order.orderId != orderId)

                    this.orders = [...otherOrders]

                    const storedOrdersData = localStorage.getItem('orders')
                    // nice one last issue when u dont add any quantity it adds it to the cart eg
                    if (storedOrdersData) {

                        localStorage.setItem('orders', JSON.stringify(this.orders))

                    }
                },
                getAdditionalData(size) {
                    console.log(size)
                },
                prepAdditionalData(e, size, product) {
                    console.log(size)
                    const name = size.name
                    const price = size.pivot.additional_price

                    product.additional = {
                        name,
                        price
                    }
                },
                getOrderName: function (order) {

                    if (order.additional) {

                        return order.name = `${order.name}(${order.additional.name})`
                    }

                    return order.name
                },
                getOrderPrice: function (order) {

                    if (order.additional) {

                        return order.price + order.additional.price
                    }

                    return order.price
                },
            },
            computed: {
                getSubTotal: function () {

                    return this.orders.reduce((a, b) => a + (b.price * b.quantity), 0)
                },
                getTotal: function () {
                    return this.getSubTotal  // dont add delivery fee
                },
                reactivateControls: function () {
                    jQuery('input').iCheck({
                        checkboxClass: 'icheckbox_square-green',
                        radioClass: 'iradio_square-green'
                    });
                    console.log('test2')
                }
            },
            mounted: function () {
                jQuery('input').iCheck({
                    checkboxClass: 'icheckbox_square-green',
                    radioClass: 'iradio_square-green'
                });
                var rangeSelector= $(".iradio_square-green", ".iradio_square-grey")
                .css("position", "absolute").addClass("range-selector")
                .appendTo("")
                .click(function(e) {
                    rangeSelector.html("<p>Hello</p>")
                    console.log('test')

                    $(document.elementFromPoint(e.clientX,e.clientY)).trigger("click");
                });

                rangeSelector.html("<p>Hello</p>")

                var parent = document.getElementsByClassName('range-selector')

                for(var obj of parent) {
                    var child = obj.firstElementChild
                    var objParent = obj.parentElement
                    objParent.appendChild(child)
                    obj.innerHTML = ''
                    obj.style.width = '130%'
                    obj.style.height = '130%'
                    console.log(objParent)
                }

            },
            created() {
                this.branchId = localStorage.getItem('branchId')
                console.log("My branch Id :", this.branchId);
                this.branchName = localStorage.getItem('branchName')
                this.location = localStorage.getItem('location')
                this.open_time = localStorage.getItem('openingTime')
                this.GetAllCategoriesAndProducts();


                let storedOrdersData = localStorage.getItem('orders')

                if (storedOrdersData) {

                    const storeOrders = JSON.parse(storedOrdersData)

                    this.orders = storeOrders
                }
            }
        })

    </script>


</body>

</html>
