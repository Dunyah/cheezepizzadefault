@extends('emails.layout')

@section('content')

    {!! $message !!}
    @component('mail::button', ['url' => $url, 'color' => 'blue'])
        View Order
    @endcomponent

@endsection
