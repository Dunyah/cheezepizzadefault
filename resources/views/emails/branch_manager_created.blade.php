@extends('emails.layout')

@section('content')

    {{$message}}
    @component('mail::button', ['url' => $url, 'color' => 'blue'])
        Login
    @endcomponent

@endsection
