@component('mail::message')

    @yield('content')

    Thanks,
    {{ config('app.name') }}
@endcomponent
