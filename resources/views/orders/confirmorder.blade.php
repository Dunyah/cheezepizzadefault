<!DOCTYPE html>
<!--[if IE 9]><html class="ie ie9"> <![endif]-->
<html>

<head>
    @include('layouts.head')
    <!-- Radio and check inputs -->
    <link href="css/skins/square/grey.css" rel="stylesheet">
</head>

<body>

    <!-- Header ================================================== -->
    @include('layouts.header')
    <!-- End Header =============================================== -->

    <!-- SubHeader =============================================== -->
    <section class="parallax-window" id="short" data-parallax="scroll" data-image-src="img/sub_header_cart.jpg"
        data-natural-width="1400" data-natural-height="350">
        <div id="subheader">
            <div id="sub_content">
                <h1>Place your order</h1>
                <div class="bs-wizard">
                    <div class="col-xs-4 bs-wizard-step complete">
                        <div class="text-center bs-wizard-stepnum"><strong>1.</strong> Your details</div>
                        <div class="progress">
                            <div class="progress-bar"></div>
                        </div>
                        <a href="cart.html" class="bs-wizard-dot"></a>
                    </div>

                    <div class="col-xs-4 bs-wizard-step complete">
                        <div class="text-center bs-wizard-stepnum"><strong>2.</strong> Payment</div>
                        <div class="progress">
                            <div class="progress-bar"></div>
                        </div>
                        <a href="cart_2.html" class="bs-wizard-dot"></a>
                    </div>

                    <div class="col-xs-4 bs-wizard-step complete">
                        <div class="text-center bs-wizard-stepnum"><strong>3.</strong> Finish!</div>
                        <div class="progress">
                            <div class="progress-bar"></div>
                        </div>
                        <a href="#0" class="bs-wizard-dot"></a>
                    </div>
                </div><!-- End bs-wizard -->
            </div><!-- End sub_content -->
        </div><!-- End subheader -->
    </section><!-- End section -->
    <!-- End SubHeader ============================================ -->

    <div id="position">
        <div class="container">
            <ul>
                <li><a href="#0">Home</a></li>
                <li><a href="#0">Category</a></li>
                <li>Page active</li>
            </ul>
            <a href="#0" class="search-overlay-menu-btn"><i class="icon-search-6"></i> Search</a>
        </div>
    </div><!-- Position -->

    <!-- Content ================================================== -->
    <div class="container margin_60_35">
        <div class="row">
            <div class="col-md-offset-3 col-md-6">
                <div class="box_style_2" id="app">
                    <h2 class="inner">Order confirmed!</h2>
                    <div id="confirm">
                        <i class="icon_check_alt2"></i>
                        <h3>Thank you!</h3>
                        <p>
                            Your order has been received. We will get back to you as soon as possible
                        </p>
                    </div>
                    <h4>Summary</h4>
                    <table class="table table-striped nomargin">
                        <tbody>
                            <tr v-for="order of orders" :key="order.order_id">
                                <td>
                                    <strong>[[order.quantity]]x</strong> [[order.product_name]]
                                </td>
                                <td>
                                    <strong class="pull-right">₵[[order.product_price]]</strong>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                   Delivery fee
                                </td>
                                <td>
                                    <strong class="pull-right">₵[[delivery_fee]]</strong>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    Delivery schedule <a href="#" class="tooltip-1" data-placement="top" title=""
                                        data-original-title="Please consider 30 minutes of margin for the delivery!"><i
                                            class="icon_question_alt"></i></a>
                                </td>
                                <td>
                                    <strong class="pull-right">[[ delivery_day]]</strong>
                                </td>
                            </tr>
                            <tr>
                                <td class="total_confirm">
                                    TOTAL
                                </td>
                                <td class="total_confirm">
                                    <span class="pull-right">₵[[total]]</span>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                   <div class="row" style="padding-top : 10px;">
                    <div class="col-md-6">
                        <a class="btn_full_outline" href="/products"><i class="icon-right"></i> Add other items</a>
                    </div>
                    <div class="col-md-6">
                        <a class="btn_full" href="/vieworders">View orders</a>
                    </div>
                   </div>
                </div>

            </div>
        </div><!-- End row -->
    </div><!-- End container -->
    <!-- End Content =============================================== -->

    <!-- Footer ================================================== -->
    @include('layouts.footer')
    <!-- End Footer =============================================== -->

    <div class="layer"></div><!-- Mobile menu overlay mask -->

    <!-- Login modal -->
    @include('layouts.modal')
    <!-- End modal -->

    <!-- COMMON SCRIPTS -->


    @include('layouts.script')

    <script>
        var app = new Vue({
                el: '#app',
                delimiters: ['[[', ']]'],
                data: {
                    delivery_day: '',
                    total: '',
                    delivery_fee:'',
                    orders: []
                },
                methods:{
                  GetAllordersByOrderNumber(){
                    const authUser = {
                        headers: {
                            Authorization: 'Bearer ' + localStorage.getItem('token')
                        }
                    }
                    axios.get('/api/v1/order/'+localStorage.getItem("orderId"),authUser)
                        .then((response) => {
                            if (response.data.status) {
                                 this.orders = response.data.items;
                                 this.total = response.data.order.total_price;
                                 this.delivery_day = response.data.order.expected_delivery_day;
                                 this.delivery_fee = response.data.order.delivery_fee;
                                 console.log("Total " , this.total);
                                  console.log("Checkout order items " , response.data)                     
                            } else {
                                Vue.$toast.warning(response.data.message, {
                                    position:'bottom-right'
                                    })
                            }
                        })
                        .catch(function (error) {
                            Vue.$toast.error(error, {
                                    position:'bottom-right'
                                    })
                        });
                  }
                },
                created() {
                    this. GetAllordersByOrderNumber();
                    //  this.delivery_day = localStorage.getItem("expected_delivery_day");
                    // let storedOrdersData = localStorage.getItem('orders')

                    // if (storedOrdersData) {

                    //     const storeOrders = JSON.parse(storedOrdersData)

                    //     this.orders = storeOrders
                    // }
                },
                computed: {
                    // getSubTotal: function () {

                    //     return this.orders.reduce((a, b) => a + (b.price * b.quantity), 0)
                    // },
                    // getTotal: function () {
                    //     this.total = this.getSubTotal + 10;
                    //     return this.getSubTotal + 10
                    // }
                }
        })
     
    </script>
</body>

</html>
