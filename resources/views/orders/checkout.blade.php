<!DOCTYPE html>
<!--[if IE 9]><html class="ie ie9"> <![endif]-->
<html>

<head>
    @include('layouts.head')
    <!-- Radio and check inputs -->
    <link href="css/skins/square/grey.css" rel="stylesheet">
</head>

<body>

    <!-- Header ================================================== -->
    @include('layouts.header')
    <!-- End Header =============================================== -->

    <!-- SubHeader =============================================== -->
    <section class="parallax-window" id="short" data-parallax="scroll" data-image-src="img/sub_header_cart.jpg"
        data-natural-width="1400" data-natural-height="350">
        <div id="subheader">
            <div id="sub_content">
                <h1>Place your order</h1>
                <div class="bs-wizard">
                    <div class="col-xs-4 bs-wizard-step complete">
                        <div class="text-center bs-wizard-stepnum"><strong>1.</strong> Your details</div>
                        <div class="progress">
                            <div class="progress-bar"></div>
                        </div>
                        <a href="/products" class="bs-wizard-dot"></a>
                    </div>

                    <div class="col-xs-4 bs-wizard-step active">
                        <div class="text-center bs-wizard-stepnum"><strong>2.</strong> Payment</div>
                        <div class="progress">
                            <div class="progress-bar"></div>
                        </div>
                        <a href="/carts" class="bs-wizard-dot"></a>
                    </div>

                    <div class="col-xs-4 bs-wizard-step disabled">
                        <div class="text-center bs-wizard-stepnum"><strong>3.</strong> Finish!</div>
                        <div class="progress">
                            <div class="progress-bar"></div>
                        </div>
                        <a href="#0" class="bs-wizard-dot"></a>
                    </div>
                </div><!-- End bs-wizard -->
            </div><!-- End sub_content -->
        </div><!-- End subheader -->
    </section><!-- End section -->
    <!-- End SubHeader ============================================ -->

    <div id="position">
        <div class="container">
            <ul>
                <li><a href="#0">Home</a></li>
                <li><a href="#0">Checkout</a></li>
                <li>Page active</li>
            </ul>
            <!-- <a href="#0" class="search-overlay-menu-btn"><i class="icon-search-6"></i> Search</a> -->
        </div>
    </div><!-- Position -->

    <!-- Content ================================================== -->
    <div class="container margin_60_35">
        <div class="row">
            <div class="col-md-3">
                <div class="box_style_2 hidden-xs info">
                    <h4 class="nomargin_top">Delivery time <i class="icon_clock_alt pull-right"></i></h4>
                    <p>
                        Our efficient delivery bike riders will get to you before the your food is cold.
                    </p>
                    <hr>
                    <h4>Secure payment <i class="icon_creditcard pull-right"></i></h4>
                    <p>
                        We provide secure payments on all transactions you do on our platform
                    </p>
                </div><!-- End box_style_2 -->

                <div class="box_style_2 hidden-xs" id="help">
                    <i class="icon_lifesaver"></i>
                    <h4>Need <span>Help?</span></h4>
                    <a href="tel://004542344599" class="phone">0544133397</a>
                    <small>Monday to Friday 9.00am - 7.30pm</small>
                </div>
            </div><!-- End col-md-3 -->
          <div id="app">
            <div class="col-md-6">
                <div class="box_style_2">
                    <h2 class="inner">Payment methods</h2>
                    <div class="payment_select">
                        <label><input type="radio" value="" v-model="payment_method" checked="checked" name="payment_method" class="icheck"> Payment On Delivery</label>
                        <i class="icon_creditcard"></i>
                    </div>
                    <div class="payment_select">
                        <label><input type="radio" value="" v-model="payment_method" name="payment_method" class="icheck">Cash On Pickup</label>
                        <i class="icon_creditcard"></i>
                    </div>

                    <div class="payment_select nomargin">
                        <label><input type="radio" value="" v-model="payment_method" name="payment_method" class="icheck">MTN Mobile Money</label>
                        <i class="icon_wallet"></i>
                    </div>

                </div><!-- End box_style_1 -->
            </div><!-- End col-md-6 -->

            <div class="col-md-3" id="sidebar">
                <div class="theiaStickySidebar">
                    <div id="cart_box">
                        <h3>Your order <i class="icon_cart_alt pull-right"></i></h3>
                        <table class="table table_summary">
                            <tbody>
                                <tr v-for="order of orders" :key="order.orderId">
                                    <td>
                                        <a href="#0" class="remove_item" v-on:click="removeOrderItem(order.orderId)">
                                            <i class="icon_minus_alt"></i></a> <strong>[[ order.quantity]]x</strong>
                                        [[order.name ]]
                                    </td>
                                    <td>
                                        <strong class="pull-right">₵[[ order.price ]]</strong>
                                    </td>
                                </tr>
                                <span v-if="!orders.length" style="width: 100%;" class="text-center">You have no
                                    orders</span>
                            </tbody>
                        </table>
                        <hr>
                        <div class="row" id="options_2">
                            <div class="col-lg-6 col-md-12 col-sm-12 col-xs-6">
                                <label><input type="radio" value="" checked name="option_2"
                                        class="icheck">Delivery</label>
                            </div>
                            <div class="col-lg-6 col-md-12 col-sm-12 col-xs-6">
                                <label><input type="radio" value="" name="option_2" class="icheck">Take Away</label>
                            </div>
                        </div><!-- Edn options 2 -->
                        <hr>
                        <table class="table table_summary">
							<tbody>
								<tr>
									<td>
										Subtotal <span class="pull-right" v-if="this.orders.length">₵[[ getSubTotal
											]]</span> <span class="pull-right" v-else>----</span>
									</td>
								</tr>
								<tr>
									<td>
										Delivery fee <span class="pull-right">₵0</span>
									</td>
								</tr>
								<tr>
									<td class="total">
										TOTAL <span class="pull-right" v-if="this.orders.length">₵[[ getTotal]]</span>
										<span class="pull-right" v-else>----</span>
									</td>
								</tr>
							</tbody>
                        </table>
                        <hr>
                        <a class="btn_full" v-if="orders.length"  v-on:click="Submitorders">Confirm your order</a>
                    </div><!-- End cart_box -->
                </div><!-- End theiaStickySidebar -->
            </div><!-- End col-md-3 -->
		</div>
        </div><!-- End row -->
    </div><!-- End container -->
    <!-- End Content =============================================== -->

    <!-- Footer ================================================== -->
    @include('layouts.footer')
    <!-- End Footer =============================================== -->

    <div class="layer"></div><!-- Mobile menu overlay mask -->

    <!-- Login modal -->
    @include('layouts.modal')
    <!-- End modal -->




    <!-- End Search Menu -->

    <!-- COMMON SCRIPTS -->
    @include('layouts.script')

    <script>
        var app = new Vue({
			el: '#app',
			delimiters: ['[[', ']]'],
            data: {
                payment_method:'',
                total:'',
                delivery_fee :'',
                orders: []
            },
            methods: {
                removeOrderItem(orderId) {

                    console.log('test')

                    const otherOrders = this.orders.filter(order => order.orderId != orderId)

                    this.orders = [...otherOrders]

                    const storedOrdersData = localStorage.getItem('orders')
                    // nice one last issue when u dont add any quantity it adds it to the cart eg
                    if (storedOrdersData) {

                        localStorage.setItem('orders', JSON.stringify(this.orders))

                    }
                },
                Submitorders(){
					  const authUser = {
                        headers: {
                            Authorization: 'Bearer ' + localStorage.getItem('token')
                        }
                    }
                    axios.post('/api/v1/checkout', {           
                             branch_id: localStorage.getItem("branchId"),
                             address : localStorage.getItem("address"),
                             expected_delivery_day : localStorage.getItem("expected_delivery_day"),
                             expected_delivery_time : localStorage.getItem("delivery_schedule_time"),
                             recipient_phone : localStorage.getItem("recipient_phone"),
                             delivery : 1,//localStorage.getItem("address")
                             delivery_location: localStorage.getItem("delivery_location"),
                             delivery_lat : 0,
                             delivery_lng : 0,
                             total_price : this.total,
                             payment_option :1,                            
                             items: this.orders.map(order => { 
                                    return Object.assign(order, {
                                        additional_price: order.additionalPrice,
                                        id: order.orderId,
                                        size: order.size
                                    })
                            })
                        },authUser)
                        .then((response) => {
                            if (response.data.status) {
                                Vue.$toast.success(response.data.message, {
                                    position:'bottom-right'
                                    })        
                                    localStorage.setItem("orderId", response.data.orderId)     
                                    console.log(" Checkout data ", response.data);                   
                                    localStorage.removeItem("orders");                         
                                    window.location.href ="/confirm";  
                            } else {
                                Vue.$toast.warning(response.data.message, {
                                    position:'bottom-right'
                                    })
                            }
                        })
                        .catch(function (error) {
                            Vue.$toast.error(error, {
                                    position:'bottom-right'
                                    })
                        });
                }
            },
            created() {

                let storedOrdersData = localStorage.getItem('orders')

                if (storedOrdersData) {

                    const storeOrders = JSON.parse(storedOrdersData)

                    this.orders = storeOrders
                }
            },
            mounted: function () {
                jQuery('input').iCheck({
                    checkboxClass: 'icheckbox_square-green',
                    radioClass: 'iradio_square-green'
                });
                var rangeSelector= $(".iradio_square-green", ".iradio_square-grey")
                .css("position", "absolute").addClass("range-selector")
                .appendTo("")
                .click(function(e) {
                    rangeSelector.html("<p>Hello</p>")
                    console.log('test')

                    $(document.elementFromPoint(e.clientX,e.clientY)).trigger("click");
                });

                rangeSelector.html("<p>Hello</p>")

                var parent = document.getElementsByClassName('range-selector')

                for(var obj of parent) {
                    var child = obj.firstElementChild
                    var objParent = obj.parentElement
                    objParent.appendChild(child)
                    obj.innerHTML = ''
                    obj.style.width = '130%'
                    obj.style.height = '130%'
                    console.log(objParent)
                }

            },
            computed: {
                getSubTotal: function () {

                    return this.orders.reduce((a, b) => a + (b.price * b.quantity), 0)
                },
                getTotal: function () {
                    this.total = this.getSubTotal ;
                    return this.getSubTotal
                }
            }
        })

    </script>
</body>

</html>
