<!DOCTYPE html>
<!--[if IE 9]><html class="ie ie9"> <![endif]-->
<html>

<head>
    @include('layouts.head')
    <link href="css/skins/square/grey.css" rel="stylesheet">
    <link href="css/skins/square/grey.css" rel="stylesheet">
    <link href="css/admin.css" rel="stylesheet">
    <link href="css/bootstrap3-wysihtml5.min.css" rel="stylesheet">
    <link href="css/dropzone.css" rel="stylesheet">
</head>

<body>
    <!-- Header ================================================== -->
    @include('layouts.header')
    <!-- End Header =============================================== -->

    <!-- SubHeader =============================================== -->
    <section class="parallax-window" id="short" data-parallax="scroll" data-image-src="img/sub_header_cart.jpg"
        data-natural-width="1400" data-natural-height="350">
        <div id="subheader">
            <div id="sub_content">
                <h1>Order details</h1>
                <p></p>
                <p></p>
            </div><!-- End sub_content -->
        </div><!-- End subheader -->
    </section><!-- End section -->
    <!-- End SubHeader ============================================ -->

    <div id="position">
        <div class="container">
            <ul>
                <li><a href="#0">Home</a>
                </li>
                <li><a href="#0">Category</a>
                </li>
                <li>Page active</li>
            </ul>
            <a href="#0" class="search-overlay-menu-btn"><i class="icon-search-6"></i> Search</a>
        </div>
    </div><!-- Position -->

    <!-- Content ================================================== -->
    <div class="container margin_60">
        <div id="tabs" class="tabs">
            <nav>
                <ul>
                    <li><a href="#section-1" class="icon-profile"><span>Order item details</span></a>
                    </li>
                </ul>
            </nav>
            <div class="content" id="app">

                <section id="section-1">
                    <div class="indent_title_in">
                        <table class="table table-striped nomargin">
                            <tbody>
                                <tr>
                                    <th> Category Name </th>
                                    <th> Product Name </th>
                                    <th> Unit Price (GHS) </th>
                                    <th> Quantity </th>
                                    <th> Delivery Fee(s) </th>
                                    <th> Total Amount (GHS) </th>
                                    <th> Order Status </th>
                                    <th> Order Delivery Day </th>
                                </tr>
                                <tr v-for="order of orders" :key="order.order_id">
                                    <td> [[ order.category_name ]]</td>
                                    <td> [[ order.product_name ]]</td>
                                    <td> [[ order.product_price ]]</td>
                                    <td> [[ order.quantity ]]</td>
                                    <td> [[ delivery_fee ]]</td>
                                    <td> [[ total ]]</td>
                                    <td> [[ order.status ]]</td>
                                    <td> [[ delivery_day ]]</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    <hr class="styled_2">             
                </section><!-- End section 1 -->

            </div><!-- End content -->
        </div>
    </div><!-- End container  -->
    <!-- End Content =============================================== -->

    <!-- Footer ================================================== -->
    @include('layouts.footer')
    <!-- End Footer =============================================== -->

    <div class="layer"></div>
    <!-- Mobile menu overlay mask -->

    <!-- Login modal -->

    @include('layouts.modal')
    <!-- Search Menu -->
    <div class="search-overlay-menu">
        <span class="search-overlay-close"><i class="icon_close"></i></span>
        <form role="search" id="searchform" method="get">
            <input value="" name="q" type="search" placeholder="Search..." />
            <button type="submit"><i class="icon-search-6"></i>
            </button>
        </form>
    </div>
    <!-- End Search Menu -->
    <!-- COMMON SCRIPTS -->
    @include('layouts.script')
    <!-- Specific scripts -->
    <script src="js/tabs.js"></script>
    <script>
        new CBPFWTabs(document.getElementById('tabs'));

    </script>
    <script src="js/dropzone.min.js"></script>

    <script>
        var app = new Vue({
            el: '#app',
            delimiters: ['[[', ']]'],
            data: {
                orders: [],
                total:'',
                delivery_day:'',
                delivery_fee:''
            },
            methods: {
                GetAllordersByOrderNumber(){
                    const authUser = {
                        headers: {
                            Authorization: 'Bearer ' + localStorage.getItem('token')
                        }
                    }
                    axios.get('/api/v1/order/'+localStorage.getItem("orderId"),authUser)
                        .then((response) => {
                            if (response.data.status) {
                                 this.orders = response.data.items;
                                 this.total = response.data.order.total_price;
                                 this.delivery_day = response.data.order.expected_delivery_day;
                                 this.delivery_fee = response.data.order.delivery_fee;
                                 console.log("Total " , this.total);                   
                            } else {
                                Vue.$toast.warning(response.data.message, {
                                    position:'bottom-right'
                                    })
                            }
                        })
                        .catch(function (error) {
                            Vue.$toast.error(error, {
                                    position:'bottom-right'
                                    })
                        });
                  }
            },
            created() {
                this.GetAllordersByOrderNumber();
            }
        })

    </script>
</body>

</html>
