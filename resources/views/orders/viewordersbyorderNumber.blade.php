<!DOCTYPE html>
<!--[if IE 9]><html class="ie ie9"> <![endif]-->
<html>

<head>
    @include('layouts.head')
    <link href="css/skins/square/grey.css" rel="stylesheet">
    <link href="css/skins/square/grey.css" rel="stylesheet">
    <link href="css/admin.css" rel="stylesheet">
    <link href="css/bootstrap3-wysihtml5.min.css" rel="stylesheet">
    <link href="css/dropzone.css" rel="stylesheet">
</head>

<body>
    <!-- Header ================================================== -->
    @include('layouts.header')
    <!-- End Header =============================================== -->

    <!-- SubHeader =============================================== -->
    <section class="parallax-window" id="short" data-parallax="scroll" data-image-src="img/sub_header_cart.jpg"
        data-natural-width="1400" data-natural-height="350">
        <div id="subheader">
            <div id="sub_content">
                <h1>My account</h1>
                <p>manage your orders and profile</p>
                <p></p>
            </div><!-- End sub_content -->
        </div><!-- End subheader -->
    </section><!-- End section -->
    <!-- End SubHeader ============================================ -->

    <div id="position">
        <div class="container">
            <ul>
                <li><a href="#0">Home</a>
                </li>
                <li><a href="#0">Category</a>
                </li>
                <li>Page active</li>
            </ul>
            <a href="#0" class="search-overlay-menu-btn"><i class="icon-search-6"></i> Search</a>
        </div>
    </div><!-- Position -->

    <!-- Content ================================================== -->

    <div class="container margin_60">
        <div class="col-md-10 col-md-offset-1">
            <div class="col-md-10">
                <input type="text" class="form-control" v-model placeholder="Search by order number"/>
            </div>
            <div class="col-md-2">
                <input type="button" value="Get orders"  class="btn_1 green" />
            </div>
        </div> 
    </div>
    

    <div class="container margin_60" id="app">
        <div id="tabs" class="tabs">
            <nav>
                <ul>
                    <li class="getSelectedTab" id="section-1-tab" data-content="section-1"><a href="#" class="icon-profile">Order information</a>
                    </li>
                </ul>
            </nav>
            <div class="content">

                <section id="section-1" class="mycontent content-current">
                    <div class="indent_title_in">
                        {{-- <h3>General restaurant description</h3> --}}
                        <table class="table table-striped nomargin">
                            <tbody>
                                <tr>
                                    <th> Order Number </th>
                                    <th> Branch Name </th>
                                    <th> Delivery Day </th>
                                    <th> Total price </th>
                                    <th> Order Status </th>
                                    <th> Action </th>
                                </tr>
                                <tr v-for="order of vieworders" :key="order.id">
                                    <td> [[ order.number ]]</td>
                                    <td> [[ order.branch_name ]]</td>
                                    <td> [[ order.expected_delivery_day ]]</td>
                                    <td> [[ order.total_price ]]</td>
                                    <td> [[ order.status ]]</td>
                                    <td>  <button type="submit" v-on:click="ViewItemsByOrderId(order.id)" class="btn_1 green">View details</button></td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    <hr class="styled_2">             
                </section><!-- End section 1 -->
            </div><!-- End content -->
        </div>
    </div><!-- End container  -->
    <!-- End Content =============================================== -->

    <!-- Footer ================================================== -->
    @include('layouts.footer')
    <!-- End Footer =============================================== -->

    <div class="layer"></div>
    <!-- Mobile menu overlay mask -->

    <!-- Login modal -->

    @include('layouts.modal')
    <!-- Search Menu -->
    <div class="search-overlay-menu">
        <span class="search-overlay-close"><i class="icon_close"></i></span>
        <form role="search" id="searchform" method="get">
            <input value="" name="q" type="search" placeholder="Search..." />
            <button type="submit"><i class="icon-search-6"></i>
            </button>
        </form>
    </div>
    <!-- End Search Menu -->
    <!-- COMMON SCRIPTS -->
    @include('layouts.script')
    <!-- Specific scripts -->
    {{-- <script src="js/tabs.js"></script> --}}
    <script>
        new CBPFWTabs(document.getElementById('tabs'));

    </script>
    <script src="js/dropzone.min.js"></script>
    <script>
        var app = new Vue({
            el: '#app',
            delimiters: ['[[', ']]'],
            data: {
                vieworders: [],
                orderNumber:''
            },
            methods: {
                GetCustomerOrderByOrderNumber() {
                    const authUser = {
                        headers: {
                            Authorization: 'Bearer ' + localStorage.getItem('token')
                        }
                    }
                    axios.get('/api/v1/orders/pending', authUser)
                        .then((response) => {
                            if (response.data.status) {
                                this.vieworders = response.data.orders;
                                // console.log("View Pending orders : ", this.vieworders)
                            } else {
                                Vue.$toast.warning(response.data.message, {
                                    position: 'bottom-right'
                                })
                            }
                        })
                        .catch(function (error) {
                            Vue.$toast.error(error, {
                                position: 'bottom-right'
                            })
                        });
                },
                ViewItemsByOrderId(id){
                    localStorage.setItem("orderId" , id);
                    window.location.href ="/orderDetails"
                }
            },
            created() {
                this.GetCustomerOrderByOrderNumber();               
            }
        })

    </script>
</body>

</html>
