<!DOCTYPE html>
<!--[if IE 9]><html class="ie ie9"> <![endif]-->
<html>

<head>
    @include('layouts.head')
    <link href="css/skins/square/grey.css" rel="stylesheet">
    <link href="css/skins/square/grey.css" rel="stylesheet">
    <link href="css/admin.css" rel="stylesheet">
    <link href="css/bootstrap3-wysihtml5.min.css" rel="stylesheet">
    <link href="css/dropzone.css" rel="stylesheet">
</head>

<body>
    <!-- Header ================================================== -->
    @include('layouts.header')
    <!-- End Header =============================================== -->

    <!-- SubHeader =============================================== -->
    <section class="parallax-window" id="short" data-parallax="scroll" data-image-src="img/sub_header_cart.jpg"
        data-natural-width="1400" data-natural-height="350">
        <div id="subheader">
            <div id="sub_content">
                <h1>My account</h1>
                <p>manage your orders and profile</p>
                <p></p>
            </div><!-- End sub_content -->
        </div><!-- End subheader -->
    </section><!-- End section -->
    <!-- End SubHeader ============================================ -->

    <div id="position">
        <div class="container">
            <ul>
                <li><a href="#0">Home</a>
                </li>
                <li><a href="#0">Category</a>
                </li>
                <li>Page active</li>
            </ul>
            <a href="#0" class="search-overlay-menu-btn"><i class="icon-search-6"></i> Search</a>
        </div>
    </div><!-- Position -->

    <!-- Content ================================================== -->
    <div class="container margin_60" id="app">
        <div id="tabs" class="tabs">
            <nav>
                <ul>
                    <li class="getSelectedTab" id="section-1-tab" data-content="section-1"><a href="#" class="icon-profile">Pending orders</a>
                    </li>
                    <li class="getSelectedTab" id="section-2-tab" data-content="section-2"><a href="#" class="icon-menut-items">Completed orders</a>
                    </li>
                    <li class="getSelectedTab" id="section-3-tab" data-content="section-3"><a href="#" class="icon-settings">Settings</a>
                    </li>
                </ul>
            </nav>
            <div class="content">

                <section id="section-1" class="mycontent content-current">
                    <div class="indent_title_in">
                        {{-- <h3>General restaurant description</h3> --}}
                        <table class="table table-striped nomargin">
                            <tbody>
                                <tr>
                                    <th> Order Number </th>
                                    <th> Branch Name </th>
                                    <th> Delivery Day </th>
                                    <th> Total price </th>
                                    <th> Order Status </th>
                                    <th> Action </th>
                                </tr>
                                <tr v-for="order of vieworders" :key="order.id">
                                    <td> [[ order.number ]]</td>
                                    <td> [[ order.branch_name ]]</td>
                                    <td> [[ order.expected_delivery_day ]]</td>
                                    <td> [[ order.total_price ]]</td>
                                    <td> [[ order.status ]]</td>
                                    <td>  <button type="submit" v-on:click="ViewItemsByOrderId(order.id)" class="btn_1 green">View details</button></td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    <hr class="styled_2">             
                </section><!-- End section 1 -->
 
                <!-- End section 2 -->
                <section id="section-2" class="mycontent">
                    <div class="indent_title_in ">
                        <table class="table table-striped nomargin">
                            <tbody>
                                <tr>
                                    <th> Order Number </th>
                                    <th> Branch Name </th>
                                    <th> Delivery Day </th>
                                    <th> Total price </th>
                                    <th> Order Status </th>
                                    <th> Action </th>
                                </tr>
                                <tr v-for="order of viewcompletedorders" :key="order.id">
                                    <td> [[ order.number ]]</td>
                                    <td> [[ order.branch_name ]]</td>
                                    <td> [[ order.expected_delivery_day ]]</td>
                                    <td> [[ order.total_price ]]</td>
                                    <td> [[ order.status ]]</td>
                                    <td>  <button type="submit" class="btn_1 green">View details</button></td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    <hr class="styled_2">
                </section><!-- End section 2 -->
                <section id="section-3" class="mycontent">
                    <div class="row">

                        <div class="col-md-6 col-sm-6 add_bottom_15">
                            <div class="indent_title_in">
                                <i class="icon_lock_alt"></i>
                                <h3>Change your password</h3>
                                <p>
                                    Mussum ipsum cacilds, vidis litro abertis.
                                </p>
                            </div>
                            <div class="wrapper_indent">
                                <div class="form-group">
                                    <label>Old password</label>
                                    <input class="form-control" name="old_password" id="old_password" type="password">
                                </div>
                                <div class="form-group">
                                    <label>New password</label>
                                    <input class="form-control" name="new_password" id="new_password" type="password">
                                </div>
                                <div class="form-group">
                                    <label>Confirm new password</label>
                                    <input class="form-control" name="confirm_new_password" id="confirm_new_password"
                                        type="password">
                                </div>
                                <button type="submit" class="btn_1 green">Update Password</button>
                            </div><!-- End wrapper_indent -->
                        </div>

                        <div class="col-md-6 col-sm-6 add_bottom_15">
                            <div class="indent_title_in">
                                <i class="icon_mail_alt"></i>
                                <h3>Change your email</h3>
                                <p>
                                    Mussum ipsum cacilds, vidis litro abertis.
                                </p>
                            </div>
                            <div class="wrapper_indent">
                                <div class="form-group">
                                    <label>Old email</label>
                                    <input class="form-control" name="old_email" id="old_email" type="email">
                                </div>
                                <div class="form-group">
                                    <label>New email</label>
                                    <input class="form-control" name="new_email" id="new_email" type="email">
                                </div>
                                <div class="form-group">
                                    <label>Confirm new email</label>
                                    <input class="form-control" name="confirm_new_email" id="confirm_new_email"
                                        type="email">
                                </div>
                               
                            </div><!-- End wrapper_indent -->
                        </div>

                    </div><!-- End row -->

                    <hr class="styled_2">

                    <div class="indent_title_in">
                        <i class="icon_shield"></i>
                        <h3>Notification settings</h3>
                        <p>
                            Mussum ipsum cacilds, vidis litro abertis.
                        </p>
                    </div>
                    <div class="row">

                        <div class="col-md-6 col-sm-6">
                            <div class="wrapper_indent">
                                <table class="table table-striped notifications">
                                    <tbody>
                                        <tr>
                                            <td style="width:5%">
                                                <i class="icon_pencil-edit_alt"></i>
                                            </td>
                                            <td style="width:65%">
                                                New orders
                                            </td>
                                            <td style="width:35%">
                                                <label>
                                                    <input type="checkbox" name="option_1_settings" checked
                                                        class="icheck" value="yes">Yes</label>
                                                <label class="margin_left">
                                                    <input type="checkbox" name="option_1_settings" class="icheck"
                                                        value="no">No</label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <i class="icon_pencil-edit_alt"></i>
                                            </td>
                                            <td>
                                                Modified orders
                                            </td>
                                            <td>
                                                <label>
                                                    <input type="checkbox" name="option_2_settings" checked
                                                        class="icheck" value="yes">Yes</label>
                                                <label class="margin_left">
                                                    <input type="checkbox" name="option_2_settings" class="icheck"
                                                        value="no">No</label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <i class="icon_pencil-edit_alt"></i>
                                            </td>
                                            <td>
                                                New user registration
                                            </td>
                                            <td>
                                                <label>
                                                    <input type="checkbox" name="option_3_settings" checked
                                                        class="icheck" value="yes">Yes</label>
                                                <label class="margin_left">
                                                    <input type="checkbox" name="option_3_settings" class="icheck"
                                                        value="no">No</label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <i class="icon_pencil-edit_alt"></i>
                                            </td>
                                            <td>
                                                New comments
                                            </td>
                                            <td>
                                                <label>
                                                    <input type="checkbox" name="option_4_settings" checked
                                                        class="icheck" value="yes">Yes</label>
                                                <label class="margin_left">
                                                    <input type="checkbox" name="option_4_settings" class="icheck"
                                                        value="no">No</label>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                                <button type="submit" class="btn_1 green">Update notifications settings</button>
                            </div>

                        </div><!-- End row -->
                    </div><!-- End wrapper_indent -->

                </section><!-- End section 3 -->

            </div><!-- End content -->
        </div>
    </div><!-- End container  -->
    <!-- End Content =============================================== -->

    <!-- Footer ================================================== -->
    @include('layouts.footer')
    <!-- End Footer =============================================== -->

    <div class="layer"></div>
    <!-- Mobile menu overlay mask -->

    <!-- Login modal -->

    @include('layouts.modal')
    <!-- Search Menu -->
    <div class="search-overlay-menu">
        <span class="search-overlay-close"><i class="icon_close"></i></span>
        <form role="search" id="searchform" method="get">
            <input value="" name="q" type="search" placeholder="Search..." />
            <button type="submit"><i class="icon-search-6"></i>
            </button>
        </form>
    </div>
    <!-- End Search Menu -->
    <!-- COMMON SCRIPTS -->
    @include('layouts.script')
    <!-- Specific scripts -->
    {{-- <script src="js/tabs.js"></script> --}}
    <script>
        new CBPFWTabs(document.getElementById('tabs'));

    </script>
    <script src="js/dropzone.min.js"></script>
    <script>
        var app = new Vue({
            el: '#app',
            delimiters: ['[[', ']]'],
            data: {
                vieworders: [],
                viewcompletedorders:[]
            },
            methods: {
                setTab(e, tab) {
                    e.preventDefault()
                    e.stopPropagation()
                    console.log(e.target.classList.add('tab-current'))
                },
                GetPendingOrdersPerCustomer() {
                    const authUser = {
                        headers: {
                            Authorization: 'Bearer ' + localStorage.getItem('token')
                        }
                    }
                    axios.get('/api/v1/orders/pending', authUser)
                        .then((response) => {
                            if (response.data.status) {
                                this.vieworders = response.data.orders;
                                // console.log("View Pending orders : ", this.vieworders)
                            } else {
                                Vue.$toast.warning(response.data.message, {
                                    position: 'bottom-right'
                                })
                            }
                        })
                        .catch(function (error) {
                            Vue.$toast.error(error, {
                                position: 'bottom-right'
                            })
                        });
                },
                GetCompletedOrdersPerCustomer() {
                    const authUser = {
                        headers: {
                            Authorization: 'Bearer ' + localStorage.getItem('token')
                        }
                    }
                    axios.get('/api/v1/orders/completed', authUser)
                        .then((response) => {
                            if (response.data.status) {
                                this.viewcompletedorders = response.data.orders;
                                // console.log("View completed orders ", this.viewcompletedorders)
                            } else {
                                Vue.$toast.warning(response.data.message, {
                                    position: 'bottom-right'
                                })
                            }
                        })
                        .catch(function (error) {
                            Vue.$toast.error(error, {
                                position: 'bottom-right'
                            })
                        });
                },
                ViewItemsByOrderId(id){
                    localStorage.setItem("orderId" , id);
                    window.location.href ="/orderDetails"
                },
                resetTabs(defaultId) {
                    var tabs = document.getElementsByClassName('getSelectedTab')

                    for(var i = 0; i < tabs.length; i++) {

                        tabs[i].classList.remove('tab-current')

                        document.getElementById(defaultId).classList.add('tab-current')
                        // console.log(tabs[i].getAttribute('data-content'))

                        var targetElement = document.getElementById(tabs[i].getAttribute('data-content'))
                        console.log(targetElement)

                        if(targetElement) { 
                            targetElement.classList.remove('content-current')
                        }
                        // document.getElementById(tabs[i].getAttribute('data-content')).classList.remove('content-current')
                    }
                },
            },
            created() {
                this.GetPendingOrdersPerCustomer();
                this.GetCompletedOrdersPerCustomer();

                
            },
            mounted() {

                var tabs = document.getElementsByClassName('getSelectedTab')

                var self = this

                for(var i = 0; i < tabs.length; i++) {

                   tabs[i].addEventListener('click', function(e) {
                        self.resetTabs(this.id)

                        var targetElement = document.getElementById(this.getAttribute('data-content'))

                        if(targetElement) { 
                            targetElement.classList.add('content-current')
                        }
                        
                   }, false)
                }
            }
        })

    </script>
</body>

</html>
