<!DOCTYPE html>
<!--[if IE 9]><html class="ie ie9"> <![endif]-->
<html>

<head>
    @include('layouts.head')
    <!-- Radio and check inputs -->
    <link href="css/skins/square/grey.css" rel="stylesheet">
</head>

<body>


    <!-- Header ================================================== -->
    @include('layouts.header')
    <!-- End Header =============================================== -->

    <!-- SubHeader =============================================== -->
    <section class="parallax-window" id="short" data-parallax="scroll" data-image-src="img/sub_header_cart.jpg"
        data-natural-width="1400" data-natural-height="350">
        <div id="subheader">
            <div id="sub_content">
                <h1>Place your order</h1>
                <div class="bs-wizard">
                    <div class="col-xs-4 bs-wizard-step active">
                        <div class="text-center bs-wizard-stepnum"><strong>1.</strong> Your details</div>
                        <div class="progress">
                            <div class="progress-bar"></div>
                        </div>
                        <a href="/products" class="bs-wizard-dot"></a>
                    </div>

                    <div class="col-xs-4 bs-wizard-step disabled">
                        <div class="text-center bs-wizard-stepnum"><strong>2.</strong> Payment</div>
                        <div class="progress">
                            <div class="progress-bar"></div>
                        </div>
                        <a href="#" class="bs-wizard-dot"></a>
                    </div>

                    <div class="col-xs-4 bs-wizard-step disabled">
                        <div class="text-center bs-wizard-stepnum"><strong>3.</strong> Finish!</div>
                        <div class="progress">
                            <div class="progress-bar"></div>
                        </div>
                        <a href="#" class="bs-wizard-dot"></a>
                    </div>
                </div><!-- End bs-wizard -->
            </div><!-- End sub_content -->
        </div><!-- End subheader -->
    </section><!-- End section -->
    <!-- End SubHeader ============================================ -->

    <div id="position">
        <div class="container">
            <ul>
                <li><a href="#0">Home</a></li>
                <li><a href="#0">Carts</a></li>
                <li>Page active</li>
            </ul>
            <!-- <a href="#0" class="search-overlay-menu-btn"><i class="icon-search-6"></i> Search</a> -->
        </div>
    </div><!-- Position -->

    <!-- Content ================================================== -->

    <div class="container margin_60_35">
        <div class="row">
            <div class="col-md-3">

                <div class="box_style_2 hidden-xs info">
                    <h4 class="nomargin_top">Delivery time <i class="icon_clock_alt pull-right"></i></h4>
                    <p>
                        Our efficient delivery bike riders will get to you before the your food is cold.
                    </p>
                    <hr>
                    <h4>Secure payment <i class="icon_creditcard pull-right"></i></h4>
                    <p>
                        We provide secure payments on all transactions you do on our platform
                    </p>
                </div><!-- End box_style_1 -->

                <div class="box_style_2 hidden-xs" id="help">
                    <i class="icon_lifesaver"></i>
                    <h4>Need <span>Help?</span></h4>
                    <a href="tel://004542344599" class="phone">0544133397</a>
                    <small>Monday to Friday 9.00am - 7.30pm</small>
                </div>

            </div><!-- End col-md-3 -->
            <div id="app">
                <div class="col-md-6">
                    <div class="box_style_2" id="order_process">
                        <h2 class="inner">Your order details</h2>
                        <div class="form-check" v-if="showCheckbox">
                            <input type="checkbox" class="form-check-input" id="Loginaccount"
                                v-on:change="CheckButtonStatus" v-model="checkedForLogin">
                            <label class="form-check-label" for="Loginaccount">I already have an account</label>
                        </div>
                        {{-- login --}}
                        <div v-if="checkedForLogin">
                            <div v-bind:class="{'form-group': true, 'has-error': emailAddressError, 'has-success': !emailAddressError }">
                                <label>Email</label>
                                <input type="email" id="email" name="emailAddress" v-model="emailAddress" class="form-control"
                                    placeholder="Your email">
                                    <small id="fullnameHelp" class="text-danger" v-if="emailAddressError">
                                        Email address is required
                                      </small>  
                            </div>
                            <div v-bind:class="{'form-group': true, 'has-error': passwordError, 'has-success': !passwordError }">
                                <label>Password</label>
                                <input type="password" v-model="password" class="form-control" id="password"
                                    name="password" placeholder="Password">
                                    <small id="fullnameHelp" class="text-danger" v-if="emailAddressError">
                                       Password is required
                                      </small> 
                            </div>
                            <a class="btn_full" v-on:click="LoginCustomer"> Login </a>
                        </div>
                        {{-- register --}}
                        <div v-if="registercustomer">
                            <div v-bind:class="{'form-group': true, 'has-error': fullnameError, 'has-success': !fullnameError }">
                                <label>Full name</label>
                                <input type="text" v-model="fullname" id="fullname" name="fullname" class="form-control"
                                    placeholder="Full name" ref="fullname">
                                    <small id="fullnameHelp" class="text-danger" v-if="fullnameError">
                                        Full name is required
                                      </small>    
                            </div>
                            <div class="row">
                                <div class="col-md-6 col-sm-6">
                                    <div v-bind:class="{'form-group': true, 'has-error': passwordError, 'has-success': !passwordError }">
                                        <label>Password</label>
                                        <input type="password" id="password" v-model="password" name="password"class="form-control"
                                            placeholder="Password" ref="password">
                                            <small id="passwordHelp" class="text-danger" v-if="passwordError">
                                                Password is required
                                              </small>  
                                    </div>
                                </div>
                                <div class="col-md-6 col-sm-6">
                                    <div v-bind:class="{'form-group': true, 'has-error': passwordConfirmError, 'has-success': !passwordConfirmError }">
                                        <label>Confirm Password</label>
                                        <input type="password" id="passwordConfirm" v-model="passwordConfirm" name="passwordConfirm"
                                            class="form-control" placeholder=" Confirm Password" ref="cpassword">
                                            <small id="passwordConfirmHelp" class="text-danger" v-if="passwordConfirmError">
                                               Confirm Password is required
                                              </small>  
                                    </div>
                                </div>
                            </div>
                            <div v-bind:class="{'form-group': true, 'has-error': phoneNumberError, 'has-success': !phoneNumberError }">
                                <label>Telephone/mobile</label>
                                <input type="text" id="tel_order" name="tel_order" v-model="phoneNumber" class="form-control"
                                    placeholder="Telephone/mobile" ref="telephone">
                                    <small id="phoneNumberErrorHelp" class="text-danger" v-if="phoneNumberError">
                                       Mobile number is required
                                       </small>  
                            </div>
                            <div v-bind:class="{'form-group': true, 'has-error': emailAddressError, 'has-success': !emailAddressError }">
                                <label>Email</label>
                                <input type="email" id="email" name="email_order" v-model="emailAddress" class="form-control"
                                    placeholder="Your email" ref="email">
                                    <small id="emailAddressHelp" class="text-danger" v-if="emailAddressError">
                                        Email address is required
                                        </small> 
                            </div>
                            <div class="form-group">
                                <a class="btn_full" v-on:click="CreateCustomer"> Register </a>
                            </div>
                        </div>

                        <div v-if="ordertDetails">
                            <div v-bind:class="{'form-group': true, 'has-error': order_addressError, 'has-success': !order_addressError }">
                                <label>Your full address</label>
                                <input type="text" id="order_address" v-model="order_address" name="order_address" class="form-control"
                                    placeholder=" Your full address">
                                    <small id="order_addressHelp" class="text-danger" v-if="order_addressError">
                                        Full address is required
                                        </small> 
                            </div>
                            <div class="row">
                                <div class="col-md-6 col-sm-6">
                                    <div v-bind:class="{'form-group': true, 'has-error': delivery_locationError, 'has-success': !delivery_locationError }">
                                        <label>Delivery Location</label>
                                        <input type="text" id="delivery_location" v-model="delivery_location" name="delivery_location" class="form-control"
                                            placeholder="Delivery location">
                                            <small id="delivery_locationHelp" class="text-danger" v-if="delivery_locationError">
                                                Delivery location is required
                                                </small> 
                                    </div>
                                </div>
                                <div class="col-md-6 col-sm-6">
                                    <div v-bind:class="{'form-group': true, 'has-error': recipient_phoneError, 'has-success': !recipient_phoneError }">
                                        <label>Recipient Phone</label>
                                        <input type="text" id="recipient_phone" v-model="recipient_phone" name="recipient_phone" class="form-control"
                                            placeholder=" Your recipient phone number">
                                            <small id="recipient_phoneHelp" class="text-danger" v-if="recipient_phoneError">
                                                Mobile number is required
                                                </small> 
                                    </div>
                                </div>
                            </div>
                            <hr>
                            <div class="row">
                                <div class="col-md-6 col-sm-6">
                                    <div v-bind:class="{'form-group': true, 'has-error': expected_delivery_dayError, 'has-success': !expected_delivery_dayError }">
                                        <label>Delivery Day</label>
                                        <select class="form-control" v-model="expected_delivery_day" name="expected_delivery_day"
                                            id="expected_delivery_day">
                                            <option value="" selected>Select day</option>
                                            <option value="today">Today</option>
                                            <option value="tomorrow">Tomorrow</option>
                                        </select>
                                        <small id="expected_delivery_dayHelp" class="text-danger" v-if="expected_delivery_dayError">
                                            Expected delivery day is required
                                            </small> 
                                    </div>
                                </div>
                                <div class="col-md-6 col-sm-6">
                                    <div v-bind:class="{'form-group': true, 'has-error': delivery_schedule_timeError, 'has-success': !delivery_schedule_timeError }">
                                        <label>Delivery time</label>
                                        <select class="form-control" name="delivery_schedule_time" v-model="delivery_schedule_time"
                                            id="delivery_schedule_time">
                                            <option value="" selected>Select day</option>
                                            <option value="soon_as_possible" selected>As soon as possible</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <hr>
                            {{-- <div class="row">
                                <div class="col-md-12">

                                    <label>Notes for the restaurant</label>
                                    <textarea class="form-control" style="height:150px"
                                        placeholder="Ex. Allergies, cash change..." name="notes" id="notes"></textarea>

                                </div>
                            </div> --}}
                        </div>
                    </div>
                </div><!-- End col-md-6 -->

                <div class="col-md-3" id="sidebar">
                    <div class="theiaStickySidebar">
                        <div id="cart_box">
                            <h3>Your order <i class="icon_cart_alt pull-right"></i></h3>
                            <table class="table table_summary">
                                <tbody>
                                    <tr v-for="order of orders" :key="order.orderId">
                                        <td>
                                            <a href="#0" class="remove_item"
                                                v-on:click="removeOrderItem(order.orderId)">
                                                <i class="icon_minus_alt"></i></a> <strong>[[ order.quantity]]x</strong>
                                            [[order.name ]]
                                        </td>
                                        <td>
                                            <strong class="pull-right">₵[[ order.price ]]</strong>
                                        </td>
                                    </tr>
                                    <span v-if="!orders.length" style="width: 100%;" class="text-center">You have no
                                        orders</span>
                                </tbody>
                            </table>
                            <hr>
                            <div class="row" id="options_2">
                                <div class="col-lg-6 col-md-12 col-sm-12 col-xs-6">
                                    <label><input type="radio" value="" checked name="option_2"
                                            class="icheck">Delivery</label>
                                </div>
                                <div class="col-lg-6 col-md-12 col-sm-12 col-xs-6">
                                    <label><input type="radio" value="" name="option_2" class="icheck">Take Away</label>
                                </div>
                            </div><!-- Edn options 2 -->
                            <hr>
                            <table class="table table_summary">
                                <tbody>
                                    <tr>
                                        <td>
                                            Subtotal <span class="pull-right" v-if="this.orders.length">₵[[ getSubTotal
                                                ]]</span> <span class="pull-right" v-else>----</span>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Delivery fee <span class="pull-right">₵0</span>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="total">
                                            TOTAL <span class="pull-right" v-if="this.orders.length">₵[[
                                                getTotal]]</span>
                                            <span class="pull-right" v-else>----</span>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                            <hr>
                            <a class="btn_full" v-if="checkoutBtnVisible && orders.length" v-on:click="GotoCheckout">Go to
                                checkout</a>
                            <a class="btn_full_outline" href="/products"><i class="icon-right"></i> Add other items</a>
                        </div><!-- End cart_box -->
                    </div><!-- End theiaStickySidebar -->
                </div><!-- End col-md-3 -->
            </div>
        </div><!-- End row -->
    </div><!-- End container -->

    <!-- End Content =============================================== -->

    <!-- Footer ================================================== -->
    @include('layouts.footer')
    <!-- End Footer =============================================== -->

    <div class="layer"></div><!-- Mobile menu overlay mask -->

    <!-- Login modal -->
    @include('layouts.modal')
    <!-- End modal -->


    <!-- Search Menu -->
    <div class="search-overlay-menu">
        <span class="search-overlay-close"><i class="icon_close"></i></span>
        <form role="search" id="searchform" method="get">
            <input value="" name="q" type="search" placeholder="Search..." />
            <button type="submit"><i class="icon-search-6"></i>
            </button>
        </form>
    </div>
    <!-- End Search Menu -->

    <!-- COMMON SCRIPTS -->
    @include('layouts.script')

    <!-- SPECIFIC SCRIPTS -->
    <script src="js/theia-sticky-sidebar.js"></script>
    <script>
        jQuery('#sidebar').theiaStickySidebar({
            additionalMarginTop: 80
        });

    </script>
    <script>

        // const registerSchema = {

        // }

        var app = new Vue({
            el: '#app',
            delimiters: ['[[', ']]'],
            data: {
                registercustomer: true,
                ordertDetails: false,
                checkoutBtnVisible: false,
                fullname: '',
                fullnameError: false,
                mobilenumber: '',
                phoneNumber : '',
                phoneNumberError: false,
                mobilenumberError: false,
                password: '',
                passwordError: false,
                passwordConfirm:'',
                passwordConfirmError:false,
                emailAddress: '',
                emailAddressError: false,
                formIsValid:false,
                checkedForLogin: '',
                showCheckbox: true,
                disabled: 0,
                recipient_phone:'',
                recipient_phoneError: false, 
                delivery_location:'',
                delivery_locationError: false,
                order_address:'',
                order_addressError: false,
                expected_delivery_day:'',
                expected_delivery_dayError: false,
                delivery_schedule_time:'',
                delivery_schedule_timeError:false,
                orders: []
            },
            methods: {
                CreateCustomer() {  
                    console.log("Mobile number :" , this.phoneNumber);
                    console.log("Before Create customer data :" , this.fullname + ' '+ this.password+ ' '+this.passwordConfirm+ ' ' + this.phoneNumber);
                    if(this.fullname === '' || this.fullname.length < 2 || !this.fullname.length) {
                        this.fullnameError = true
                        this.formIsValid = false
                       
                    }  else {

                        this.fullnameError = false
                        this.formIsValid = true
                    }

                    if(this.password === '' || this.password.length < 2 || !this.password.length) {
                        this.passwordError = true
                        this.formIsValid = false
                       
                    }  else {
                        this.passwordError = false
                        this.formIsValid = true
                    }
                    if(this.passwordConfirm === '' || this.passwordConfirm < 2 || !this.passwordConfirm.length){
                          this.passwordConfirmError = true
                          this.formIsValid = false
                    }
                    else{
                        this.passwordConfirmError = false
                        this.formIsValid = true
                    }
                    if(this.phoneNumber === '' || this.phoneNumber < 2 || !this.phoneNumber.length || this.phoneNumber === undefined){
                          this.phoneNumberError = true
                          this.formIsValid = false
                    }
                    else{
                        this.phoneNumberError = false
                        this.formIsValid = true
                    }
                    if(this.emailAddress === '' || this.emailAddress < 2 || !this.emailAddress.length){
                          this.emailAddressError = true
                          this.formIsValid = false
                    }
                    else{
                        this.emailAddressError = false
                        this.formIsValid = true
                    }

                    if(!this.formIsValid) {
                        return
                    }
                    axios.post('/api/v1/register', {
                            name: this.fullname,
                            password: this.password,
                            phone : this.mobileNumber,
                            password_confirmation : this.passwordConfirm,
                            email: this.emailAddress,
                        })
                        .then((response) => {
                            if (response.data.status) {
                                this.registercustomer = false;
                                this.ordertDetails = true
                                this.checkoutBtnVisible = true
                                this.showCheckbox = false
                                localStorage.setItem("token", response.data.token);
                            } else {
                                console.log("empty " + response.data)
                                Vue.$toast.warning(response.data.message, {
                                    position:'bottom-right'
                                    })
                            }
                        })
                        .catch(function (error) {
                            Vue.$toast.error(error, {
                                    position:'bottom-right'
                             })
                        });
                },
                CheckButtonStatus() {
                    if (this.checkedForLogin) {
                        console.log(" Checkbox Status " + this.checkedForLogin);
                        this.registercustomer = false
                    } else {
                        this.registercustomer = true
                    }

                },
                CustomerAlreadyLoggedInn(){
                     if(localStorage.getItem("token") != null)
                     {
                        this.ordertDetails = true;
                        this.checkedForLogin = false
                        this.showCheckbox = false
                        this.checkoutBtnVisible = true
                        this.registercustomer = false
                        this.recipient_phone = localStorage.getItem("recipient_phone")
                        this.order_address = localStorage.getItem("address")
                        this.delivery_schedule_time = localStorage.getItem("delivery_schedule_time")
                        this.expected_delivery_day = localStorage.getItem("expected_delivery_day")
                        this.delivery_location = localStorage.getItem("delivery_location")
                        // Get Customer data

                     }
                },
                LoginCustomer() {
                  
                    if(this.emailAddress === '' || this.emailAddress < 2 || !this.emailAddress.length){
                          this.emailAddressError = true
                          this.formIsValid = false
                    }
                    else{
                        this.emailAddressError = false
                        this.formIsValid = true
                    }
                    if(this.password === '' || this.password < 2 || !this.password.length){
                          this.passwordError = true
                          this.formIsValid = false
                    }
                    else{
                        this.passwordError = false
                        this.formIsValid = true
                    }

                    if(!this.formIsValid) {
                        return
                    }
                    axios.post('/api/v1/login', {
                            email: this.emailAddress,
                            password: this.password
                        })
                        .then((response) => {
                            console.log(" User data :", response)
                            if (response.data.status) {
                                this.ordertDetails = true;
                                this.checkedForLogin = false
                                this.showCheckbox = false
                                this.checkoutBtnVisible = true
                                 localStorage.setItem("token", response.data.token)
                            } else {
                                Vue.$toast.warning(response.data.message, {
                                    position:'bottom-right'
                                    })
                            }
                        })
                        .catch(function (error) {
                            Vue.$toast.error(error, {
                                    position:'bottom-right'
                              })
                        });
                },
                GotoCheckout(){

                    if(this.order_address === '' || this.order_address < 2 || !this.order_address.length){
                          this.order_addressError = true
                          this.formIsValid = false
                    }
                    else{
                        this.order_addressError = false
                        this.formIsValid = true
                    }
                    if(this.delivery_location === '' || this.delivery_location < 2 || !this.delivery_location.length){
                          this.delivery_locationError = true
                          this.formIsValid = false
                    }
                    else{
                        this.delivery_locationError = false
                        this.formIsValid = true
                    }
                    if(this.recipient_phone === '' || this.recipient_phone < 2 || !this.recipient_phone.length){
                          this.recipient_phoneError = true
                          this.formIsValid = false
                    }
                    else{
                        this.recipient_phoneError = false
                        this.formIsValid = true
                    }
                    //recipient_phoneError
                    if(this.expected_delivery_day === '' || this.expected_delivery_day < 2 || !this.expected_delivery_day.length){
                          this.expected_delivery_dayError = true
                          this.formIsValid = false
                    }
                    else{
                        this.expected_delivery_dayError = false
                        this.formIsValid = true
                    }
                   //delivery_schedule_time
                   if(this.delivery_schedule_time === '' || this.delivery_schedule_time < 2 || !this.delivery_schedule_time.length){
                          this.expected_delivery_dayError = true
                          this.formIsValid = false
                    }
                    else{
                        this.expected_delivery_dayError = false
                        this.formIsValid = true
                    }
                    if(!this.formIsValid) {
                        return
                    }
                    
                    localStorage.setItem("address",this.order_address)
                    localStorage.setItem("recipient_phone",this.recipient_phone)
                    localStorage.setItem("delivery_location",this.delivery_location)
                    localStorage.setItem("order_address",this.order_address)
                    localStorage.setItem("expected_delivery_day",this.expected_delivery_day)
                    localStorage.setItem("delivery_schedule_time",this.delivery_schedule_time)
                    window.location.href ="/checkout";
                },
                removeOrderItem(orderId) {

                    console.log('test')

                    const otherOrders = this.orders.filter(order => order.orderId != orderId)

                    this.orders = [...otherOrders]

                    const storedOrdersData = localStorage.getItem('orders')
                    // nice one last issue when u dont add any quantity it adds it to the cart eg
                    if (storedOrdersData) {

                        localStorage.setItem('orders', JSON.stringify(this.orders))

                    }
                }
            },
            created() {
                this.CustomerAlreadyLoggedInn();
                let storedOrdersData = localStorage.getItem('orders')

                if (storedOrdersData) {

                    const storeOrders = JSON.parse(storedOrdersData)

                    this.orders = storeOrders
                }
            },
            
            computed: {
                getSubTotal: function () {

                    return this.orders.reduce((a, b) => a + (b.price * b.quantity), 0)
                },
                getTotal: function () {
                    return this.getSubTotal
                }
            }
        })

    </script>
</body>

</html>
