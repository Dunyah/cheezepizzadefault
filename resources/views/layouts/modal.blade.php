<!-- Login modal -->
<div class="modal fade" id="login_2" tabindex="-1" role="dialog" aria-labelledby="myLogin" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content modal-popup" >
            <a href="#" class="close-link"><i class="icon_close_alt2"></i></a>
            <div id="app1">
            <form action="#" class="popup-form" id="myLogin">
                <div class="login_icon"><i class="icon_lock_alt"></i></div>
                    <input type="text" class="form-control form-white" v-model="emailAddress" placeholder="Email address">
                    <small id="emailaddressHelp" class="text-danger" v-if="emailAddressError">
                        Email address is required
                      </small> 

                    <input type="password" class="form-control form-white" v-model="password" placeholder="Password">
                    <small id="passwordHelp" class="text-danger" v-if="passwordError">
                        Password is required
                      </small> 
          
                <div class="text-left">
                    {{-- <a href="#">Forgot Password?</a> --}}
                </div>
                <button type="button" v-on:click="MyLoginCustomer" class="btn btn-submit">Submit</button>
            </form>
          </div>
        </div>
    </div>
</div><!-- End modal -->

<!-- Register modal -->
{{-- <div class="modal fade" id="register" tabindex="-1" role="dialog" aria-labelledby="myRegister" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content modal-popup">
            <a href="#" class="close-link"><i class="icon_close_alt2"></i></a>
            <form action="#" class="popup-form" id="myRegister">
                <div class="login_icon"><i class="icon_lock_alt"></i></div>
                <input type="text" class="form-control form-white" placeholder="Name">
                <input type="text" class="form-control form-white" placeholder="Last Name">
                <input type="email" class="form-control form-white" placeholder="Email">
                <input type="text" class="form-control form-white" placeholder="Password" id="password1">
                <input type="text" class="form-control form-white" placeholder="Confirm password" id="password2">
                <div id="pass-info" class="clearfix"></div>
                <div class="checkbox-holder text-left">
                    <div class="checkbox">
                        <input type="checkbox" value="accept_2" id="check_2" name="check_2" />
                        <label for="check_2"><span>I Agree to the <strong>Terms &amp; Conditions</strong></span></label>
                    </div>
                </div>
                <button type="submit" v-on:click="RegisterCustomer" class="btn btn-submit">Register</button>
            </form>
        </div>
    </div>
</div> --}}
<!-- End Register modal -->

<script>
       var app1 = new Vue({
        el: '#app1',
        delimiters: ['[[', ']]'],
        data:{
             emailAddress:'',
             emailAddressError:false,
             password:'',
             passwordError:'',
             formIsValid: false
        },
        methods: {
            MyLoginCustomer(){
                if(this.emailAddress === '' || this.emailAddress.length < 2 || !this.emailAddress.length) {
                        this.emailAddressError = true
                        this.formIsValid = false
                       
                    }  else {
                        this.emailAddressError = false
                        this.formIsValid = true
                    }
                if(this.password === '' || this.password.length < 2 || !this.password.length) {
                        this.passwordError = true
                        this.formIsValid = false
                       
                    }  else {
                        this.passwordError = false
                        this.formIsValid = true
                    }
                axios.post('/api/v1/login', {
                            email: this.emailAddress,
                            password: this.password
                        })
                        .then((response) => {
                            if (response.data.status) {
                                 localStorage.setItem("token", response.data.token)
                                 window.location.href = "/"
                            } else {
                                Vue.$toast.warning(response.data.message, {
                                    position:'bottom-right'
                                    })
                            }
                        })
                        .catch(function (error) {
                            Vue.$toast.error(error, {
                                    position:'bottom-right'
                              })
                        });
                }
           }
       })
</script>