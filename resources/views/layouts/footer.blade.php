<!-- Footer ================================================== -->
<footer>
        <div class="container">
            <div class="row">

                <div class="col-md-2 col-sm-3">

                </div>
            </div><!-- End row -->
            <div class="row">
                <div class="col-md-12">
                    <div id="social_footer">
                        <ul>
                            <li><a href="https://m.facebook.com/cheezzypizza/"><i class="icon-facebook"></i></a></li>
                            <li><a href="https://twitter.com/CheezzyPizza"><i class="icon-twitter"></i></a></li>
                            <!-- <li><a href="#0"><i class="icon-google"></i></a></li>
                                <li><a href="#0"><i class="icon-instagram"></i></a></li>
                                <li><a href="#0"><i class="icon-vimeo"></i></a></li>  -->
                            <li><a href="https://www.youtube.com/watch?v=wt5v0g4sNxI&feature=emb_logo"><i class="icon-youtube-play"></i></a></li>
                        </ul>
                        <p>
                            © Cheezy Pizza 2020
                        </p>
                    </div>
                </div>
            </div><!-- End row -->
        </div><!-- End container -->
    </footer>
    <!-- End Footer =============================================== -->
