<div id="preloader">
        <div class="sk-spinner sk-spinner-wave" id="status">
            <div class="sk-rect1"></div>
            <div class="sk-rect2"></div>
            <div class="sk-rect3"></div>
            <div class="sk-rect4"></div>
            <div class="sk-rect5"></div>
        </div>
    </div><!-- End Preload -->
    
    <!-- Header ================================================== -->
    <header>
    <div class="container-fluid">
        <div class="row">
            <div class="col--md-4 col-sm-4 col-xs-4">
                <a href="#" id="logo">
                <img src="/img/logo.png" width="190" height="23" alt="" data-retina="true" class="hidden-xs">
                <img src="img/logo.png" width="59" height="23" alt="" data-retina="true" class="hidden-lg hidden-md hidden-sm">
                </a>
            </div>
            <nav class="col--md-8 col-sm-8 col-xs-8">
            <a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a>
            <div class="main-menu" id="app2">
                {{-- <div id="app"></div> --}}
                <div id="header_menu">
                    <img src="img/logo.png" width="190" height="23" alt="" data-retina="true">
                </div>
                <a href="#" class="open_close" id="close_in"><i class="icon_close"></i></a>
                <ul>
                    <li><a href="/">Home</a></li>
                    <li><a href="/aboutus">About us</a></li>
                    <li><a href="#0" data-toggle="modal" v-if="showLogin" data-target="#login_2">Login</a></li>
                    <li><a href="contactus">Contact us</a></li>
                    <li><a href="/vieworders" v-if="loginStatus" >My account</a></li>
                    <li><a  href="#" v-if="loginStatus" v-on:click="Logout">Logout</a></li>
                </ul>
            </div><!-- End main-menu -->
            </nav>
        </div><!-- End row -->
    </div><!-- End container -->

    </header>

    <script>
         var app2 = new Vue({
            el: '#app2',
            delimiters: ['[[', ']]'],
            data: {
                loginStatus : false,
                 hasLogin: '',
                 showLogin : true

            },
            methods:{
                Logout(){
                    localStorage.removeItem("token");
                    window.location.href = "/"
                }
            },
            created(){
                this.hasLogin = localStorage.getItem("token")
                 if(this.hasLogin != null){
                   this.loginStatus = true
                   this.showLogin = false
                 }
            }
         })
    </script>
