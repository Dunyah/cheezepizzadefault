<!DOCTYPE html>
<!--[if IE 9]><html class="ie ie9"> <![endif]-->
<html>
    <head>
        @include('layouts.head')
           <!-- Modernizr -->
     	<script src="js/modernizr.js"></script> 
    </head>
<body>
     <!-- Header -->
      @include('layouts.header')
	<!-- End Header =============================================== -->
    
    <!-- SubHeader =============================================== -->
    <section class="header-video">
    <div id="hero_video">
       
    </div>
    <img src="img/video_fix.png" alt="" class="header-video--media" data-video-src="video/intro" data-teaser-source="video/intro" data-provider="Vimeo" data-video-width="1920" data-video-height="960">
    <div id="count" class="hidden-xs">
        <ul>
            <li><span class="number">2650</span> Restaurant</li>
            <li><span class="number">5350</span> People Served</li>
            <li><span class="number">12350</span> Registered Users</li>
        </ul>
    </div>
    </section><!-- End Header video -->
    <!-- End SubHeader ============================================ -->
    
   <!-- Content ================================================== -->

            
    <div class="white_bg">
    <div class="container margin_60">
        
        <div class="main_title">
            <h2 class="nomargin_top">Select from our branches </h2>
        </div>
        
        <div class="row" id="app">
                <div class="col-md-6"  v-for="branch in branches" :key="branch.id">
                    <a v-on:click="GetBranchProductsAndCat(branch)" class="strip_list">
                        <div class="ribbon_1">Popular</div>
                        <div class="desc">
                            <div class="thumb_strip">
                                <img src="img/thumb_restaurant.jpg" alt="">
                            </div>
                            <div class="rating">
                                <i class="icon_star voted"></i><i class="icon_star voted"></i><i class="icon_star voted"></i><i class="icon_star voted"></i><i class="icon_star"></i>
                            </div>
                            <h3>[[ branch.name]] </h3>
                            <div class="type">
                                 [[branch.id ]]
                            </div>
                            <div class="location">
                                [[ branch.location ]] <br><span class="opening"> Opens at [[ branch.opening_time ]]  </span>
                            </div>
                            <ul>
                                <li>Take away<i class="icon_check_alt2 ok"></i></li>
                                <li>Delivery<i class="icon_check_alt2 ok"></i></li>
                            </ul>
                        </div>
                    </a>
                    <!-- End strip_list-->
                </div><!-- End col-md-6-->
            </div><!-- End row -->
        
        </div><!-- End container -->
        </div><!-- End white_bg -->
        
       <div class="high_light">
      	<div class="container">
      		<h3>Choose from over 2,000 Restaurants</h3>
            <p>Ridiculus sociosqu cursus neque cursus curae ante scelerisque vehicula.</p>
            <a href="#">View all Restaurants</a>
        </div><!-- End container -->
      </div><!-- End hight_light -->
                  
    <section class="parallax-window" data-parallax="scroll" data-image-src="img/bg_office.jpg" data-natural-width="1200" data-natural-height="600">
    <div class="parallax-content">
        <div class="sub_content">
            <i class="icon_mug"></i>
            <h3>We also deliver to your office</h3>
            <p>
                Ridiculus sociosqu cursus neque cursus curae ante scelerisque vehicula.
            </p>
        </div><!-- End sub_content -->
    </div><!-- End subheader -->
    </section><!-- End section -->
    <!-- End Content =============================================== -->
    

 <!-- Footer ================================================== -->
   @include('layouts.footer')
    <!-- End Footer =============================================== -->

<div class="layer"></div><!-- Mobile menu overlay mask -->
@include('layouts.modal')
      @include('layouts.script')
      <script src="js/video_header.js"></script>
<script>
$(document).ready(function() {
	'use strict';
   	  HeaderVideo.init({
      container: $('.header-video'),
      header: $('.header-video--media'),
      videoTrigger: $("#video-trigger"),
      autoPlayVideo: true
    });    

});
</script>
<script>
      var app = new Vue({
        el: '#app',
        delimiters: ['[[', ']]'],
        data: {
            message: 'Hello Vue!',
            branches:{},
            emailAddress:'',
            password:''
        },
        created() {
            console.log(arguments)
            this.GetBranches();
        },
        methods:{
            LoginCustomer(){
              console.log("Email " , this.emailAddress);
            },
            GetBranches(){
            axios.get('/api/v1/branches')
                .then((response) => {
                    // handle success
                    if(response.data.status){
                    this.branches = response.data.data;
                    }
                    else{
                        console.log("empty " + response.data)
                    }  
                })
                .catch(function (error) {
                    // handle error
                    console.log(error);
                });               
           },
        GetBranchProductsAndCat(branch){
            window.location.href= '/products';
            console.log("Branch Id :" , branch.id)
            localStorage.setItem('branchId' , branch.id);
            localStorage.setItem('branchName', branch.name);
            localStorage.setItem('location', branch.location);
            localStorage.setItem('openingTime', branch.opening_time)
        }
    }
        
    })
</script>

</body>
</html>