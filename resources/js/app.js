/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue');
import VueRouter from 'vue-router'
Vue.use(VueRouter)

const axios = require('axios').default;
window.axios = axios;

import Swal from 'sweetalert2'
window.swal = Swal

/**
 * The following block of code may be used to automatically register your
 * Vue components. It will recursively scan this directory for the Vue
 * components and automatically register them with their "basename".
 *
 * Eg. ./components/ExampleComponent.vue -> <example-component></example-component>
 */

// const files = require.context('./', true, /\.vue$/i)
// files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default))

//Vue.component('example-component', require('./components/ExampleComponent.vue').default);



/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

const router = new VueRouter({
    mode :'history',
    routes :[
        {
         path: '/',
         component: () => import('./Main/main.vue'),
         children:[
             {
                 path: '/',
                 name: 'home',
                 component: () => import('./views/Home.vue')
             },
             {
                path: '/about',
                name: 'About',
                component: () => import('./views/About.vue')
             },
             {
                path: '/contact',
                name: 'Contact',
                component: () => import('./views/ContactUs.vue')
             },
             {
                path: '/products',
                name: 'products',
                component: () => import('./views/Products.vue')
             },
             {
                path: '/carts',
                name: 'carts',
                component: () => import('./views/Carts.vue')
             },
             {
                path: '/payments',
                name: 'Payment Mthods',
                component: () => import('./views/Paymentmethods.vue')
             },
             {
                path: '/confirm',
                name: 'Confirm PAyment',
                component: () => import('./views/ConfirmPayment.vue')
             }

         ],
        }
 ]

})

const app = new Vue({
    el: '#app',
    router
});

export default app
